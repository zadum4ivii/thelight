﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TheLight.Entities;

namespace TheLight.IO
{
    [Serializable]
    public class InventoryItemSlotIO
    {
        public string ItemType;
        public int ItemAmount;

        public InventoryItemSlotIO(Slot slot)
        {
            if (slot.ContainedItem != null)
            {
                ItemType = slot.ContainedItem.GetType().ToString();
                ItemAmount = slot.ContainedItem.CurrentStack;
            }
        }

        public void Serialize(Stream stream)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, this);
        }

        public void Deserialize()
        {

        }
    }
}
