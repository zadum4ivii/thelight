﻿using Microsoft.Xna.Framework;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TheLight.Entities;
using TheLight.Gameplay;

namespace TheLight.IO
{
    [Serializable]
    public class WorldIO
    {
        public int Time;
        public int WorldNumber;
        public float PlayerPosX;
        public float PlayerPosY;
        public float PlayerPosZ;
        public string WorldName;

        public WorldIO(World world, Player player)
        {
            Time = world.Time;
            PlayerPosX = player.Position.X;
            PlayerPosY = player.Position.Y;
            PlayerPosZ = player.Position.Z;
            WorldName = world.WorldName;
            WorldNumber = world.WorldNumber;
        }

        public void Serialize(Stream stream)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, this);
        }

        public void Deserialize()
        {
            Main Main = Main.Instance;
            Main.World.Time = Time;
            Main.Player.Position = new Vector3(PlayerPosX, PlayerPosY, PlayerPosZ);
            Main.World.WorldName = WorldName;
            Main.World.WorldNumber = WorldNumber;
        }
    }
}
