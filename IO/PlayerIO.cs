﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TheLight.Entities;

namespace TheLight.IO
{
    [Serializable]
    public class PlayerIO
    {
        public int ColorIndex;
        public int Direction;
        public int CurrentHealth;
        public int MaximumHealth;
        public int CurrentMana;
        public int MaximumMana;
        
        public PlayerIO(Player player)
        {
            ColorIndex = player.ColorIndex;
            Direction = player.Direction;
            CurrentHealth = player.CurrentHealth;
            MaximumHealth = player.MaximumHealth;
            CurrentMana = player.CurrentMana;
            MaximumMana = player.MaximumMana;
        }

        public void Serialize(Stream stream)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, this);
        }

        public void Deserialize()
        {
            Main Main = Main.Instance;
            Main.Player.ColorIndex = ColorIndex;
            Main.Player.Autoload();
            Main.Player.Direction = Direction;
            Main.Player.CurrentHealth = CurrentHealth;
            Main.Player.MaximumHealth = MaximumHealth;
            Main.Player.CurrentMana = CurrentMana;
            Main.Player.MaximumMana = MaximumMana;
        }
    }
}
