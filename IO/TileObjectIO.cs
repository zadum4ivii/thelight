﻿using Microsoft.Xna.Framework;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TheLight.Entities;

namespace TheLight.IO
{
    [Serializable]
    public class TileObjectIO
    {
        public float WorldPosX;
        public float WorldPosY;
        public float WorldPosZ;
        public int Direction;
        public int ColorIndex;
        public int RandomFrame;
        public string Type;

        public TileObjectIO(TileObject t)
        {
            WorldPosX = t.WorldPosition.X;
            WorldPosY = t.WorldPosition.Y;
            WorldPosZ = t.WorldPosition.Z;
            Direction = t.Direction;
            ColorIndex = t.ColorIndex;
            RandomFrame = t.RandomFrame;
            Type = t.GetType().ToString();
        }

        public void Serialize(Stream stream)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, this);
        }

        public void Deserialize()
        {
            TileObject placedTile = TileObject.PlaceTile(new Vector3(WorldPosX, WorldPosY, WorldPosZ), System.Type.GetType(Type));
            if (placedTile != null)
            {
                placedTile.Direction = Direction;
                placedTile.RandomFrame = RandomFrame;
                if (placedTile.ColorsTexture != null)
                {
                    placedTile.ColorIndex = ColorIndex;
                    placedTile.RecoloredTexture = Utils.SwapColors(placedTile.ColorsTexture, placedTile.Texture, placedTile.ColorIndex);
                }
            }
        }
    }
}
