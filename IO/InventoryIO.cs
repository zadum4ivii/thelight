﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TheLight.Entities;
using TheLight.Entities.UI;

namespace TheLight.IO
{
    [Serializable]
    public class InventoryIO
    {
        public List<InventoryItemSlotIO> InventoryItemSlots;
        public InventoryItemSlotIO LeftHandHotbar1;
        public InventoryItemSlotIO LeftHandHotbar2;
        public InventoryItemSlotIO RightHandHotbar1;
        public InventoryItemSlotIO RightHandHotbar2;       
        public InventoryItemSlotIO HeadArmorSlot;
        public InventoryItemSlotIO BodyArmorSlot;
        public InventoryItemSlotIO LegsArmorSlot;        
        public InventoryItemSlotIO AccessoryArmorSlot;
        public InventoryItemSlotIO NecklaceArmorSlot;
        public InventoryItemSlotIO RingArmorSlot;

        public InventoryIO(Inventory inventory)
        {
            Main Main = Main.Instance;
            InventoryItemSlots = new List<InventoryItemSlotIO>();
            foreach (Slot s1 in inventory.InventorySlots)
            {
                if (s1 is InventoryItemSlot)
                {
                    InventoryItemSlotIO s2 = new InventoryItemSlotIO(s1);
                    InventoryItemSlots.Add(s2);
                }
            }
            LeftHandHotbar1 = new InventoryItemSlotIO(Main.Interface.LeftHandHotbar1);
            LeftHandHotbar2 = new InventoryItemSlotIO(Main.Interface.LeftHandHotbar2);
            RightHandHotbar1 = new InventoryItemSlotIO(Main.Interface.RightHandHotbar1);
            RightHandHotbar2 = new InventoryItemSlotIO(Main.Interface.RightHandHotbar2);
            HeadArmorSlot = new InventoryItemSlotIO(Main.Interface.Inventory.HeadArmorSlot);
            BodyArmorSlot = new InventoryItemSlotIO(Main.Interface.Inventory.BodyArmorSlot);
            LegsArmorSlot = new InventoryItemSlotIO(Main.Interface.Inventory.LegsArmorSlot);
            AccessoryArmorSlot = new InventoryItemSlotIO(Main.Interface.Inventory.AccessoryArmorSlot);
            NecklaceArmorSlot = new InventoryItemSlotIO(Main.Interface.Inventory.NecklaceArmorSlot);
            RingArmorSlot = new InventoryItemSlotIO(Main.Interface.Inventory.RingArmorSlot);
        }

        public void Serialize(Stream stream)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, this);
        }

        public void Deserialize()
        {
            Main Main = Main.Instance;
            for (int i = 0; i < InventoryItemSlots.Count; i++)
            {
                if (InventoryItemSlots[i].ItemType != null)
                {
                    if (Main.Interface.Inventory.InventorySlots[i] is InventoryItemSlot)
                    {
                        Main.Interface.Inventory.InventorySlots[i].ContainedItem = Item.CreateItem(Type.GetType(InventoryItemSlots[i].ItemType));
                        Main.Interface.Inventory.InventorySlots[i].ContainedItem.CurrentStack = InventoryItemSlots[i].ItemAmount;
                    }
                }
            }
            if (LeftHandHotbar1.ItemType != null)
            {
                Main.Interface.LeftHandHotbar1.ContainedItem = Item.CreateItem(Type.GetType(LeftHandHotbar1.ItemType));
                Main.Interface.LeftHandHotbar1.ContainedItem.CurrentStack = LeftHandHotbar1.ItemAmount;
            }
            if (LeftHandHotbar2.ItemType != null)
            {
                Main.Interface.LeftHandHotbar2.ContainedItem = Item.CreateItem(Type.GetType(LeftHandHotbar2.ItemType));
                Main.Interface.LeftHandHotbar2.ContainedItem.CurrentStack = LeftHandHotbar2.ItemAmount;
            }
            if (RightHandHotbar1.ItemType != null)
            {
                Main.Interface.RightHandHotbar1.ContainedItem = Item.CreateItem(Type.GetType(RightHandHotbar1.ItemType));
                Main.Interface.RightHandHotbar1.ContainedItem.CurrentStack = RightHandHotbar1.ItemAmount;
            }
            if (RightHandHotbar2.ItemType != null)
            {
                Main.Interface.RightHandHotbar2.ContainedItem = Item.CreateItem(Type.GetType(RightHandHotbar2.ItemType));
                Main.Interface.RightHandHotbar2.ContainedItem.CurrentStack = RightHandHotbar2.ItemAmount;
            }
            if (HeadArmorSlot.ItemType != null)
            {
                Main.Interface.Inventory.HeadArmorSlot.ContainedItem = Item.CreateItem(Type.GetType(HeadArmorSlot.ItemType));
                Main.Interface.Inventory.HeadArmorSlot.ContainedItem.CurrentStack = HeadArmorSlot.ItemAmount;
                Main.Interface.Inventory.HeadArmorSlot.ContainedItem.PlacedInSlot(Main.Interface.Inventory.HeadArmorSlot);
            }
            if (BodyArmorSlot.ItemType != null)
            {
                Main.Interface.Inventory.BodyArmorSlot.ContainedItem = Item.CreateItem(Type.GetType(BodyArmorSlot.ItemType));
                Main.Interface.Inventory.BodyArmorSlot.ContainedItem.CurrentStack = BodyArmorSlot.ItemAmount;
                Main.Interface.Inventory.BodyArmorSlot.ContainedItem.PlacedInSlot(Main.Interface.Inventory.BodyArmorSlot);
            }
            if (LegsArmorSlot.ItemType != null)
            {
                Main.Interface.Inventory.LegsArmorSlot.ContainedItem = Item.CreateItem(Type.GetType(LegsArmorSlot.ItemType));
                Main.Interface.Inventory.LegsArmorSlot.ContainedItem.CurrentStack = LegsArmorSlot.ItemAmount;
                Main.Interface.Inventory.LegsArmorSlot.ContainedItem.PlacedInSlot(Main.Interface.Inventory.LegsArmorSlot);
            }
            if (AccessoryArmorSlot.ItemType != null)
            {
                Main.Interface.Inventory.AccessoryArmorSlot.ContainedItem = Item.CreateItem(Type.GetType(AccessoryArmorSlot.ItemType));
                Main.Interface.Inventory.AccessoryArmorSlot.ContainedItem.CurrentStack = AccessoryArmorSlot.ItemAmount;
                Main.Interface.Inventory.AccessoryArmorSlot.ContainedItem.PlacedInSlot(Main.Interface.Inventory.AccessoryArmorSlot);
            }
            if (RingArmorSlot.ItemType != null)
            {
                Main.Interface.Inventory.RingArmorSlot.ContainedItem = Item.CreateItem(Type.GetType(RingArmorSlot.ItemType));
                Main.Interface.Inventory.RingArmorSlot.ContainedItem.CurrentStack = RingArmorSlot.ItemAmount;
                Main.Interface.Inventory.RingArmorSlot.ContainedItem.PlacedInSlot(Main.Interface.Inventory.RingArmorSlot);
            }
            if (NecklaceArmorSlot.ItemType != null)
            {
                Main.Interface.Inventory.NecklaceArmorSlot.ContainedItem = Item.CreateItem(Type.GetType(NecklaceArmorSlot.ItemType));
                Main.Interface.Inventory.NecklaceArmorSlot.ContainedItem.CurrentStack = NecklaceArmorSlot.ItemAmount;
                Main.Interface.Inventory.NecklaceArmorSlot.ContainedItem.PlacedInSlot(Main.Interface.Inventory.NecklaceArmorSlot);
            }
        }
    }
}
