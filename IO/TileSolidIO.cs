﻿using Microsoft.Xna.Framework;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TheLight.Entities;

namespace TheLight.IO
{
    [Serializable]
    public class TileSolidIO
    {
        public int WorldPosX;
        public int WorldPosY;
        public int WorldPosZ;
        public int RandomFrame;
        public string Type;

        public TileSolidIO(TileSolid t)
        {
            WorldPosX = (int)(t.Position.X / 32f);
            WorldPosY = (int)(t.Position.Y / 32f);
            WorldPosZ = (int)(t.Position.Z / 32f);
            RandomFrame = t.RandomFrame;
            Type = t.GetType().ToString();
        }

        public void Serialize(Stream stream)
        {
            BinaryFormatter binaryFormatter = new BinaryFormatter();
            binaryFormatter.Serialize(stream, this);
        }

        public void Deserialize()
        {
            TileSolid placedTile = TileSolid.PlaceTile(new Vector3(WorldPosX, WorldPosY, WorldPosZ), System.Type.GetType(Type));
            placedTile.RandomFrame = RandomFrame;
        }
    }
}
