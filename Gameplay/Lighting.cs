﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheLight.Gameplay
{
    public class Lighting
    {
        Main Main = Main.Instance;
        private float _lightingAlpha = 0f;
        private Texture2D _lightTexture { get; set; }
        public Color Color { get; set; }

        public Lighting() { LightingTexture(); }

        public void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch) { spriteBatch.Draw(_lightTexture, Vector2.Zero, Color * LightingAlpha()); }

        public void Update(GameTime gameTime)
        {
            if (Main.World.Night) Color = Color.Black;
            else if (Main.World.Sunset) Color = new Color(12, 0, 22);
            else if (Main.World.Dawn) Color = new Color(25, 16, 0);
            else Color = Color.White;
        }

        public void LightingTexture()
        {
            Texture2D texture = new Texture2D(Main.GraphicsDevice, Main.Screen.Width, Main.Screen.Height);
            Color[] data = new Color[Main.Screen.Width * Main.Screen.Height];
            for (int i = 0; i < data.Length; i++) data[i] = Color.White;
            texture.SetData(data);
            _lightTexture = texture;
        }

        private float LightingAlpha()
        {
            double stopTime = Main.World.DayTime / 6;
            float maxAlpha = 0.95f;
            if (Main.World.Day) { _lightingAlpha = 0f; }
            else if (Main.World.Sunset)
            {
                double startPoint = Main.World.DayTime / 2 + stopTime;
                _lightingAlpha = maxAlpha * (float)((Main.World.Time - startPoint) / (Main.World.DayTime - stopTime * 5));
            }
            else if (Main.World.Night) { _lightingAlpha = maxAlpha; }
            else if (Main.World.Dawn)
            {
                double startPoint = stopTime;
                _lightingAlpha = maxAlpha - (maxAlpha * (float)((Main.World.Time - startPoint) / (Main.World.DayTime - stopTime * 5)));
            }
            return _lightingAlpha;
        }
    }
}
