﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using TheLight.Entities;
using TheLight.Entities.Items;
using TheLight.Entities.NPCs;
using TheLight.Entities.Tiles.Objects;
using TheLight.Entities.Tiles.Solid;
using TheLight.IO;

namespace TheLight.Gameplay
{
    public class World
    {
        Main Main = Main.Instance;
        private int _tilesX = 64;
        private int _tilesY = 64;
        private int _tilesZ = 15;
        public int WorldNumber { get; set; }
        public bool Dawn { get; set; }
        public bool Day { get; set; }
        public bool Night { get; set; }
        public bool Sunset { get; set; }
        public double DayTime { get; set; }
        public float[] DayTimeFactor { get; set; }
        public int FPS { get; private set; }
        public int Hours { get => Time / (60 * 60 * 24); }
        public int Minutes { get => Time / (60 * 60) % 60 % 24; }
        public int Seconds { get => Time / 60 % 60; }
        public int Ticks { get => Time % 60; }
        public int Time { get; set; }
        public int WorldHeight { get; set; }
        public int WorldWidth { get; set; }
        public int WorldLength { get; set; }
        public static World Instance { get; private set; }
        public TileSolid[,,] WorldTiles { get; set; }
        public const string WorldSaveFile = ".world";
        public string WorldName { get; set; }
        public bool DrawingWorldName { get; set; }
        private int _worldNameTimer { get; set; }
        private float _worldNameAlpha = 0f;

        public World()
        {
            Instance = this;
            WorldWidth = 32 * _tilesX;
            WorldLength = 32 * _tilesY;
            WorldHeight = 32 * _tilesZ;
            WorldTiles = new TileSolid[_tilesX, _tilesY, _tilesZ];
        }

        public void Update(GameTime gameTime)
        {
            Vector2 PlacingPosition = new Vector2(Main.Interface.Cursor.Position.X, Main.Interface.Cursor.Position.Y + 32);
            if (KeyButtons.PressedRightMouse() && Main.Settings.EnableTilePlacement)
                TileSolid.PlaceHill(PlacingPosition, (int)Main.Interface.Cursor.HillLvl);

            SpawnEnemies();
            UpdateTime(gameTime);
            UpdateFPS(gameTime);

            foreach (TileSolid tile in WorldTiles)
            {
                if (tile != null)
                    tile.Update(gameTime);
            }
        }

        private void UpdateTime(GameTime gameTime)
        {
            double stopTime = DayTime / 6;
            Day = Main.World.Time >= Main.World.DayTime / 2 - stopTime && Main.World.Time < Main.World.DayTime / 2 + stopTime;
            Sunset = Main.World.Time >= Main.World.DayTime / 2 + stopTime && Main.World.Time < Main.World.DayTime - stopTime;
            Night = Main.World.Time >= Main.World.DayTime - stopTime || Main.World.Time < stopTime;
            Dawn = Main.World.Time >= stopTime && Main.World.Time < Main.World.DayTime / 2 - stopTime;
            //Time += !Main.Interface.TimeIcon.Controled ? 1 : 0;
            if (Time > DayTime) Time = 0;
        }

        private void UpdateFPS(GameTime gameTime)
        {
            if (gameTime.TotalGameTime.Ticks % 60 != 0) return;
            FPS = (int)Main.TimeOfDrawing;
            Main.TimeOfDrawingNull();
        }

        public void WorldLoad()
        {
            if (File.Exists(WorldSaveFile))
            {
                if (System.Windows.Forms.MessageBox.Show("Обнаружено сохранение мира. Загрузить?", "Уведомление", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    Utils.Log("World save file found, loading world");
                    using (Stream stream = File.Open(WorldSaveFile, FileMode.Open))
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        while (stream.Position != stream.Length)
                        {
                            object obj = binaryFormatter.Deserialize(stream);
                            //solid tiles
                            if (obj.GetType().ToString().Contains("TileSolidIO"))
                            {
                                TileSolidIO tile = (TileSolidIO)obj;
                                tile.Deserialize();
                            }
                            //object tiles
                            if (obj.GetType().ToString().Contains("TileObjectIO"))
                            {
                                TileObjectIO tile = (TileObjectIO)obj;
                                tile.Deserialize();
                            }
                            //world
                            if (obj.GetType().ToString().Contains("WorldIO"))
                            {
                                WorldIO world = (WorldIO)obj;
                                world.Deserialize();
                            }
                        }
                    }
                    DayTime = 3000.0;
                }
                else
                {
                    Utils.Log("World save file found, but creating a new world");
                    DayTime = 3000.0;
                    Time = (int)(DayTime / 2);
                    GenerateWorld();
                }
            }
            else
            {
                Utils.Log("World save file not found, creating a new world");
                DayTime = 3000.0;
                Time = (int)(DayTime / 2);
                GenerateWorld();
            }
            OnWorldEnter();
        }

        public void WorldSave()
        {
            Utils.Log("Saving the world");
            try
            {
                using (Stream stream = File.Open(WorldSaveFile, FileMode.Create))
                {
                    foreach (TileSolid tile in WorldTiles)
                    {
                        if (tile != null)
                        {
                            TileSolidIO t = new TileSolidIO(tile);
                            t.Serialize(stream);
                        }
                    }
                    foreach (Entity e in Main.ActiveEntities)
                    {
                        if (e is TileObject object1)
                        {
                            TileObjectIO t = new TileObjectIO(object1);
                            t.Serialize(stream);
                        }
                    }
                    WorldIO w = new WorldIO(this, Main.Player);
                    w.Serialize(stream);
                }
			}
            catch (Exception e)
            {
                Utils.Log("Failed to save the world: " + e);
            }
        }

        private void SpawnEnemies()
        {
            if (Main.Settings.EnableEnemies)
            {
                Vector3 SpawnPoint = new Vector3(Main.Rand.Next((int)Main.Screen.ScreenPosition.X + 10, (int)Main.Screen.ScreenPosition.X + Main.Screen.Width - 10), Main.Rand.Next((int)Main.Screen.ScreenPosition.Y + 10, (int)Main.Screen.ScreenPosition.Y + Main.Screen.Height - 10), 0);
                if (Main.Rand.Next(200) == 0) NPC.NewNPC<SmallTumbleweed>(SpawnPoint);
                if (Main.Rand.Next(200) == 0) NPC.NewNPC<BigTumbleweed>(SpawnPoint);
                if (Main.Rand.Next(200) == 0) NPC.NewNPC<Mummy>(SpawnPoint);
            }
        }

        public void OnWorldEnter(bool firstEnter = false)
        {
            if (firstEnter)
            {

            }
            else
            {
                DrawingWorldName = true;
                _worldNameTimer = 240;
            }
        }

        public void DrawWorldName(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (_worldNameTimer > 0 ) _worldNameTimer--;
            if (_worldNameAlpha < 1f) _worldNameAlpha += 0.01f;
            if (_worldNameTimer == 0) DrawingWorldName = false;
            string worldNumber;
            switch (WorldNumber)
            {
                case 1:
                    worldNumber = "I";
                    break;
                case 2:
                    worldNumber = "II";
                    break;
                case 3:
                    worldNumber = "III";
                    break;
                //add more numbers when necessary
                default:
                    worldNumber = "";
                    break;
            }
            string worldName1 = "[ Мир " + worldNumber + " ]";
            string worldName2 = "'" + WorldName + "'";
            float scale1 = 1.5f + Main.Interface.InterfaceScale;
            float scale2 = 3f + Main.Interface.InterfaceScale;
            Vector2 size1 = Main.SpriteFont.MeasureString(worldName1);
            Vector2 size2 = Main.SpriteFont.MeasureString(worldName2);
            Utils.DrawBorderString(spriteBatch, worldName1, new Vector2(Main.Screen.Width, Main.Screen.Height) / 2 - (new Vector2(size1.X, 50) * scale1) / 2, Color.White * _worldNameAlpha, Color.Black, scale1);
            Utils.DrawBorderString(spriteBatch, worldName2, new Vector2(Main.Screen.Width, Main.Screen.Height) / 2 - (new Vector2(size2.X, size2.Y + 50) * scale2) / 2, Color.Yellow * _worldNameAlpha, Color.Black, scale2);
        }

        public void ClearWorld()
		{

		}

        public void GenerateWorld()
        {
            GenerateLand();
            GenerateObjects();
            WorldName = "Горящая Пустыня";
            WorldNumber = 1;
            Main.Player.Position = new Vector3(Main.Rand.Next(100, Main.Screen.Width - 100), Main.Rand.Next(100, Main.Screen.Height - 100), 0);
            Item.SpawnItem<ForgottenSack>(Main.Player.Position);
        }

        private void GenerateLand()
        {
            for (int i = 0; i < _tilesX; i++)
                for (int j = 0; j < _tilesY; j++)
                    TileSolid.PlaceTile<Sand>(new Vector3(i, j, 0));
        }

        private void GenerateObjects()
        {
            for (float i = 0; i < WorldLength / 32f; i++)
            {
                for (float j = 0; j < WorldWidth / 32f; j++)
                {
                    if (Main.Rand.Next(85) == 0) TileObject.PlaceTile<Cactus>(new Vector3(i, j, 0));
                    if (Main.Rand.Next(85) == 0) TileObject.PlaceTile<DesertWeed>(new Vector3(i, j, 0));
                    if (Main.Rand.Next(200) == 0)
                    {
                        TileObject.PlaceTile<Entities.Tiles.Objects.Skull>(new Vector3(i, j, 0));
                        if (i > 1 && i < (WorldLength / 32f) - 1 && Main.Rand.Next(2) == 0) TileObject.PlaceTile<Bones>(new Vector3(i + (Main.Rand.Next(2) == 0 ? 1 : -1), j, 0));
                    }
                    if (Main.Rand.Next(90) == 0)
                    {
                        switch (Main.Rand.Next(3))
                        {
                            case 0:
                                TileObject.PlaceTile<BigDeadTree>(new Vector3(i, j, 0));
                                break;
                            case 1:
                                TileObject.PlaceTile<MediumDeadTree>(new Vector3(i, j, 0));
                                break;
                            default:
                                TileObject.PlaceTile<DeadTreeStump>(new Vector3(i, j, 0));
                                break;
                        }
                    }
                }
            }
        }
    }
}
