﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities
{
    [AutoloadTextures(NPCTexture.Texture, NPCTexture.Shadow)]
    public class NPC : Entity
    {
        private bool _wasMoving = false;
        private bool _wasStanding = false;
        public bool Dead { get; set; }
        public bool ShowAggroIcon { get; set; }
        public bool ShowHealth { get; set; }
        public enum AggroType
        {
            Hostile = 0,
            Friendly,
            Neutral
        }
        public AggroType Aggro { get; set; }
        public double AggroAnimation { get { return 8.5; } }
        public double AggroAnimationFrameRate { get; set; }
        public float AggroAnimationIconY { get; set; }
        public float AggroRadius { get; set; }
        public float Knockback { get; set; }
        public float MovementSpeed { get; set; }
        public float OutLineTextureOffsetX { get; set; }
        public float OutLineTextureOffsetY { get; set; }
        public int AggroTime { get; set; }
        public int AggroTimer { get; set; }
        public int AggroX { get; set; }
        public int AttackFrames { get; set; }
        public int CurrentHealth { get; set; }
        public int Damage { get; set; }
        public int DeathAnimationSpeed { get; set; }
        public int DeathAnimationTimer { get; set; }
        public int DeathFrame { get; set; }
        public int ExtraFrames { get; set; }
        public int IdleFrames { get; set; }
        public int InvincibilityTime { get; set; }
        public int MaxDeathFrames { get; set; }
        public int MaximumHealth { get; set; }
        public int MaxInvincibilityTime { get; set; }
        public int WalkFrames { get; set; }
        public Texture2D DeathTexture { get; set; }
        public Texture2D OutLineTexture { get; set; }
        public Vector2 CorpseOffset { get; set; }
        public Vector2 CorpseSize { get; set; }
        public Vector2 Target { get; set; }

        public NPC()
        {
            Aggro = AggroType.Friendly;
            MaxInvincibilityTime = 60;
            MaximumHealth = 80;
            CurrentHealth = MaximumHealth;
            MaxInvincibilityTime = 30;
            Acceleration = 0.1f;
            ShowHealth = true;
            SetDefaults();
            Autoload();
            WhoAmI = Main.ActiveEntities.Count;
        }

        public override void Autoload()
        {
            List<object> attr = Utils.GetAllAttributes(GetType());
            if (attr != null)
            {
                if (attr.Contains(NPCTexture.Texture) && Texture == null) Texture = Utils.LoadTexture(TexturePath);
                if (attr.Contains(NPCTexture.Shadow) && ShadowTexture == null) ShadowTexture = Utils.LoadTexture(TexturePath + "_Shadow");
                if (attr.Contains(NPCTexture.Colors) && ColorsTexture == null) ColorsTexture = Utils.LoadTexture(TexturePath + "_Colors");
                if (attr.Contains(NPCTexture.Death) && DeathTexture == null) DeathTexture = Utils.LoadTexture(TexturePath + "_Death");
                if (attr.Contains(NPCTexture.Outline) && Texture != null) OutLineTexture = Utils.SwapAllColors(Texture, Color.White);
                if (ColorsTexture != null)
                {
                    ColorIndex = Main.Rand.Next(1, ColorsTexture.Height);
                    if (Texture != null) Texture = Utils.SwapColors(ColorsTexture, Texture, ColorIndex);
                    if (DeathTexture != null) DeathTexture = Utils.SwapColors(ColorsTexture, DeathTexture, ColorIndex);
                }
            }
        }

        public override void Update(GameTime gameTime)
        {
            BaseHitbox = new Rectangle((int)Position.X, (int)Position.Y - 6, Texture.Width / MaxFrames, Texture.Height / 6);
            AI();
            if (Position.X + (Texture.Width / MaxFrames) < 0 || Position.X > Main.World.WorldWidth || Position.Y + Texture.Height < 0 || Position.Y > Main.World.WorldLength)
                Despawn();
            Center = Dead ? new Vector2(Position.X, Position.Y - Position.Z) + (Direction == -1 ? new Vector2(CorpseOffset.X + CorpseSize.X / 2, -CorpseOffset.Y - CorpseSize.Y / 2) : new Vector2(Texture.Width / MaxFrames - CorpseOffset.X - (CorpseSize.X / 2), -CorpseOffset.Y - CorpseSize.Y / 2))
                : new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(TextureBox().Width / 2, -TextureBox().Height / 2);
            CanLoot = Dead && MouseOver() && Vector2.Distance(Main.Player.Center, Center) <= 50f + (CorpseSize.X / 2);
            if (CanLoot && KeyButtons.Pressed(KeyButtons.Use))
            {
                Drop();
                Despawn();
            }
            if (InvincibilityTime > 0)
            {
                InvincibilityTime--;
                Alpha = 0.8f;
            }
            else Alpha = 1f;
            PlayerCollision();
            if (NewVelocity.X > 0f) NewVelocity.X -= Acceleration * 2f;
            else if (NewVelocity.X < 0f) NewVelocity.X += Acceleration * 2f;
            else NewVelocity.X = 0f;
            if (NewVelocity.X <= 0.1f && NewVelocity.X >= -0.1f) NewVelocity.X = 0f;
            if (NewVelocity.Y > 0f) NewVelocity.Y -= Acceleration * 2f;
            else if (NewVelocity.Y < 0f) NewVelocity.Y += Acceleration * 2f;
            else NewVelocity.Y = 0f;
            if (NewVelocity.Y <= 0.1f && NewVelocity.Y >= -0.1f) NewVelocity.Y = 0f;
            MouseTextString = (ShowHealth && !Dead) ? Name + "\n" + CurrentHealth + "/" + MaximumHealth : Name;
            if (GetGroundTile() != null) TileStandingEffects();
            base.Update(gameTime);
        }

        public override void Animation()
        {
            if (!Dead)
            {
                if (Velocity == Vector2.Zero)
                {
                    _wasStanding = true;
                    if (_wasMoving)
                    {
                        AnimationTimer = 0;
                        _wasMoving = false;
                    }
                    AnimationTimer++;
                    if (AnimationTimer == AnimationSpeed)
                    {
                        Frame++;
                        AnimationTimer = 0;
                    }
                    if (Frame >= IdleFrames) Frame = 0;
                }
                else
                {
                    _wasMoving = true;
                    if (_wasStanding)
                    {
                        AnimationTimer = 0;
                        Frame = IdleFrames;
                        _wasStanding = false;
                    }
                    AnimationTimer++;
                    if (AnimationTimer == AnimationSpeed)
                    {
                        Frame++;
                        AnimationTimer = 0;
                    }
                    if (Frame >= WalkFrames + IdleFrames) Frame = IdleFrames;
                }
                if (++AggroAnimationFrameRate >= AggroAnimation)
                {
                    AggroAnimationFrameRate = 0.0;
                    AggroX++;
                    if (AggroX > 4) AggroX = 0;
                }
                AggroAnimationIconY = AggroAnimationIconY < 100f ? AggroAnimationIconY + 1f : 0f;
            }
            else Death();
        }

        public virtual void Death()
        {
            Dead = true;
            DeathAnimationTimer++;
            if (DeathFrame < MaxDeathFrames - 1)
            {
                if (DeathAnimationTimer == DeathAnimationSpeed)
                {
                    if (DeathFrame < MaxDeathFrames - 1) DeathFrame++;
                    DeathAnimationTimer = 0;
                }
            }
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!Dead)
            {
                DrawOutline(gameTime, spriteBatch);
                spriteBatch.Draw(Texture, new Vector2(Position.X, Position.Y - Position.Z), new Rectangle((Texture.Width / MaxFrames) * Frame, 0, (Texture.Width / MaxFrames), Texture.Height), Color * Alpha, 0f, new Vector2(0, Texture.Height), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth);
                if (MouseOver()) DrawAggroIcon(gameTime, spriteBatch);
            }
            else if (DeathTexture != null) DrawDeathAnimation(gameTime, spriteBatch);
        }

        public virtual void DrawOutline(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (OutLineTexture != null && Main.Settings.ShowInfo)
            {
                List<Vector2> outlineOffsets = new List<Vector2> { new Vector2(2, 0), new Vector2(0, 2) };
                for (int i = 0; i < 4; i++)
                    spriteBatch.Draw(OutLineTexture, new Vector2(Position.X, Position.Y - Position.Z) + outlineOffsets[i / 2] * Scale * (i % 2 == 0 ? 1 : -1), new Rectangle((Texture.Width / MaxFrames) * Frame, 0, (Texture.Width / MaxFrames), Texture.Height), Color.White, 0f, new Vector2(0, Texture.Height), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth - 0.00000005f);
            }
        }

        public override void DrawShadow(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (!Dead && ShadowTexture != null)
                spriteBatch.Draw(ShadowTexture, new Vector2(Position.X, Position.Y - 1f) + new Vector2((Texture.Width / MaxFrames) / 2, 0) - ShadowOffset, null, (Color.White) * (1f + (TextureOffset.Y / 50f)), 0f, new Vector2(ShadowTexture.Width, ShadowTexture.Height) / 2, Scale + (TextureOffset.Y / 80f), SpriteEffects.None, LayerDepth);
        }

        public virtual void AI() { }

        public virtual void ChangeHostility()
        {
            if (Vector2.Distance(Main.Player.Center, Center) <= AggroRadius + (TextureBox().Width / 2)) AggroTimer = AggroTime;

            if (AggroTimer > 0)
            {
                AggroTimer--;
                Aggro = AggroType.Hostile;
            }
            else Aggro = AggroType.Neutral;
        }

        public virtual void DrawDeathAnimation(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(DeathTexture, new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(Direction == 1 ? (Texture.Width / MaxFrames) : DeathTexture.Width, 2), new Rectangle(0, (DeathTexture.Height / MaxDeathFrames) * DeathFrame, DeathTexture.Width, DeathTexture.Height / MaxDeathFrames), Color * Alpha, 0f, new Vector2(DeathTexture.Width, DeathTexture.Height / MaxDeathFrames), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth);
        }

        public virtual void DrawAggroIcon(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ShowAggroIcon)
            {
                Texture2D AggroIcon = Main.Content.Load<Texture2D>("Images/UI/AggroIcons");
                int height = 12, width = 12;
                Rectangle rect = new Rectangle(width * AggroX, height * (int)Aggro, width, height);
                float k = AggroAnimationIconY / 50f;
                float frequency = 5f;
                float number = (float)Math.PI * frequency * k;
                Vector2 velocity = new Vector2(0f, (float)Math.Sin(number));
                spriteBatch.Draw(AggroIcon, new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(width / 2 + 5, -6 - Texture.Height - AggroIcon.Height / 4) + velocity, rect, Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, LayerDepth);
            }
        }

        public virtual void Despawn()
        {
            Main.ActiveEntities.RemoveAt(WhoAmI);
            for (int i = 0; i < Main.ActiveEntities.Count; i++)
                Main.ActiveEntities[i].WhoAmI = i;
            Main.Player.CanLoot = false;
        }

        public override System.Drawing.Rectangle TextureBox()
        {
            if (Dead)
                return new System.Drawing.Rectangle((int)(Position.X + (Direction == -1 ? CorpseOffset.X : (Texture.Width / MaxFrames - CorpseSize.X - CorpseOffset.X)) - Main.Screen.ScreenPosition.X), (int)(Position.Y + CorpseOffset.Y - Main.Screen.ScreenPosition.Y - (DeathTexture.Height / MaxDeathFrames)), (int)CorpseSize.X, (int)CorpseSize.Y);
            else return base.TextureBox();
        }

        public virtual void Drop() { }

        public static void NewNPC<T>(Vector3 Position) where T : NPC, new()
        {
            Main main = Main.Instance;
            NPC npc = new T();
            npc.Position = Position;
            npc.Direction = main.Rand.Next(2) == 0 ? 1 : -1;
            main.ActiveEntities.Insert(npc.WhoAmI, npc);
        }

        public virtual void PlayerCollision()
        {
            if (BaseHitbox.Intersects(Main.Player.BaseHitbox))
            {
                if (!Dead && Aggro == AggroType.Hostile)
                {
                    Main.Player.DamagePlayer(Damage, this, Knockback);
                    OnHitPlayer();
                }
            }
        }

        public virtual void OnHitPlayer() { }

        public virtual void DamageNPC(int damage, Entity damageDealer, float knockback)
        {
            if (!Dead && InvincibilityTime == 0)
            {
                OnHitNPC();
                damage += Main.Rand.Next(-2, 2);
                FloatingText.NewText(damage.ToString(), this, FloatingText.TextTypes.Damage);
                CurrentHealth -= damage;
                InvincibilityTime = MaxInvincibilityTime;
                Vector2 screenPos = Main.Screen.ScreenPosition;
                float hitDirectionX = Position.X - screenPos.X > damageDealer.Position.X - screenPos.X ? 1f : -1f;
                float hitDirectionY = Position.Y - screenPos.Y > damageDealer.Position.Y - screenPos.Y ? 1f : -1f;
                if (knockback > 5f) knockback = 5f;
                NewVelocity.X += knockback * hitDirectionX;
                NewVelocity.Y += knockback * hitDirectionY;
                if (CurrentHealth <= 0)
                {
                    CurrentHealth = 0;
                    Death();
                }
            }
        }

        public virtual void OnHitNPC() { }

        public virtual TileSolid GetGroundTile()
        {
            float i = Position.X / 32f;
            float j = Position.Y / 32f;
            float k = Position.Z / 32f;
            TileSolid tile = Main.World.WorldTiles[(int)i, (int)j, (int)k];
            if (tile.Position.Z == Position.Z) return tile;
            else return null;
        }

        public virtual void TileStandingEffects()  { GetGroundTile().OnNPCStanding(this); }
    }
}
