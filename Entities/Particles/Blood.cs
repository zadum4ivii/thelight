﻿namespace TheLight.Entities.Particles
{
    public class Blood : Particle
    {
        public override void SetDefaults()
        {
            MaxFrames = 2;
        }
    }
}
