﻿namespace TheLight.Entities.Particles
{
    public class SandWalking : Particle
    {
        public override void SetDefaults()
        {
            MaxFrames = 2;
        }
    }
}
