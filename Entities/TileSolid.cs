﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using TheLight.Entities.Particles;
using TheLight.Entities.Tiles.Solid;

namespace TheLight.Entities
{
    public class TileSolid : Tile
    {
        public bool CanPlaceObject { get; set; }
        public bool Solid { get; set; }
        public Vector2 BottomFrame { get; set; }
        public Vector2 BottomLeftFrame { get; set; }
        public Vector2 BottomRightFrame { get; set; }
        public Vector2 TopFrame { get; set; }
        public Vector2 TopLeftFrame { get; set; }
        public Vector2 TopRightFrame { get; set; }
        public Rectangle LeftHitbox { get; set; }
        public Rectangle RightHitbox { get; set; }
        public Rectangle TopHitbox { get; set; }
        public Rectangle BottomHitbox { get; set; }
        public Rectangle GroundHitbox { get; set; }
        public Type WalkingParticle { get; set; }

        public enum HitboxType
        {
            Left = 0,
            Right,
            Top,
            Bottom,
            None
        }

        public TileSolid()
        {
            SetDefaults();
            base.Autoload();
            RandomFrame = Main.Rand.Next(0, 2);
            TopFrame = TopLeftFrame = TopRightFrame = BottomFrame = BottomRightFrame = BottomLeftFrame = new Vector2(0, 4);
        }

        public static TileSolid PlaceTile<T>(Vector3 Position) where T : TileSolid, new()
        {
            Main Main = Main.Instance;
            TileSolid tile = new T();
            tile.Position = Position * 32f;
            tile.CanPlaceObject = true;
            tile.Solid = true;
            Main.World.WorldTiles[(int)(tile.Position.X / 32f), (int)(tile.Position.Y / 32f), (int)(tile.Position.Z / 32f)] = tile;
            return tile;
        }

        public static TileSolid PlaceTile(Vector3 Position, Type t)
        {
            Main Main = Main.Instance;
            TileSolid tile = (TileSolid)Activator.CreateInstance(t);
            tile.Position = Position * 32f;
            tile.CanPlaceObject = true;
            tile.Solid = true;
            Main.World.WorldTiles[(int)(tile.Position.X / 32f), (int)(tile.Position.Y / 32f), (int)(tile.Position.Z / 32f)] = tile;
            return tile;
        }

        public override void UpdateDepth() { LayerDepth = Position.Y / 1000000f; }

        public static void PlaceHill(Vector2 Position, int height)
        {
            Main Main = Main.Instance;
            Vector2 _position = new Vector2((int)(Position.X + Main.Screen.ScreenPosition.X) / 32, (int)(Position.Y + Main.Screen.ScreenPosition.Y) / 32);
            if (Main.World.WorldTiles[(int)_position.X, (int)_position.Y, 0].Position.Z == 0)
                for (int i = 1; i < height + 1; i++)
                    PlaceTile<Sand>(new Vector3(_position.X, _position.Y, i));
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Vector2 emptyFrame = new Vector2(0, 4);
            Vector3 tilePosition = Position / 32f;
            int x = (int)tilePosition.X;
            int y = (int)tilePosition.Y;
            int z = (int)tilePosition.Z;
            var worldTiles = Main.World.WorldTiles;
            if (TextureBox().IntersectsWith(Main.Screen.ScreenR))
            {
                UpdateDepth();
                if (z == 0)
                    TopFrame = new Vector2(0 + worldTiles[x, y, z].RandomFrame, 1); //add random
                else
                {
                    //front layer
                    if (worldTiles[x, y + 1, z] == null)
                    {
                        TopFrame = emptyFrame;
                        TopLeftFrame = emptyFrame;
                        TopRightFrame = emptyFrame;
                        BottomFrame = emptyFrame;
                        BottomLeftFrame = emptyFrame;
                        BottomRightFrame = emptyFrame;
                        //bottom block
                        if (worldTiles[x, y + 1, z - 1] != null
                            && worldTiles[x, y, z + 1] != null)
                        {
                            //bottom frames
                            BottomFrame = new Vector2(0 + worldTiles[x, y, z].RandomFrame, 3); //add random
                            //right & left edges
                            if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] == null) BottomRightFrame = new Vector2(6, 0);
                            if (x > 0 && worldTiles[x - 1, y, z] == null) BottomLeftFrame = new Vector2(6, 0);
                        }
                        //middle block
                        if (worldTiles[x, y, z + 1] != null
                            && worldTiles[x, y, z - 1] != null
                            && worldTiles[x, y + 1, z - 1] == null)
                        {
                            //bottom frames
                            BottomFrame = new Vector2(worldTiles[x, y, z].RandomFrame, 2); //add random
                            //right & left edges
                            if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] == null) BottomRightFrame = new Vector2(5, 0);
                            if (x > 0 && worldTiles[x - 1, y, z] == null) BottomLeftFrame = new Vector2(5, 0);
                        }
                        //top block
                        if (worldTiles[x, y, z + 1] == null)
                        {
                            //bottom frames
                            BottomFrame = new Vector2(4 + worldTiles[x, y, z].RandomFrame, 2); //add random
                            //if there're blocks higher on the right or left edge
                            if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] != null
                                && worldTiles[x + 1, y, z + 1] != null) BottomFrame = new Vector2(3, 2);
                            if (x > 0 && worldTiles[x - 1, y, z] != null
                                && worldTiles[x - 1, y, z + 1] != null) BottomFrame = new Vector2(6, 2);
                            //right & left edge
                            if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] == null) BottomRightFrame = new Vector2(4, 0);
                            if (x > 0 && worldTiles[x - 1, y, z] == null) BottomLeftFrame = new Vector2(4, 0);
                            //right corner
                            // * *
                            //   *
                            if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] != null
                                && worldTiles[x + 1, y + 1, z] != null
                                && worldTiles[x, y + 1, z] == null)
                            {
                                BottomFrame = new Vector2(7, 2);
                                BottomRightFrame = emptyFrame;
                            }
                            //left corner
                            // * *
                            // *
                            if (x > 0 && worldTiles[x - 1, y, z] != null
                                && worldTiles[x - 1, y + 1, z] != null
                                && worldTiles[x, y + 1, z] == null)
                            {
                                BottomFrame = new Vector2(2, 2);
                                BottomLeftFrame = emptyFrame;
                            }
                            //if it's 1 block high
                            if (worldTiles[x, y + 1, z - 1] != null)
                            {
                                BottomFrame = new Vector2(4 + worldTiles[x, y, z].RandomFrame, 1); //add random
                                //if there're blocks higher on the right & left edge
                                if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] != null
                                    && worldTiles[x + 1, y, z + 1] != null) BottomFrame = new Vector2(3, 1);
                                if (x > 0 && worldTiles[x - 1, y, z] != null
                                    && worldTiles[x - 1, y, z + 1] != null) BottomFrame = new Vector2(6, 1);
                                //right & left edges
                                if (worldTiles[x + 1, y, z] == null) BottomRightFrame = new Vector2(7, 0);
                                if (x > 0 && worldTiles[x - 1, y, z] == null) BottomLeftFrame = new Vector2(7, 0);
                                //right corner
                                // * *
                                //   *
                                if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] != null
                                    && worldTiles[x + 1, y + 1, z] != null
                                    && worldTiles[x, y + 1, z] == null)
                                {
                                    BottomFrame = new Vector2(7, 1);
                                    BottomRightFrame = emptyFrame;
                                }
                                //left corner
                                // * *
                                // *
                                if (x > 0 && worldTiles[x - 1, y, z] != null
                                    && worldTiles[x - 1, y + 1, z] != null
                                    && worldTiles[x, y + 1, z] == null)
                                {
                                    BottomFrame = new Vector2(2, 1);
                                    BottomLeftFrame = emptyFrame;
                                }
                            }
                            //top frames
                            TopFrame = new Vector2(worldTiles[x, y, z].RandomFrame, 1); //add random
                            TopLeftFrame = emptyFrame;
                            TopRightFrame = emptyFrame;
                            //right & left edge
                            if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] == null) TopRightFrame = new Vector2(3, 0);
                            if (x > 0 && worldTiles[x - 1, y, z] == null) TopLeftFrame = new Vector2(3, 0);
                            //if there're no tiles behind
                            if (worldTiles[x, y - 1, z] == null)
                            {
                                TopFrame = new Vector2(worldTiles[x, y, z].RandomFrame, 0); //add random
                                //right & left corners
                                if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] == null
                                    && worldTiles[x, y - 1, z] == null
                                    && worldTiles[x + 1, y - 1, z] == null) TopRightFrame = new Vector2(2, 0);
                                if (x > 0 && worldTiles[x - 1, y, z] == null
                                    && worldTiles[x, y - 1, z] == null
                                    && worldTiles[x - 1, y - 1, z] == null) TopLeftFrame = new Vector2(2, 0);
                            }
                        }
                    }
                    //back layer
                    else
                    {
                        TopFrame = emptyFrame;
                        TopLeftFrame = emptyFrame;
                        TopRightFrame = emptyFrame;
                        BottomFrame = emptyFrame;
                        BottomLeftFrame = emptyFrame;
                        BottomRightFrame = emptyFrame;
                        //top block
                        if (worldTiles[x, y, z + 1] == null)
                        {
                            TopFrame = new Vector2(worldTiles[x, y, z].RandomFrame, 1); //add random
                            //top frames
                            //right & left edges
                            if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] == null) TopRightFrame = new Vector2(3, 0);
                            if (x > 0 && worldTiles[x - 1, y, z] == null) TopLeftFrame = new Vector2(3, 0);
                            //if there're no tiles behind
                            if (y > 0 && worldTiles[x, y - 1, z] == null)
                            {
                                TopFrame = new Vector2(worldTiles[x, y, z].RandomFrame, 0); //add random
                                //right & left corners
                                if (x < Main.World.WorldLength / 32f - 1 && worldTiles[x + 1, y, z] == null
                                    && worldTiles[x, y - 1, z] == null
                                    && worldTiles[x + 1, y - 1, z] == null) TopRightFrame = new Vector2(2, 0);
                                if (x > 0 && worldTiles[x - 1, y, z] == null
                                    && worldTiles[x, y - 1, z] == null
                                    && worldTiles[x - 1, y - 1, z] == null) TopLeftFrame = new Vector2(2, 0);
                            }
                        }
                    }
                }
                if (Main.Settings.EnableTilePlacement) Color = MouseOver() ? Color.Red : Color.White;
                else if (!Main.Settings.EnableTilePlacement && Color == Color.Red) Color = Color.White;
                //drawing
                //top
                if (TopFrame != emptyFrame)
                    spriteBatch.Draw(Texture, new Vector2(Position.X, Position.Y - Position.Z), new Rectangle(2 + (Texture.Width / (int)MaxFrames.X) * (int)TopFrame.X, 2 + (Texture.Height / (int)MaxFrames.Y) * (int)TopFrame.Y, Texture.Width / (int)MaxFrames.X - 2, Texture.Height / (int)MaxFrames.Y - 2), Color * Alpha, 0f, new Vector2(0, Texture.Height / MaxFrames.Y - 2), Scale, SpriteEffects.None, LayerDepth);
                //top right
                if (TopRightFrame != emptyFrame)
                    spriteBatch.Draw(Texture, new Vector2(Position.X + 32f, Position.Y - Position.Z), new Rectangle(2 + (Texture.Width / (int)MaxFrames.X) * (int)TopRightFrame.X, 2 + (Texture.Height / (int)MaxFrames.Y) * (int)TopRightFrame.Y, Texture.Width / (int)MaxFrames.X - 2, Texture.Height / (int)MaxFrames.Y - 2), Color * Alpha, 0f, new Vector2(0, Texture.Height / MaxFrames.Y - 2), Scale, SpriteEffects.None, LayerDepth );
                //top left
                if (TopLeftFrame != emptyFrame)
                    spriteBatch.Draw(Texture, new Vector2(Position.X - 32f, Position.Y - Position.Z), new Rectangle(2 + (Texture.Width / (int)MaxFrames.X) * (int)TopLeftFrame.X, 2 + (Texture.Height / (int)MaxFrames.Y) * (int)TopLeftFrame.Y, Texture.Width / (int)MaxFrames.X - 2, Texture.Height / (int)MaxFrames.Y - 2), Color * Alpha, 0f, new Vector2(0, Texture.Height / MaxFrames.Y - 2), Scale, SpriteEffects.FlipHorizontally, LayerDepth);
                //bottom
                if (BottomFrame != emptyFrame)
                    spriteBatch.Draw(Texture, new Vector2(Position.X, Position.Y - Position.Z + 32f), new Rectangle(2 + (Texture.Width / (int)MaxFrames.X) * (int)BottomFrame.X, 2 + (Texture.Height / (int)MaxFrames.Y) * (int)BottomFrame.Y, Texture.Width / (int)MaxFrames.X - 2, Texture.Height / (int)MaxFrames.Y - 2), Color * Alpha, 0f, new Vector2(0, Texture.Height / MaxFrames.Y - 2), Scale, SpriteEffects.None, LayerDepth);
                //bottom right
                if (BottomRightFrame != emptyFrame)
                    spriteBatch.Draw(Texture, new Vector2(Position.X + 32f, Position.Y - Position.Z + 32f), new Rectangle(2 + (Texture.Width / (int)MaxFrames.X) * (int)BottomRightFrame.X, 2 + (Texture.Height / (int)MaxFrames.Y) * (int)BottomRightFrame.Y, Texture.Width / (int)MaxFrames.X - 2, Texture.Height / (int)MaxFrames.Y - 2), Color * Alpha, 0f, new Vector2(0, Texture.Height / MaxFrames.Y - 2), Scale, SpriteEffects.None, LayerDepth + 0.0000001f);
                //bottom left
                if (BottomLeftFrame != emptyFrame)
                    spriteBatch.Draw(Texture, new Vector2(Position.X - 32f, Position.Y - Position.Z + 32f), new Rectangle(2 + (Texture.Width / (int)MaxFrames.X) * (int)BottomLeftFrame.X, 2 + (Texture.Height / (int)MaxFrames.Y) * (int)BottomLeftFrame.Y, Texture.Width / (int)MaxFrames.X - 2, Texture.Height / (int)MaxFrames.Y - 2), Color * Alpha, 0f, new Vector2(0, Texture.Height / MaxFrames.Y - 2), Scale, SpriteEffects.FlipHorizontally, LayerDepth + 0.0000001f);
            }
            /*Utils.FillRectangle(spriteBatch, TopHitbox, Color.Red);
            Utils.FillRectangle(spriteBatch, BottomHitbox, Color.Blue);
            Utils.FillRectangle(spriteBatch, LeftHitbox, Color.Green);
            Utils.FillRectangle(spriteBatch, RightHitbox, Color.Yellow);*/
        }

        public override bool MouseOver()
        {
            Tile tile = Main.World.WorldTiles[(int)Position.X / 32, (int)Position.Y / 32, (int)Position.Z / 32];
            System.Drawing.Rectangle Rectangle = new System.Drawing.Rectangle((int)Main.Interface.Cursor.Position.X - 2, (int)Main.Interface.Cursor.Position.Y - Main.Interface.Cursor.Texture.Height, 1, 1);
            return Rectangle.IntersectsWith(tile.TextureBox());
        }

        public virtual bool IsAnyTileLeft()
        {
            Vector3 tilePosition = Position / 32f;
            int x = Math.Max(0, (int)tilePosition.X - 1);
            int y = (int)tilePosition.Y;
            int z = (int)tilePosition.Z;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public virtual bool IsAnyTileRight()
        {
            Vector3 tilePosition = Position / 32f;
            int x = (int)tilePosition.X + 1;
            if (x > Main.World.WorldWidth)
                return false;
            int y = (int)tilePosition.Y;
            int z = (int)tilePosition.Z;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public virtual bool IsAnyHighestTileTop()
        {
            Vector3 tilePosition = Position / 32f;
            int x = (int)tilePosition.X;
            int y = (int)tilePosition.Y;
            int z = (int)tilePosition.Z + 1;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public virtual bool IsAnyTileTop()
        {
            Vector3 tilePosition = Position / 32f;
            int x = (int)tilePosition.X;
            int y = Math.Max(0, (int)tilePosition.Y - 1);
            int z = (int)tilePosition.Z;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public virtual bool IsAnyTileBottom()
        {
            Vector3 tilePosition = Position / 32f;
            int x = (int)tilePosition.X;
            int y = (int)tilePosition.Y + 1;
            if (y > Main.World.WorldLength)
                return false;
            int z = (int)tilePosition.Z;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public virtual bool IsAnyTileBottomLeft()
        {
            Vector3 tilePosition = Position / 32f;
            int x = (int)tilePosition.X + 1;
            if (x > Main.World.WorldWidth)
                return false;
            int y = (int)tilePosition.Y + 1;
            if (y > Main.World.WorldLength)
                return false;
            int z = (int)tilePosition.Z;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public virtual bool IsAnyTileTopRight()
        {
            Vector3 tilePosition = Position / 32f;
            int x = Math.Max(0, (int)tilePosition.X - 1);
            int y = Math.Max(0, (int)tilePosition.Y - 1);
            int z = (int)tilePosition.Z;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public virtual bool IsAnyTileAbove()
        {
            Vector3 tilePosition = Position / 32f;
            int x = (int)tilePosition.X;
            int y = (int)tilePosition.Y;
            int z = (int)tilePosition.Z + 1;
            if (z > Main.World.WorldHeight)
                return false;
            var worldTiles = Main.World.WorldTiles;
            return !(worldTiles[x, y, z] is null);
        }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)Position.X - (int)Main.Screen.ScreenPosition.X, (int)Position.Y - (int)Position.Z - (int)32f - (int)Main.Screen.ScreenPosition.Y, (int)(Texture.Width / MaxFrames.X), 32);

        public override void Update(GameTime gameTime)
        {
            if (TextureBox().IntersectsWith(Main.Screen.ScreenR))
            {
                Vector3 tilePosition = Position / 32f;
                int i = (int)tilePosition.X;
                int j = (int)tilePosition.Y;
                int k = (int)tilePosition.Z;
                BaseHitbox = new Rectangle((int)Position.X, (int)Position.Y - 32, 32, 32);
                GroundHitbox = new Rectangle((int)Position.X, (int)Position.Y - 64, 32, 32);
                if (k > 0)
                {
                    int collidingOffsetX1 = IsAnyTileLeft() ? 4 : 0;
                    int collidingOffsetY1 = IsAnyTileTop() ? 4 : 0;
                    TopHitbox = IsAnyTileTop() ? new Rectangle() : new Rectangle((int)Position.X + 2 - collidingOffsetX1, (int)Position.Y - 32, 28 + collidingOffsetX1, 2);
                    BottomHitbox = IsAnyTileBottom() ? new Rectangle() : new Rectangle((int)Position.X + 2 - collidingOffsetX1, (int)Position.Y - 2, 28 + collidingOffsetX1, 2);
                    LeftHitbox = IsAnyTileLeft() ? new Rectangle() : new Rectangle((int)Position.X, (int)Position.Y - 30 - collidingOffsetY1, 2, 28 + collidingOffsetY1);
                    RightHitbox = IsAnyTileRight() ? new Rectangle() : new Rectangle((int)Position.X + 30, (int)Position.Y - 30 - collidingOffsetY1, 2, 28 + collidingOffsetY1);
                    
                }
                //if (PlayerStanding()) PlayerStandingEffects();
                base.Update(gameTime);
            }
        }

        public virtual void CheckPlayerCollision()
        {
        }

        public virtual void OnPlayerCollision(HitboxType hitboxType) 
        {
            switch (hitboxType)
            {
                case HitboxType.Bottom:
                    {
                        Main.Player.CanMoveUp = false;
                        Main.Player.Velocity = new Vector2(Main.Player.Velocity.X, 0f);
                    }
                    break;
                case HitboxType.Top:
                    {
                        Main.Player.CanMoveDown = false;
                        Main.Player.Velocity = new Vector2(Main.Player.Velocity.X, 0f);
                    }
                    break;
                case HitboxType.Left:
                    {
                        Main.Player.CanMoveRight = false;
                        Main.Player.Velocity = new Vector2(0f, Main.Player.Velocity.Y);
                    }
                    break;
                case HitboxType.Right:
                    {
                    }
                    break;
                default:
                    break;
            }
        }

        public virtual void OnStopPlayerCollision(HitboxType hitboxType)
        {
            switch (hitboxType)
            {
                case HitboxType.Bottom:
                    {
                        Main.Player.CanMoveUp = true;
                    }
                    break;
                case HitboxType.Top:
                    {
                        Main.Player.CanMoveDown = true;
                    }
                    break;
                case HitboxType.Left:
                    {
                        Main.Player.CanMoveRight = true;
                    }
                    break;
                case HitboxType.Right:
                    {
                    }
                    break;
                default:
                    break;
            }
        }

        public virtual void OnPlayerStanding() { }
        public virtual void OnNPCStanding(NPC npc) { }
    }
}
