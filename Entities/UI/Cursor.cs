﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using TheLight.Entities.NPCs;

namespace TheLight.Entities.UI
{
    public class Cursor : UIElement
    {
        public bool MouseText { set; get; }
        public Item MouseItem { get; set; }
        public Texture2D HillTexture { get; set; }
        public float HillLvl { get; set; }
        public new Vector2 Position { get; set; }

        public override void SetDefaults()
        {
            HillTexture = Main.Content.Load<Texture2D>("Images/UI/Tile");
            Color = new Color(Main.Rand.Next(1, 255), Main.Rand.Next(1, 255), Main.Rand.Next(1, 255));
            Alpha = 0.9f;
            Scale = 1f;
            HillLvl = 0;
        }

        public override void Update(GameTime gameTime)
        {
            MouseText = false;
            MouseState mouse = Mouse.GetState();
            Position = new Vector2(mouse.X, mouse.Y);
            if (KeyButtons.PressedRightMouse() && !Interface.Inventory.MouseOver() && Main.Settings.EnableEnemyPlacement) NPC.NewNPC<Mummy>(new Vector3(Position.X + Main.Screen.ScreenPosition.X, Position.Y + Main.Screen.ScreenPosition.Y, 0));
            if (KeyButtons.ScrolledWheelUp() && HillLvl < 5) HillLvl += 0.5f;
            else if (KeyButtons.ScrolledWheelDown() && HillLvl > 0) HillLvl -= 0.5f;
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (MouseItem != null)
            {
                float scale = Interface.InterfaceScale;
                Vector2 origin = new Vector2((int)(MouseItem.InventoryTexture.Width / 2) * Scale, (int)(MouseItem.InventoryTexture.Height / 2) * Scale);

                List<Vector2> outlineOffsets = new List<Vector2> { new Vector2(2, 0), new Vector2(0, 2) };
                Texture2D outline = Utils.SwapAllColors(MouseItem.InventoryTexture, Main.Interface.InterfaceColor);
                for (int i = 0; i < 4; i++)
                    spriteBatch.Draw(outline, Position + outlineOffsets[i / 2] * scale * (i % 2 == 0 ? 1 : -1), null, Color.White, 0f, origin, scale, SpriteEffects.None, 1f);

                spriteBatch.Draw(MouseItem.InventoryTexture, Position, null, Color.White, 0f, origin, scale, SpriteEffects.None, 1f);
            }
            if (Main.Settings.EnableTilePlacement)
            {
                spriteBatch.Draw(HillTexture, Position + new Vector2(Texture.Width / 2f, Texture.Height / 2f), null, Color.White * Alpha, 0f, Vector2.Zero, Scale * 0.65f, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, 1f);
                Utils.DrawBorderString(spriteBatch, "Hill Height: " + ((int)HillLvl).ToString(), Position + new Vector2(Texture.Width / 2f, Texture.Height / 2f) + new Vector2(HillTexture.Width / 1.75f, HillTexture.Height / 2.5f), Color.Black, Color.White, 1f);
            }
            spriteBatch.Draw(Texture, Position, null, Color * Alpha, 0f, Vector2.Zero, Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, 1f);
            if (MouseItem != null)
            {
                int CurrentStack = MouseItem.CurrentStack;
                Utils.DrawBorderString(spriteBatch, CurrentStack.ToString(), Position - new Vector2((float)MouseItem.InventoryTexture.Width * 0.8f, (float)MouseItem.InventoryTexture.Height * 1.2f) - new Vector2(2f, 0f), Color.Black, Color.White, 0.75f);
            }
        }
    }
}
