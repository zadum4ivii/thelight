﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TheLight.Entities.UI
{
    public class TimeIcon : UIElement
    {
        public bool Controled { get; set; }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (Main.Interface.TimeBar == null)
                return;
            MouseState mouse = Mouse.GetState();
            Vector2 iconSize = new Vector2(23, 9);
            Vector2 barPosition = Main.Interface.TimeBar.Position;
            Vector2 startPosition = barPosition + iconSize * Scale;
            Vector2 endPosition = barPosition + (new Vector2(Main.Interface.TimeBar.Texture.Width, 0) - iconSize) * Scale;
            if (MouseOver() && KeyButtons.PressedLeftMouse() && KeyButtons.PressingLeftMouse())
                Controled = true;
            if (mouse.Y >= Position.Y - Texture.Height / 3 && mouse.Y <= (Position.Y + Texture.Height / 3) && Controled && KeyButtons.PressingLeftMouse() && Main.Settings.ShowInfo)
            {
                Scale *= 1.1f;
                Position = new Vector2(mouse.X, Position.Y);
                if (Position.X < startPosition.X)
                    Position = new Vector2(startPosition.X, Position.Y);
                else if (Position.X > endPosition.X)
                    Position = new Vector2(endPosition.X, Position.Y);
                float Percent = (Position.X - startPosition.X) / (endPosition.X - startPosition.X);
                Main.World.Time = (int)(Main.World.DayTime * Percent);
            }
            else
            {
                Controled = false;
                Vector2 Movement = (endPosition - startPosition) * (float)(Main.World.Time / Main.World.DayTime);
                Position = startPosition + Movement - new Vector2(0f, Movement.Y);
            }
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, new Rectangle(0, (Texture.Height / 2) * (Main.World.Day || Main.World.Dawn ? 0 : 1), Texture.Width, Texture.Height / 2), Color.White, 0f, new Vector2(Texture.Width, Texture.Height / 2) / 2, Scale, SpriteEffects.None, 0f);
        }
    }
}
