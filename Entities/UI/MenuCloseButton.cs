﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System.Windows.Forms;

namespace TheLight.Entities.UI
{
    public class MenuCloseButton : UIElement
    {
        public override void SetDefaults() 
        { 
            Name = "Выйти из игры";
            HoverSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonHover");
            PressSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonPress");
        }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)(Position.X + 4 * Scale), (int)(Position.Y + 4 * Scale), (int)(14 * Scale), (int)((14 * Scale)));

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (PressedLeftButton() && MessageBox.Show("Вы действительно хотите выйти из игры?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes) Main.Instance.Exit();
        }
    }
}
