﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;

namespace TheLight.Entities.UI
{
    public class Chat : Entity
    {
        public const int MaxMessages = 50;
        public const int MaxShowMessagesCount = 7;
        public const int MessageTime = 150;
        public List<ChatLine> Messages { get; set; }
        public List<ChatLine> ShowedMessages { get; set; }
        public bool ChatMode { get; set; }
        public string PlayerChatText { get { return _playerChatText; } set { _playerChatText = value; } }

        private string _playerChatText = "";
        private KeyboardState _oldState { get; set; }
        private int _blinker { get; set; }
        private int _blinkerState { get; set; }
        private bool _chatRelease { get; set; }
        private int _lastMessageTime { get; set; }

        public Chat()  {
            Messages = new List<ChatLine>();
            ShowedMessages = null;
        }

		public override void Update(GameTime gameTime)
        {
            if (PlayerChatText.Equals("") && !ChatMode)
                _lastMessageTime = Math.Max(0, --_lastMessageTime);
            if (!ChatMode && KeyButtons.PressedForChat(KeyButtons.Enter))
            {
                _blinkerState = 1;
                _blinker = 0;
                if (_chatRelease)
                {
                    if (!KeyButtons.PressedForChat(KeyButtons.Escape)) ChatMode = true;
                    _lastMessageTime = MessageTime;
                }
                _chatRelease = false;
            }
            else _chatRelease = true;
            if (ChatMode)
            {
                Keys[] multipleKeys;
                multipleKeys = KeyButtons.CurrentKeyState.GetPressedKeys();
                ChatBoxInput(multipleKeys);
                if (++_blinker >= 25)
                {
                    _blinkerState = _blinkerState == 0 ? 1 : 0;
                    _blinker = 0;
                }
             }
            else _blinkerState = 0;
        }

        protected void ChatBoxInput(Keys[] keys)
        {
            KeyboardState newState = KeyButtons.CurrentKeyState;
            foreach (Keys key in keys)
            {
                if (KeyButtons.PressedForChat(key))
                {
                    if (!_oldState.IsKeyDown(key))
                    {
                        Keys keyValue = key;
                        bool DN = keyValue >= Keys.D0 && keyValue <= Keys.D9;
                        bool AZ = keyValue >= Keys.A && keyValue <= Keys.Z;
                        bool NumPadN = keyValue >= Keys.NumPad0 && keyValue <= Keys.NumPad9;
                        if (DN || AZ || NumPadN)
                        {
                            string text = string.Concat(key);
                            char D = 'D';
                            if (text.Contains(D) && DN)
                                text = text.ToLower().Replace(D.ToString().ToLower(), "");
                            string numpad = "NumPad";
                            if (text.Contains(numpad) && NumPadN)
                                text = text.ToLower().Replace(numpad.ToLower(), "");
                            PlayerChatText += text;
                        }
                        float x = (float)(Main.Screen.Width - 330);
                        while (Main.SpriteFont.MeasureString(PlayerChatText).X > x)
                            PlayerChatText = PlayerChatText[0..^1];
                        switch (keyValue)
                        {
                            case Keys.OemBackslash:
                                PlayerChatText += "/";
                                break;
                            case Keys.Space:
                                PlayerChatText += " ";
                                break;
                            case Keys.OemQuestion:
                                PlayerChatText += "?";
                                break;
                            case Keys.OemOpenBrackets:
                                PlayerChatText += ")";
                                break;
                            case Keys.Back:
                                if (PlayerChatText.Length > 0)
                                    PlayerChatText = PlayerChatText[0..^1];
                                break;
                            case Keys.Enter:
                                if (!PlayerChatText.Equals("") && !PlayerChatText.Equals("|"))
                                {
                                    AddMessage(PlayerChatText.ToLower());
                                    ChatMode = false;
                                }
                                else if (_chatRelease) ChatMode = false;
                                PlayerChatText = "";
                                break;
                            case Keys.Escape:
                                if (_chatRelease)
                                    ChatMode = false;
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            _oldState = newState;
        }

        private void AddMessage(string text)
		{
            _lastMessageTime = MessageTime;
            Messages.Add(new ChatLine(text));
            ShowedMessages = Messages;
        }

        public void NewMessage(object obj)
		{
            if (obj is int || obj is float || obj is double)
            {
                if (!obj.ToString().Equals(""))
                    AddMessage(obj.ToString());
            }
            else if (obj is string)
            {
                if (!obj.Equals(""))
                    AddMessage(obj as string);
            }
            else if (obj is bool)
                AddMessage(obj.ToString());
        }

        public void NewMessage(ChatLine line)
		{
            if (!line.Text.Equals(""))
            {
                _lastMessageTime = MessageTime;
                Messages.Add(line);
                ShowedMessages = Messages;
            }
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            int x = 22;
            string chatText = PlayerChatText;
            if (_blinkerState == 1) chatText += "|";
            Utils.DrawBorderString(Main.SpriteBatch, chatText.ToLower(), new Vector2(x, 80), Color.Black, Color.White);
            int y = 100 - (!ChatMode ? 20 : 0);
            for (int i = 0; i < Messages.Count; i++)
            {
                if (i < MaxShowMessagesCount)
                    Utils.DrawBorderString(Main.SpriteBatch, ShowedMessages[i].Text, new Vector2(x, y + (i * 20)), ShowedMessages[i].TextColor, ShowedMessages[i].OutlineColor);
                else
                    ShowedMessages.RemoveAt(0);
                if (i > Messages.Count - 1) Messages.RemoveAt(0);
            }
            if (!(ShowedMessages is null) && ShowedMessages.Count > 0 && _lastMessageTime <= 0)
            {
                _lastMessageTime = MessageTime;
                ShowedMessages.RemoveAt(0);
            }
        }
    }
}
