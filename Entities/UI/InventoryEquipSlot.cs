﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheLight.ID;

namespace TheLight.Entities.UI
{
    public class InventoryEquipSlot : Slot
    {
        public enum SlotTypes
        {
            HeadArmorSlot = 0,
            BodyArmorSlot,
            LegsArmorSlot,
            NecklaceArmorSlot,
            RingArmorSlot,
            AccessoryArmorSlot
        }
        public new SlotTypes Type { get; set; }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)(Position.X + 4 * Scale), (int)(Position.Y + 4 * Scale), (int)(22 * Scale), (int)((22 * Scale)));

        public override void DrawContainedItem(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ContainedItem != null && ContainedItem.InventoryTexture != null)
            {
                base.DrawBackground(gameTime, spriteBatch);
                spriteBatch.Draw(ContainedItem.InventoryTexture, new Vector2(TextureBox().X, TextureBox().Y) + new Vector2(TextureBox().Width / 2, TextureBox().Height / 2) + new Vector2(0, 1), new Rectangle(0, 0, ContainedItem.InventoryTexture.Width, ContainedItem.InventoryTexture.Height), Color.White, 0f, new Vector2(ContainedItem.InventoryTexture.Width / 2, ContainedItem.InventoryTexture.Height / 2), Scale, SpriteEffects.None, 1f);
            }
        }

        public override bool CanPlaceItem(Item item)
        {
            bool MouseHasItem = Interface.Cursor.MouseItem != null;
            return Type switch
            {
                SlotTypes.HeadArmorSlot => MouseHasItem && item.EquipType == EquipTypeID.Head,
                SlotTypes.BodyArmorSlot => MouseHasItem && item.EquipType == EquipTypeID.Body,
                SlotTypes.LegsArmorSlot => MouseHasItem && item.EquipType == EquipTypeID.Legs,
                SlotTypes.NecklaceArmorSlot => MouseHasItem && item.EquipType == EquipTypeID.Necklace,
                SlotTypes.RingArmorSlot => MouseHasItem && item.EquipType == EquipTypeID.Ring,
                SlotTypes.AccessoryArmorSlot => MouseHasItem && item.EquipType == EquipTypeID.Accessory,
                _ => false
            };
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Frame = (int)Type;
            spriteBatch.Draw(Texture, Position, new Rectangle(0, (Texture.Height / 6) * Frame, Texture.Width, Texture.Height / 6), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
            if (ContainedItem == null) DrawHighlight(gameTime, spriteBatch);
            DrawContainedItem(gameTime, spriteBatch);
        }

        public override void DrawHighlight(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (MouseOver())
                spriteBatch.Draw(HightlightTexture, Position, new Rectangle(0, (HightlightTexture.Height / 6) * Frame, HightlightTexture.Width, HightlightTexture.Height / 6), Color * Alpha, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
        }

        public override bool MouseOver()
        {
            if (!Main.Interface.Inventory.InventoryOpened) return false;
            else return base.MouseOver();
        }
    }
}
