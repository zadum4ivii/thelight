﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheLight.Entities.UI
{
    public class LeftHandHotbar : Slot
    {
        public override void SetDefaults() { SlotBackground = Main.Content.Load<Texture2D>("Images/UI/Hotbar_Background"); }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)Position.X + (int)((Selected ? 4 : 6) * Scale), (int)Position.Y + (int)((Selected ? 4 : 6) * Scale), Selected ? (int)(22 * Scale) : (int)(18 * Scale), Selected ? (int)(22 * Scale) : (int)(18 * Scale));

        public override void DrawContainedItem(GameTime gameTime, SpriteBatch spriteBatch)
        {
            float DrawScale = Selected ? Scale : 1.35f;
            if (ContainedItem != null && ContainedItem.InventoryTexture != null)
            {
                if (SlotBackground != null) DrawBackground(gameTime, spriteBatch);
                DrawContainedItemAmount(gameTime, spriteBatch);
                spriteBatch.Draw(ContainedItem.InventoryTexture, new Vector2(TextureBox().X, TextureBox().Y) + new Vector2(TextureBox().Width / 2, TextureBox().Height / 2) + new Vector2(0, Selected ? 1 : 0), new Rectangle(0, 0, ContainedItem.InventoryTexture.Width, ContainedItem.InventoryTexture.Height), Color.White, 0f, new Vector2(ContainedItem.InventoryTexture.Width / 2, ContainedItem.InventoryTexture.Height / 2), DrawScale, SpriteEffects.None, 1f);
            }
        }

        public override Vector2 DrawContainedItemAmount(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Item CItem = ContainedItem;
            if (CItem == null)
                return Vector2.Zero;
            Texture2D ITexture = ContainedItem.InventoryTexture;
            float DrawScale = Selected ? Scale : 1.15f;
            return Utils.DrawBorderString(spriteBatch, CItem.CurrentStack == 0 ? "" : CItem.CurrentStack.ToString(), Position + new Vector2((float)ITexture.Width * (Selected ? 0.3f : 0.5f), -(float)ITexture.Height * (Selected ? 0.2f : 0.05f)), Color.Black, Color.White, DrawScale * 0.6f);
        }

        public override void DrawBackground(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(SlotBackground, new Vector2(Position.X, Position.Y), new Rectangle(Selected ? SlotBackground.Width / 2 : 0, 0, SlotBackground.Width / 2, SlotBackground.Height), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, new Rectangle(Selected ? Texture.Width / 2 : 0, 0, Texture.Width / 2, Texture.Height), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
            if (ContainedItem == null) DrawHighlight(gameTime, spriteBatch);
            DrawContainedItem(gameTime, spriteBatch);
        }

        public override void UpdatePosition()
        {
            Position = this == Interface.LeftHandHotbar1 ? Interface.HealthBar.Position - (new Vector2((Texture.Width / 2) + 2, -2) * Scale)
                : Interface.LeftHandHotbar1.Position - (new Vector2((Interface.LeftHandHotbar2.Texture.Width / 2), 0) * Interface.LeftHandHotbar2.Scale);
        }
    }
}
