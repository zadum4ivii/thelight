﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheLight.Entities.UI
{
    public class InventoryItemSlot : Slot
    {
        public int InventoryPosX { get; set; }
        public int InventoryPosY { get; set; }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)Position.X, (int)Position.Y, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            DrawContainedItem(gameTime, spriteBatch);
            DrawContainedItemAmount(gameTime, spriteBatch);
        }

        public override bool MouseOver()
        {
            if (!Main.Interface.Inventory.InventoryOpened) return false;
            else return base.MouseOver();
        }

        public override void UpdatePosition() { Position = Interface.Inventory.Position + new Vector2(8 * Scale, 22 * Scale) + new Vector2((Texture.Width + 2) * Scale * InventoryPosX, (Texture.Height + 2) * Scale * InventoryPosY); }
    }
}
