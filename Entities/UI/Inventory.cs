﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;

namespace TheLight.Entities.UI
{
    public class Inventory : UIElement
    {
        public bool InventoryOpened { get; set; }
        public InventoryEquipSlot AccessoryArmorSlot { get; set; }
        public InventoryEquipSlot BodyArmorSlot { get; set; }
        public InventoryEquipSlot HeadArmorSlot { get; set; }
        public InventoryEquipSlot LegsArmorSlot { get; set; }
        public InventoryEquipSlot NecklaceArmorSlot { get; set; }
        public InventoryEquipSlot RingArmorSlot { get; set; }
        public InventoryScrollbar InventoryScrollbar { get; set; }
        public InventoryScrollbarButton InventoryScrollbarButton { get; set; }
        public List<Slot> InventorySlots { get; set; }
        public static Inventory Instance { get; private set; }

        public Inventory()
        {
            Instance = this;
            InventoryOpened = false;
            InventorySlots = new List<Slot> { };
            InventoryScrollbar = new InventoryScrollbar();
            InventoryScrollbarButton = new InventoryScrollbarButton();
        }

        public override System.Drawing.Rectangle TextureBox()
        {
            return InventoryOpened ? new System.Drawing.Rectangle((int)Position.X, (int)Position.Y, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale))
                : new System.Drawing.Rectangle(0, 0, 0, 0);
        }

        public System.Drawing.Rectangle WindowTextureBox()
        {
            return InventoryOpened ? new System.Drawing.Rectangle((int)Position.X + (int)(144 * Scale), (int)Position.Y + (int)(48 * Scale), (int)(54 * Scale), (int)(82 * Scale))
                : new System.Drawing.Rectangle(0, 0, 0, 0);
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (InventoryOpened)
                spriteBatch.Draw(Texture, Position, new Rectangle(0, 0, Texture.Width, Texture.Height), Color * Alpha, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
        }

        public void AddSlots()
        {
            for (int j = 0; j < 6; j++)
                for (int i = 0; i < 4; i++)
                {
                    InventoryItemSlot s = new InventoryItemSlot();
                    Interface.InterfaceElements.Add(s);
                    s.InventoryPosX = i;
                    s.InventoryPosY = j;
                    s.Scale = Scale;
                }
            HeadArmorSlot = new InventoryEquipSlot();
            HeadArmorSlot.Type = InventoryEquipSlot.SlotTypes.HeadArmorSlot;
            BodyArmorSlot = new InventoryEquipSlot();
            BodyArmorSlot.Type = InventoryEquipSlot.SlotTypes.BodyArmorSlot;
            LegsArmorSlot = new InventoryEquipSlot();
            LegsArmorSlot.Type = InventoryEquipSlot.SlotTypes.LegsArmorSlot;
            NecklaceArmorSlot = new InventoryEquipSlot();
            NecklaceArmorSlot.Type = InventoryEquipSlot.SlotTypes.NecklaceArmorSlot;
            RingArmorSlot = new InventoryEquipSlot();
            RingArmorSlot.Type = InventoryEquipSlot.SlotTypes.RingArmorSlot;
            AccessoryArmorSlot = new InventoryEquipSlot();
            AccessoryArmorSlot.Type = InventoryEquipSlot.SlotTypes.AccessoryArmorSlot;
        }

        public override void UpdatePosition()
        {
            Position = new Vector2((Main.Screen.Width / 2) - (Texture.Width * Scale) / 2, Main.Screen.Height / 2 - (Texture.Height * Scale) / 2);
            foreach (Slot s in InventorySlots)
                if (s.GetType() == typeof(InventoryItemSlot)) s.UpdatePosition();
            HeadArmorSlot.Position = Position + new Vector2(108 * Scale, 44 * Scale);
            BodyArmorSlot.Position = HeadArmorSlot.Position + new Vector2(0, ((BodyArmorSlot.Texture.Height / 6) - 2) * Scale);
            LegsArmorSlot.Position = BodyArmorSlot.Position + new Vector2(0, ((LegsArmorSlot.Texture.Height / 6) - 2) * Scale);
            NecklaceArmorSlot.Position = HeadArmorSlot.Position + new Vector2(94 * Scale, 0);
            RingArmorSlot.Position = BodyArmorSlot.Position + new Vector2(94 * Scale, 0);
            AccessoryArmorSlot.Position = LegsArmorSlot.Position + new Vector2(94 * Scale, 0);
        }

        public virtual Slot FindItemSlot(Item item)
        {
            foreach (Slot s in InventorySlots)
                if (s.ContainedItem == item) return s;
            return null;
        }
    }
}
