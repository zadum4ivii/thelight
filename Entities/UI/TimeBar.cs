﻿using Microsoft.Xna.Framework;

namespace TheLight.Entities.UI
{
    public class TimeBar : UIElement
    {
        public override void SetDefaults() { base.SetDefaults(); }

        public override void UpdatePosition() { Position = new Vector2((Main.Screen.Width / 2) - (Texture.Width / 2 * Scale), Main.Interface.MenuOpenButton.Position.Y); }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            GameCurrentTime _currentTime = new GameCurrentTime();
            _currentTime.Init();
            string CurrentHour = _currentTime.Hours.ToString();
            MouseTextString = "Время: " + (_currentTime.Hours < 9 ? ("0") : "") + CurrentHour + $":{_currentTime.Minutes}:{_currentTime.Seconds}";
        }

        struct GameCurrentTime
        {
            public int Hours, Minutes, Seconds;
            public void Init()
            {
                double _hours = (double)Main.Instance.World.Time / Main.Instance.World.DayTime * 24.0; //находим текущий час со знаком плавающей запятой
                Hours = (int)_hours; //находим текущий час без плавающей запятой
                double _minutes = _hours - (double)Hours; //находим значения после запятой (0.1, 0.2, 0.3 и прочее (до 1.0))
                double CurrentMinute = _minutes * 60.0;
                if (Hours > 23)
                    Hours = 0;
                Minutes = (int)CurrentMinute; //умножаем на 60 и получаем минуты
                double _seconds = CurrentMinute % 60.0 * 60.0;
                Seconds = (int)_seconds - Minutes * 60;
            }
        }
    }
}
