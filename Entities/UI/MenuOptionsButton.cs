﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using System.Diagnostics;

namespace TheLight.Entities.UI
{
    public class MenuOptionsButton : UIElement
    {
        public override void SetDefaults() 
        { 
            Name = "Настройки";
            PressSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonPress");
            HoverSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonHover");
        }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)(Position.X + 4 * Scale), (int)(Position.Y + 4 * Scale), (int)(14 * Scale), (int)((14 * Scale)));

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (PressedLeftButton())
            {
                Process myProcess = new Process();
                myProcess.StartInfo.UseShellExecute = true;
                myProcess.StartInfo.FileName = "https://www.youtube.com/watch?v=dQw4w9WgXcQ";
                myProcess.Start();
            }
        }
    }
}
