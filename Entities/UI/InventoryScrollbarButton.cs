﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace TheLight.Entities.UI
{
    public class InventoryScrollbarButton : UIElement
    {
        public bool Controlled { get; set; }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (Main.Interface.Inventory.InventoryScrollbar == null)
                return;

            MouseState mouse = Mouse.GetState();
            InventoryScrollbar scrollbar = Main.Interface.Inventory.InventoryScrollbar;
            Vector2 startPosition = scrollbar.Position + new Vector2(5, 20) * Scale;
            Vector2 endPosition = scrollbar.Position + (new Vector2(0, scrollbar.Texture.Height - 8 - Texture.Height / 2) * Scale);
            if (MouseOver() && KeyButtons.PressedLeftMouse() && KeyButtons.PressingLeftMouse())
                Controlled = true;
            if (Controlled && KeyButtons.PressingLeftMouse())
            {
                Scale *= 1.25f;
                Position = new Vector2(Position.X, mouse.Y);
            }
            else
            {
                Controlled = false;
                if (Main.Interface.Inventory.MouseOver())
                {
                    if (KeyButtons.ScrolledWheelUp()) Position -= new Vector2(0, 4 * Scale);
                    else if (KeyButtons.ScrolledWheelDown()) Position += new Vector2(0, 4 * Scale);
                }
            }

            if (Position.Y < startPosition.Y)
                Position = new Vector2(Position.X, startPosition.Y);
            else if (Position.Y > endPosition.Y)
                Position = new Vector2(Position.X, endPosition.Y);

        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, new Rectangle(0, 0, Texture.Width, Texture.Height), Color.White, 0f, new Vector2(Texture.Width, Texture.Height) / 2, Scale, SpriteEffects.None, 0f);
        }

        public override void UpdatePosition()
        {
            InventoryScrollbar scrollbar = Main.Interface.Inventory.InventoryScrollbar;
            Position = scrollbar.Position + new Vector2(5, 20) * Scale;
        }
    }
}
