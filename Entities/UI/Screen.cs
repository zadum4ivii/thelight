﻿using Microsoft.Xna.Framework;
using TheLight.Entities;

namespace TheLight.UI
{
    public class Screen : Entity
    {
        public float CameraX { get; private set; }
        public float CameraY { get; private set; }
        public int Height { get; set; }
        public int Width { get; set; }
        public static Screen Instance { get; private set; }
        public System.Drawing.Rectangle ScreenR { get; private set; }
        public Vector2 ScreenPosition { get; private set; }

        public Screen()
        {
            Width = 800;
            Height = 600;
            Instance = this;
        }

        public override void Update(GameTime gameTime)
        {
            int x = (int)Main.Player.Center.X - (int)ScreenPosition.X - Width / 2;
            int y = (int)Main.Player.Center.Y - (int)ScreenPosition.Y - Height / 2;
            int posX = MathHelper.Clamp(x, -100, Main.World.WorldWidth / 32 - 100);
            int posY = MathHelper.Clamp(y, -100, Main.World.WorldLength / 32 - 100);
            ScreenR = new System.Drawing.Rectangle(posX, posY, Width * 2, Height * 2);
        }

        public void UpdateCam(Player player)
        {
            CameraX = MathHelper.Clamp(Utils.AbsPow(player.Velocity.X * 5f, 1.5f), -30f, 30f);
            CameraY = MathHelper.Clamp(Utils.AbsPow(player.Velocity.Y * 5f, 1.5f), -30f, 30f);
            ScreenPosition = new Vector2(MathHelper.Clamp(player.Center.X - Width / 2 + CameraX, 0, Main.World.WorldWidth - Width), MathHelper.Clamp(player.Center.Y - Height / 2 + CameraY, 0, Main.World.WorldLength - 32 - Height));
        }
    }
}
