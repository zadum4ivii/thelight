﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheLight.Entities.UI
{
    public class HealthBar : UIElement
    {
        public int Health { get; private set; }

        public override void SetDefaults() { Texture = Main.Content.Load<Texture2D>("Images/UI/Bars"); }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)Position.X + (int)(2 * Scale), (int)Position.Y + (int)(10 * Scale), (int)(98 * Scale), (int)(14 * Scale));

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Health = Main.Player.CurrentHealth / 5;
            MouseTextString = "Здоровье: " + Main.Player.CurrentHealth + "/" + Main.Player.MaximumHealth;
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Texture2D healthTexture_0 = Main.Content.Load<Texture2D>("Images/UI/Health_0");
            Texture2D healthTexture_1 = Main.Content.Load<Texture2D>("Images/UI/Health_1");
            spriteBatch.Draw(Texture, Position, new Rectangle(0, Texture.Height / 2, Texture.Width, Texture.Height / 2), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f); ;
            for (int i = 0; i < Health; i++)
                spriteBatch.Draw(healthTexture_1, Position + new Vector2((healthTexture_1.Width * (Health - i)) - 4, 10) * Scale, new Rectangle(0, 0, healthTexture_1.Width, healthTexture_1.Height), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 0f);
            if (Health > 0)
                spriteBatch.Draw(healthTexture_0, Position + new Vector2(2, 10) * Scale, new Rectangle(0, 0, healthTexture_0.Width, healthTexture_0.Height), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 0f);
            if (Health == Main.Player.MaximumHealth / 5)
                spriteBatch.Draw(healthTexture_0, Position + new Vector2((Texture.Width) - 8, 10) * Scale, new Rectangle(0, 0, healthTexture_0.Width, healthTexture_0.Height), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 0f);

        }

        public override void UpdatePosition() { Position = new Vector2((Main.Screen.Width / 2) - (Texture.Width * Scale), Main.Screen.Height - (Texture.Height * Scale) + 10); }
    }

    public class ManaBar : UIElement
    {
        public int Mana { get; private set; }

        public override void SetDefaults() { Texture = Main.Content.Load<Texture2D>("Images/UI/Bars"); }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)Position.X - (int)(Texture.Width * Scale) + (int)(6 * Scale), (int)Position.Y + (int)(10 * Scale), (int)(98 * Scale), (int)(14 * Scale));

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            Mana = Main.Player.CurrentMana / 5;
            MouseTextString = "Мана: " + Main.Player.CurrentMana + "/" + Main.Player.MaximumMana;
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Texture2D manaTexture_0 = Main.Content.Load<Texture2D>("Images/UI/Mana_0");
            Texture2D manaTexture_1 = Main.Content.Load<Texture2D>("Images/UI/Mana_1");
            spriteBatch.Draw(Texture, Position, new Rectangle(0, 0, Texture.Width, Texture.Height / 2), Color.White, 0f, new Vector2(Texture.Width, 0), Scale, SpriteEffects.None, 1f);
            for (int i = 0; i < Mana; i++)
                spriteBatch.Draw(manaTexture_1, Position - new Vector2(manaTexture_1.Width * (Mana - i) - 4, -10) * Scale, new Rectangle(0, 0, manaTexture_1.Width, manaTexture_1.Height), Color.White, 0f, new Vector2(manaTexture_1.Width, 0), Scale, SpriteEffects.None, 0f);
            if (Mana > 0)
                spriteBatch.Draw(manaTexture_0, Position - new Vector2(2, -10) * Scale, new Rectangle(0, 0, manaTexture_0.Width, manaTexture_0.Height), Color.White, 0f, new Vector2(manaTexture_0.Width, 0), Scale, SpriteEffects.None, 0f);
            if (Mana == Main.Player.MaximumMana / 5)
                spriteBatch.Draw(manaTexture_0, Position - new Vector2((Texture.Width) - 8, -10) * Scale, new Rectangle(0, 0, manaTexture_0.Width, manaTexture_0.Height), Color.White, 0f, new Vector2(manaTexture_0.Width, 0), Scale, SpriteEffects.None, 0f);
        }

        public override void UpdatePosition() { Position = new Vector2((Main.Screen.Width / 2) + (Texture.Width * Scale), Main.Screen.Height - (Texture.Height * Scale) + 10); }
    }
}
