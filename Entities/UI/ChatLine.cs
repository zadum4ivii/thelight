﻿using Microsoft.Xna.Framework;

namespace TheLight.Entities.UI
{
	public struct ChatLine
	{
		public string Text;
		public Color TextColor;
		public Color OutlineColor;
		public int Time;

		public ChatLine(string text)
		{
			Text = text;
			TextColor = Color.Black;
			OutlineColor = Color.White;
			Time = 150;
		}

		public ChatLine(string text, Color textColor, Color outlineColor, int time = 150)
		{ 
			Text = text;
			TextColor = textColor;
			OutlineColor = outlineColor;
			Time = time;
		}
	}
}
