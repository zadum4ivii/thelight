﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;

namespace TheLight.Entities.UI
{
    public class MenuSaveButton : UIElement
    {
        public override void SetDefaults() 
        { 
            Name = "Сохранить игру"; 
            PressSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonPress");
            HoverSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonHover");
        }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)(Position.X + 4 * Scale), (int)(Position.Y + 4 * Scale), (int)(14 * Scale), (int)((14 * Scale)));

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (PressedLeftButton()) Main.SaveGame();
        }
    }
}
