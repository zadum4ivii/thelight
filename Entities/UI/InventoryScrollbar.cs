﻿using Microsoft.Xna.Framework;

namespace TheLight.Entities.UI
{
    public class InventoryScrollbar : UIElement
    {
        public override void UpdatePosition() 
        { 
            UIElement inventory = Main.Interface.Inventory;
            Position = inventory.Position + (new Vector2(inventory.Texture.Width, inventory.Texture.Height) - new Vector2(14, Texture.Height + 10)) * Scale; 
        }
    }
}
