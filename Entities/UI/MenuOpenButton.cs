﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;

namespace TheLight.Entities.UI
{
    public class MenuOpenButton : UIElement
    {
        public bool DrawCloseAnimation { get; set; }
        public bool DrawOpenAnimation { get; set; }
        public bool MenuOpened { get; set; }
        public MenuCloseButton MenuCloseButton { get; set; }
        public MenuOptionsButton MenuOptionsButton { get; set; }
        public MenuSaveButton MenuSaveButton { get; set; }

        public override void SetDefaults()
        {
            Name = "Меню";
            AnimationSpeed = 5;
            MaxFrames = 10;
            Position = new Vector2(20, 20);
            HoverSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonHover");
            PressSound = Main.Content.Load<SoundEffect>("Audio/Sounds/UIButtonPress");
        }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)(Position.X + 4 * Scale), (int)(Position.Y + 4 * Scale), (int)(18 * Scale), (int)((18 * Scale)));

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (!DrawCloseAnimation && !DrawOpenAnimation && PressedLeftButton())
            {
                DrawOpenAnimation = !MenuOpened;
                DrawCloseAnimation = MenuOpened;
            }
            if (DrawOpenAnimation)
            {
                if (Frame < MaxFrames)
                {
                    if (AnimationTimer != AnimationSpeed) AnimationTimer++;
                    if (AnimationTimer == AnimationSpeed)
                    {
                        Frame++;
                        AnimationTimer = 0;
                    }
                }
            }
            if (DrawCloseAnimation)
            {
                if (Frame > 0)
                {
                    if (AnimationTimer != AnimationSpeed) AnimationTimer++;
                    if (AnimationTimer == AnimationSpeed)
                    {
                        Frame--;
                        AnimationTimer = 0;
                    }
                }
            }
            if (Frame == MaxFrames - 1)
            {
                MenuOpened = true;
                DrawOpenAnimation = false;
            }
            else if (Frame == 0)
            {
                MenuOpened = false;
                DrawCloseAnimation = false;
            }
            if (MenuOpened)
            {
                if (MenuCloseButton == null && MenuOptionsButton == null && MenuSaveButton == null)
                {
                    MenuCloseButton = new MenuCloseButton();
                    MenuOptionsButton = new MenuOptionsButton();
                    MenuSaveButton = new MenuSaveButton();
                    UpdatePosition();
                }
            }
            if (Frame < MaxFrames - 1)
            {
                MenuCloseButton = null;
                MenuOptionsButton = null;
                MenuSaveButton = null;
            }
            if (MenuCloseButton != null) MenuCloseButton.Update(gameTime);
            if (MenuOptionsButton != null) MenuOptionsButton.Update(gameTime);
            if (MenuSaveButton != null) MenuSaveButton.Update(gameTime);
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            Texture2D buttonAnimation = Main.Content.Load<Texture2D>("Images/UI/MenuOpenButton_Animation");
            spriteBatch.Draw(buttonAnimation, Position, new Rectangle(0, (buttonAnimation.Height / MaxFrames) * Frame, buttonAnimation.Width, buttonAnimation.Height / MaxFrames), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
            if (MenuCloseButton != null) MenuCloseButton.DrawSelf(gameTime, spriteBatch);
            if (MenuOptionsButton != null) MenuOptionsButton.DrawSelf(gameTime, spriteBatch);
            if (MenuSaveButton != null) MenuSaveButton.DrawSelf(gameTime, spriteBatch);
            base.DrawSelf(gameTime, spriteBatch);
        }

        public override void UpdatePosition()
        {
            MenuCloseButton.Position = Position + new Vector2(30 * Scale, 2 * Scale);
            MenuOptionsButton.Position = MenuCloseButton.Position + new Vector2(24 * Scale, 0);
            MenuSaveButton.Position = MenuOptionsButton.Position + new Vector2(24 * Scale, 0);
        }
    }
}
