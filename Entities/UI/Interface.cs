﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;

namespace TheLight.Entities.UI
{
    public class Interface
    {
        public static Interface Instance { get; private set; }
        public Color InterfaceColor { get; private set; }
        public Cursor Cursor { get; set; }
        public float InterfaceScale { get; set; }
        public bool NeedPositionUpdate { get; set; }
        public HealthBar HealthBar { get; set; }
        public Inventory Inventory { get; set; }
        public LeftHandHotbar LeftHandHotbar1 { get; set; }
        public LeftHandHotbar LeftHandHotbar2 { get; set; }
        public LeftHandHotbar SelectedLeftHandHotbar { get; set; }
        public ManaBar ManaBar { get; set; }
        public MenuOpenButton MenuOpenButton { get; set; }
        public RightHandHotbar RightHandHotbar1 { get; set; }
        public RightHandHotbar RightHandHotbar2 { get; set; }
        public RightHandHotbar SelectedRightHandHotbar { get; set; }
        public TimeBar TimeBar { get; set; }
        public TimeIcon TimeIcon { get; set; }
        public bool OverInterface { get; set; }
        public List<UIElement> InterfaceElements { get; set; }

        public Interface()
        {
            InterfaceScale = 1.5f;
            Instance = this;
            InterfaceColor = new Color(26, 32, 33);
            InterfaceElements = new List<UIElement> { };
        }

        public void AddInterfaceElements()
        {
            Cursor = new Cursor();
            Inventory = new Inventory();
            Inventory.AddSlots();
            HealthBar = new HealthBar();
            ManaBar = new ManaBar();
            MenuOpenButton = new MenuOpenButton();
            LeftHandHotbar1 = new LeftHandHotbar();
            LeftHandHotbar1.Selected = true;
            SelectedLeftHandHotbar = LeftHandHotbar1;
            LeftHandHotbar2 = new LeftHandHotbar();
            LeftHandHotbar2.Selected = false;
            RightHandHotbar1 = new RightHandHotbar();
            RightHandHotbar1.Selected = true;
            SelectedRightHandHotbar = RightHandHotbar1;
            RightHandHotbar2 = new RightHandHotbar();
            RightHandHotbar2.Selected = false;
            TimeBar = new TimeBar();
            TimeIcon = new TimeIcon();
            NeedPositionUpdate = true;
        }

        public void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            HealthBar.DrawSelf(gameTime, spriteBatch);
            ManaBar.DrawSelf(gameTime, spriteBatch);
            MenuOpenButton.DrawSelf(gameTime, spriteBatch);
            LeftHandHotbar1.DrawSelf(gameTime, spriteBatch);
            LeftHandHotbar2.DrawSelf(gameTime, spriteBatch);
            RightHandHotbar1.DrawSelf(gameTime, spriteBatch);
            RightHandHotbar2.DrawSelf(gameTime, spriteBatch);
            if (Inventory.InventoryOpened)
            {
                Inventory.DrawSelf(gameTime, spriteBatch);
                foreach (Slot s in Inventory.InventorySlots)
                    if (s.GetType() != typeof(LeftHandHotbar) && s.GetType() != typeof(RightHandHotbar))
                        s.DrawSelf(gameTime, spriteBatch);
                Inventory.InventoryScrollbar.DrawSelf(gameTime, spriteBatch);
                Inventory.InventoryScrollbarButton.DrawSelf(gameTime, spriteBatch);
            }
            TimeBar.DrawSelf(gameTime, spriteBatch);
            TimeIcon.DrawSelf(gameTime, spriteBatch);
            Cursor.DrawSelf(gameTime, spriteBatch);
        }

        public void Update(GameTime gameTime)
        {
            HealthBar.Update(gameTime);
            ManaBar.Update(gameTime);
            MenuOpenButton.Update(gameTime);
            LeftHandHotbar1.Update(gameTime);
            LeftHandHotbar2.Update(gameTime);
            RightHandHotbar1.Update(gameTime);
            RightHandHotbar2.Update(gameTime);
            Inventory.Update(gameTime);
            foreach (Slot s in Inventory.InventorySlots)
                if (s.GetType() != typeof(LeftHandHotbar) && s.GetType() != typeof(RightHandHotbar))
                    s.Update(gameTime);
            Inventory.InventoryScrollbar.Update(gameTime);
            Inventory.InventoryScrollbarButton.Update(gameTime);
            Cursor.Update(gameTime);
            TimeBar.Update(gameTime);
            TimeIcon.Update(gameTime);
            if (NeedPositionUpdate) UpdatePositions();

            OverInterface = false;
            foreach (UIElement e in InterfaceElements)
                if (e.MouseOver() && e.GetType() != typeof(Cursor))
                {
                    OverInterface = true;
                    break;
                }
        }

        public void UpdatePositions()
        {
            HealthBar.UpdatePosition();
            ManaBar.UpdatePosition();
            LeftHandHotbar1.UpdatePosition();
            RightHandHotbar1.UpdatePosition();
            LeftHandHotbar2.UpdatePosition();
            RightHandHotbar2.UpdatePosition();
            if (MenuOpenButton.MenuOpened) MenuOpenButton.UpdatePosition();
            Inventory.UpdatePosition();
            Inventory.InventoryScrollbar.UpdatePosition();
            Inventory.InventoryScrollbarButton.UpdatePosition();
            TimeBar.UpdatePosition();
            TimeIcon.UpdatePosition();
            NeedPositionUpdate = false;
        }
    }
}
