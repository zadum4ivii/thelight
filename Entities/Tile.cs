﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities
{
    [AutoloadTextures(TileTexture.Texture)]
    public class Tile : Entity
    {
        public new Vector2 Frame { get; set; }
        public new Vector2 MaxFrames { get; set; }
        public int MaxRandomFrames { get; set; }
        public int RandomFrame { get; set; }
        public Vector3 WorldPosition {get; set; }

        public Tile()
        {
            MaxFrames = new Vector2(1, 1);
            MaxRandomFrames = 1;
            SetDefaults();
            RandomFrame = Main.Rand.Next(0, MaxRandomFrames);
        }

        public override void Autoload()
        {
            List<object> attr = Utils.GetAllAttributes(GetType());
            if (attr != null)
                if (attr.Contains(TileTexture.Texture) && Texture == null) Texture = Utils.LoadTexture(TexturePath);
        }
    }
}
