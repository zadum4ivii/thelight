﻿using TheLight.ID;

namespace TheLight.Entities.Items
{
    public class ForgottenSack : Item
    {
        public override void SetDefaults()
        {
            Name = "Забытый мешок";
            Type = ItemID.ForgottenSack;
            MaxStack = 1;
            Bag = true;
        }

        public override void DropBagItems()
        {
            SpawnItem<Knife>(Position);
            SpawnItem<TestAxe>(Position);

            for (int i = 0; i < Main.Rand.Next(5, 8); i++) SpawnItem<Coin>(Position);
            for (int i = 0; i < Main.Rand.Next(3, 5); i++) SpawnItem<WeakPotionofHealing>(Position);
        }
    }
}
