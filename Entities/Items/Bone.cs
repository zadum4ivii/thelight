﻿using TheLight.ID;

namespace TheLight.Entities.Items
{
    public class Bone : Item
    {
        public override void SetDefaults()
        {
            Name = "Кость";
            Type = ItemID.Bone;
            MaxStack = 100;
        }
    }
}
