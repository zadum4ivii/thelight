﻿using Microsoft.Xna.Framework.Audio;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities.Items
{
    [AutoloadTextures(ItemTexture.Hand)]
    public class Knife : Item
    {
        public override void SetDefaults()
        {
            Name = "Ножик";
            Type = ItemID.Knife;
            MaxStack = 1;
            DrawInHand = true;
            UseTime = 22;
            UseSound = Main.Content.Load<SoundEffect>("Audio/Sounds/ItemSwing");
        }
    }
}
