﻿using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities.Items
{
    [AutoloadTextures(ItemTexture.Head)]
    public class Hat : Item
    {
        public override void SetDefaults()
        {
            Name = "Шляпка";
            Type = ItemID.Hat;
            EquipType = EquipTypeID.Head;
        }
    }
}
