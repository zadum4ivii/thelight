﻿using TheLight.ID;
using TheLight.Attributes;
namespace TheLight.Entities.Items
{
    [AutoloadTextures(ItemTexture.Head)]
    public class BizarreHat : Item
    {
        public override void SetDefaults()
        {
            Name = "Невероятная кепка";
            Type = ItemID.BizarreHat;
            EquipType = EquipTypeID.Head;
        }
    }
}
