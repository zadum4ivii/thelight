﻿using Microsoft.Xna.Framework;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities.Items
{
    [AutoloadTextures(ItemTexture.Hand)]
    public class WeakPotionofHealing : Item
    {
        public override void SetDefaults()
        {
            Name = "Слабое зелье лечения";
            Type = ItemID.WeakPotionofHealing;
            MaxStack = 10;
            Consumable = true;
            DrawInHand = true;
            UseTime = 25;
            UseAnimation = UseAnimationID.Consuming;
            HandDrawOffset = new Vector2(0, 8);
        }

        public override void ItemUseEffects()
        {
            Main.Player.HealPlayer(15);
        }
    }
}
