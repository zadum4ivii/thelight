﻿using Microsoft.Xna.Framework;
using TheLight.ID;

namespace TheLight.Entities.Items
{
    public class Coin : Item
    {
        private bool _reverseOffset = false;

        public override void SetDefaults()
        {
            Name = "Монетка";
            MaxFrames = 6;
            AnimationSpeed = 8;
            Type = ItemID.Coin;
            MaxStack = 5;
            TextureOffset = new Vector2(0, Main.Rand.Next(1, 6));
        }

        public override void Update(GameTime gameTime)
        {
            if (TextureOffset.Y >= 5f) _reverseOffset = true;
            if (TextureOffset.Y <= 0f) _reverseOffset = false;
            TextureOffset -= new Vector2(0, _reverseOffset ? 0.1f : -0.1f);
            base.Update(gameTime);
        }
    }
}
