﻿using TheLight.ID;

namespace TheLight.Entities.Items
{
    public class ExampleItem : Item
    {
        public override void SetDefaults()
        {
            Name = "Тестовый предмет";
            Type = ItemID.ExampleItem;
            MaxStack = 1;
        }

        public override void ItemEffects() { SetEffectsList.Add(nameof(Main.Player.Speed), 1.2f); }
    }
}
