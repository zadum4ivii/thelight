﻿using TheLight.ID;

namespace TheLight.Entities.Items
{
    public class Cacti : Item
    {
        public override void SetDefaults()
        {
            Name = "Кактус";
            Type = ItemID.Cactus;
            MaxStack = 100;
        }
    }
}
