﻿using Microsoft.Xna.Framework.Audio;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities.Items
{
    [AutoloadTextures(ItemTexture.Hand)]
    public class TestAxe : Item
    {
        public override void SetDefaults()
        {
            Name = "Тестовый топор";
            Type = ItemID.TestAxe;
            MaxStack = 1;
            DrawInHand = true;
            UseTime = 50;
            UseSound = Main.Content.Load<SoundEffect>("Audio/Sounds/ItemSwing");
            HandDrawOffset = new Microsoft.Xna.Framework.Vector2(10, 10);
        }
    }
}
