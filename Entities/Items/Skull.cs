﻿using TheLight.ID;

namespace TheLight.Entities.Items
{
    public class Skull : Item
    {
        public override void SetDefaults()
        {
            Name = "Череп";
            Type = ItemID.Skull;
            MaxStack = 100;
        }
    }
}
