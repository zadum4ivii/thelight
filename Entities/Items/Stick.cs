﻿using TheLight.ID;
using TheLight.Attributes;

namespace TheLight.Entities.Items
{
    [AutoloadTextures(ItemTexture.Colors)]
    public class Stick : Item
    {
        public override void SetDefaults()
        {
            Name = "Ветка";
            Type = ItemID.Stick;
            MaxStack = 100;
        }
    }
}
