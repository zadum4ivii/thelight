﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace TheLight.Entities
{
    public class FloatingText : Entity
    {
        public Color OutlineColor { get; set; }
        public Entity Target { get; set; }
        public float AlphaIncrease { get; set; }
        public int TextTime { get; set; }
        public string Text { get; set; }
        public Vector2 Movement { get; set; }
        public new Vector2 Position { get; set; }

        public enum TextTypes
        {
            Damage = 0,
            Heal,
            DefaultText
        }
        public new TextTypes Type { get; set; }
        public FloatingText()
        {
            TextTime = 40;
            Alpha = 0f;
            WhoAmI = Main.ActiveFloatingText.Count;
        }

        public override void Update(GameTime gameTime)
        {
            Alpha += 0.05f;
            Movement += Velocity;
            Position = Movement + new Vector2(Target.Position.X, Target.Position.Y - Target.Position.Z) - new Vector2(0, Target.Texture.Height) - new Vector2(Main.SpriteFont.MeasureString(Text).Length() / 2, 0) + new Vector2(((Target.Texture.Width / Target.MaxFrames) / 2) + 6, -20);
            
            if (Frame < MaxFrames - 1)
            {
                if (AnimationTimer < 10) 
                    AnimationTimer++;
                else
                {
                    AnimationTimer = 0;
                    Frame++;
                }
            }
            if (TextTime > 0) TextTime--;
            else
            {
                Main.ActiveFloatingText.RemoveAt(WhoAmI);
                for (int i = 0; i < Main.ActiveFloatingText.Count; i++)
                    Main.ActiveFloatingText[i].WhoAmI = i;
            }
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            switch (Type)
            {
                case TextTypes.Damage:
                    Texture = Main.Content.Load<Texture2D>("Images/UI/DamageTextIcon");
                    MaxFrames = 1;
                    break;
                case TextTypes.Heal:
                    Texture = Main.Content.Load<Texture2D>("Images/UI/HealTextIcon");
                    MaxFrames = 3;
                    break;
                case TextTypes.DefaultText:
                    break;
            }
            Vector2 size = Main.SpriteFont.MeasureString(Text);
            if (Texture != null)
                spriteBatch.Draw(Texture, Position - Main.Screen.ScreenPosition, new Rectangle((Texture.Width / MaxFrames ) * Frame, 0, (Texture.Width / MaxFrames), Texture.Height), Color.White * Alpha, 0f, Vector2.Zero, 1f, SpriteEffects.None, Target.LayerDepth);
            Utils.DrawBorderString(spriteBatch, Text, Position - Main.Screen.ScreenPosition + new Vector2(Texture.Width / MaxFrames + 6, 0), Color * Alpha, OutlineColor * Alpha);
        }

        public static void NewText(string text, Entity target, TextTypes textType = TextTypes.DefaultText)
        {
            Main Main = Main.Instance;
            FloatingText floatingText = new FloatingText();
            switch (textType)
            {
                case TextTypes.Damage:
                    floatingText.Color = new Color(114, 6, 15);
                    floatingText.OutlineColor = Color.White;
                    break;
                case TextTypes.Heal:
                    floatingText.Color = new Color(9, 94, 54);
                    floatingText.OutlineColor = Color.White;
                    break;
                case TextTypes.DefaultText:
                    floatingText.Color = Color.Black;
                    floatingText.OutlineColor = Color.White;
                    break;
            }
            floatingText.Type = textType;
            floatingText.Text = text;
            floatingText.Target = target;
            floatingText.Velocity = new Vector2(Main.Rand.Next(-5, 5), -Main.Rand.Next(10, 15)) / 10f;
            Main.ActiveFloatingText.Insert(floatingText.WhoAmI, floatingText);
        }
    }
}
