﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities.NPCs
{
    [AutoloadTextures( NPCTexture.Colors, NPCTexture.Outline)]
    public class SmallTumbleweed : NPC
    {
        private bool _ignorePlayer = false;
        private bool _reverseOffset = false;
        private float _rotation = 0f;
        private int _maxTextureMoveOffset = 10;

        public override void SetDefaults()
        {
            Name = "Перекати-поле";
            MaxFrames = 1;
            Type = NPCID.SmallTumbleweed;
            ShowAggroIcon = false;
            ShowHealth = false;
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            DrawOutline(gameTime, spriteBatch);
            spriteBatch.Draw(Texture, new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(TextureOffset.X, -TextureOffset.Y) + new Vector2((int)Texture.Width / 2, -(int)Texture.Height / 2), new Rectangle(0, 0, Texture.Width, Texture.Height), Color * Alpha, _rotation, new Vector2(Texture.Width / 2, Texture.Height / 2), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth);
        }

        public override void DrawOutline(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (OutLineTexture != null && Main.Settings.ShowInfo)
            {
                List<Vector2> outlineOffsets = new List<Vector2> { new Vector2(2, 0), new Vector2(0, 2) };
                for (int i = 0; i < 4; i++)
                    spriteBatch.Draw(OutLineTexture, new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(TextureOffset.X, -TextureOffset.Y) + new Vector2((int)Texture.Width / 2, -(int)Texture.Height / 2) + outlineOffsets[i / 2] * Scale * (i % 2 == 0 ? 1 : -1), new Rectangle(0, 0, Texture.Width, Texture.Height), Color * Alpha, _rotation, new Vector2(Texture.Width / 2, Texture.Height / 2), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth - 0.00000005f);
            }
        }

        public override void AI()
        {
            ShadowOffset = new Vector2(0, 2);
            _rotation += 0.05f * Direction;
            if (TextureOffset.Y >= _maxTextureMoveOffset) _reverseOffset = true;
            if (TextureOffset.Y <= 0f) _reverseOffset = false;
            if (_reverseOffset) TextureOffset -= new Vector2(0, 0.5f);
            if (!_reverseOffset) TextureOffset += new Vector2(0, 0.5f);
            Velocity.X = 1f * Direction;
            Rectangle hitbox = new Rectangle((int)Position.X, (int)Position.Y - Texture.Height, Texture.Width, Texture.Height);
            Rectangle intersect = Rectangle.Intersect(hitbox, Main.Player.BaseHitbox);
            if (intersect.Width > 10) _ignorePlayer = true;
            if (intersect.Width == 0 && intersect.Height == 0) _ignorePlayer = false;
            if (!_ignorePlayer && intersect.Width != 0 && intersect.Width == 10)
            {
                Direction = -Direction;
                Velocity.X = 5f * Direction;
            }
        }
    }
}
