﻿using Microsoft.Xna.Framework;
using TheLight.Attributes;
using TheLight.Entities.Items;
using TheLight.Entities.Particles;
using TheLight.ID;

namespace TheLight.Entities.NPCs
{
    [AutoloadTextures(NPCTexture.Death, NPCTexture.Colors, NPCTexture.Outline)]
    public class Mummy : NPC
    {
        public override void SetDefaults()
        {
            Name = "Мумия";
            MaxDeathFrames = 7;
            CorpseOffset = new Vector2(20, 20);
            CorpseSize = new Vector2(48, 22);
            Type = NPCID.Mummy;
            AnimationSpeed = 8;
            DeathAnimationSpeed = 8;
            DeathAnimationTimer = 0;
            ShowAggroIcon = true;
            Aggro = AggroType.Neutral;
            AggroRadius = 90f;
            AggroTime = 180;
            IdleFrames = 8;
            WalkFrames = 6;
            MaxFrames = IdleFrames + WalkFrames + AttackFrames + ExtraFrames;
            MovementSpeed = 1f;
            Damage = 10;
            Knockback = Damage * 0.35f;
        }

        public override void AI()
        {
            OutLineTextureOffsetX = Direction == -1 ? -2f : 0f;
            if (MouseOver() && KeyButtons.PressedLeftMouse()) DamageNPC(10, Main.Player, Knockback);
            ChangeHostility();
            if (Aggro == AggroType.Hostile && !Dead) Utils.MoveTowards(this, Main.Player.Center, MovementSpeed, 1f);
            else Velocity = Vector2.Zero;
        }

        public override void Drop()
        {
            Vector3 DropPosition = new Vector3(Position.X, Position.Y, Position.Z) + new Vector3(Direction == -1 ? TextureBox().Width / 2 : -TextureBox().Width / 2, 0, 0);
            for (int i = 0; i < 5; i++)
                Item.SpawnItem<Coin>(DropPosition);
            Item.SpawnItem<Knife>(DropPosition);
            switch (Main.Rand.Next(2))
            {
                case 0:
                    Item.SpawnItem<BizarreHat>(DropPosition);
                    break;
                case 1:
                    Item.SpawnItem<Hat>(DropPosition);
                    break;
            }
        }

        public override void OnHitNPC()
        {
            for (int i = 0; i < Main.Rand.Next(8, 14); i++)
            {
                Particle p = Particle.SpawnParticle<Blood>(this);
                p.TimeLeft += i;
                p.StickToTarget = true;
            }
        }

        public override void TileStandingEffects()
        {
            base.TileStandingEffects();
            if (GetGroundTile().WalkingParticle != null && Velocity != Vector2.Zero && Main.Rand.Next(5) == 0)
            {
                Particle p = Particle.SpawnParticle(GetGroundTile().WalkingParticle, this);
                p.TimeLeft = 10;
                p.Alpha = 1f;
                p.Position = new Vector2(Position.X, Position.Y) + new Vector2(Direction == -1 ? TextureBox().Width : 0, 0);
                p.Velocity = new Vector2(0f, -Main.Rand.Next(1, 4) / 10f);
            }
        }
    }
}
