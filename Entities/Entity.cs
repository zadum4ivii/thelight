﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using TheLight.Entities.UI;

namespace TheLight.Entities
{
    public abstract class Entity
    {
        public bool CanLoot { get; set; }
        public Color Color { get; set; }
        public float Acceleration { get; set; }
        public float Alpha { get; set; }
        public float LayerDepth { get; set; }
        public float Scale { get; set; }
        public float[] Timers { get { return new float[3]; } }
        public int AnimationSpeed { get; set; }
        public int AnimationTimer { get; set; }
        public int ColorIndex { get; set; }
        public int Direction { get; set; }
        public int Frame { get; set; }
        public int MaxFrames { get; set; }
        public int Type { get; set; }
        public int WhoAmI { get; set; }
        public Interface Interface = Interface.Instance;
        public Main Main = Main.Instance;
        public Rectangle BaseHitbox { get; set; }
        public string MouseTextString { get; set; }
        public string Name { get; set; }
        public string TexturePath { get; set; }
        public string InteractionText { get; set; }
        public Texture2D ColorsTexture { get; set; }
        public Texture2D ShadowTexture { get; set; }
        public Texture2D Texture { get; set; }
        public Texture2D RecoloredTexture { get; set; }
        public Vector2 Center { get; set; }
        public Vector2 NewVelocity = new Vector2();
        public Vector2 ShadowOffset { get; set; }
        public Vector2 TextureOffset { get; set; }
        public Vector3 Position = new Vector3();
        public Vector2 Velocity = new Vector2();

        public Entity()
        {
            MaxFrames = 1;
            Color = Color.White;
            Scale = 1f;
            Alpha = 1f;
            Name = "Entity";
            Direction = 1;
            TextureOffset = new Vector2(0, 0);
            ShadowOffset = new Vector2(0, 0);
            LayerDepth = 1f;
            InteractionText = "F, чтобы собрать";
            TexturePath = @"Images\" + (GetType().ToString().Substring(18)).Replace(".", @"\");
        }

        public virtual void SetDefaults() { }

        public virtual void Autoload() { }

        public virtual void Update(GameTime gameTime)
        {
            Animation();
            UpdateDepth();
            Position += new Vector3(Velocity.X, Velocity.Y, 0);
            Position += new Vector3(NewVelocity.X, NewVelocity.Y, 0);
            if (Velocity.X < 0) Direction = -1;
            if (Velocity.X > 0) Direction = 1;
        }

        public virtual void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch) { }

        public virtual void Animation() { }

        public virtual void DrawShadow(GameTime gameTime, SpriteBatch spriteBatch) { }

        public virtual void UpdateDepth() { LayerDepth = (Position.Y / 1000000f) + (Position.Z + 3) * 0.00001f; }

        public virtual bool MouseOver()
        {
            System.Drawing.Rectangle Cursor = new System.Drawing.Rectangle((int)Interface.Cursor.Position.X, (int)Interface.Cursor.Position.Y, (int)(4 * Scale), (int)(4 * Scale));
            return Cursor.IntersectsWith(TextureBox());
        }

        public virtual bool MouseOverLargeArea()
        {
            System.Drawing.Rectangle Cursor = new System.Drawing.Rectangle((int)Interface.Cursor.Position.X, (int)Interface.Cursor.Position.Y, (int)(10 * Scale), (int)(10 * Scale));
            return Cursor.IntersectsWith(TextureBox());
        }

        public virtual System.Drawing.Rectangle TextureBox()
        {
            if (Main.Screen.ScreenPosition == null)
                return new System.Drawing.Rectangle();
            return new System.Drawing.Rectangle((int)(Position.X + TextureOffset.X - Main.Screen.ScreenPosition.X), (int)(Position.Y + TextureOffset.Y - Main.Screen.ScreenPosition.Y - Texture.Height - Position.Z), (Texture.Width / MaxFrames), Texture.Height);
        }
    }
}
