﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities
{
    [AutoloadTextures(TileTexture.Shadow)]
    public class TileObject : Tile
    {
        public Vector2 Offset { get; set; }
        public Vector2 FlippedShadowOffset { get; set; }
        public Vector2 FreeSpace { get; set; }
        public bool CanBeLooted { get; set; }

        public TileObject()
        {
            MaxFrames = new Vector2(1, 1);
            FreeSpace = Vector2.Zero;
            SetDefaults();
            Autoload();
            WhoAmI = Main.ActiveEntities.Count;
        }

        public override void Autoload()
        {
            base.Autoload();
            List<object> attr = Utils.GetAllAttributes(GetType());
            if (attr != null)
            {
                if (attr.Contains(TileTexture.Shadow) && ShadowTexture == null) ShadowTexture = Utils.LoadTexture(TexturePath + "_Shadow");
                if (attr.Contains(TileTexture.Colors) && ColorsTexture == null) ColorsTexture = Utils.LoadTexture(TexturePath + "_Colors");
                if (ColorsTexture != null)
                {
                    ColorIndex = Main.Rand.Next(1, ColorsTexture.Height);
                    if (Texture != null) RecoloredTexture = Utils.SwapColors(ColorsTexture, Texture, ColorIndex);
                }
            }
        }

        public static TileObject PlaceTile<T>(Vector3 Position) where T : TileObject, new()
        {
            Main Main = Main.Instance;
            int x = (int)Position.X;
            int y = (int)Position.Y;
            int z = (int)Position.Z;
            var worldTiles = Main.World.WorldTiles;
            bool canGenerate = true;
            if (worldTiles[x, y, z] != null && worldTiles[x, y, z].CanPlaceObject && worldTiles[x, y, z + 1] == null)
            {
                TileObject tile = new T();
                if (x > tile.FreeSpace.X && x < (Main.World.WorldWidth / 32) - tile.FreeSpace.X
                    && y > tile.FreeSpace.Y && y < (Main.World.WorldLength / 32) - tile.FreeSpace.Y)
                {
                    for (int i = 0; i < (tile.FreeSpace.X * 2) + 1; i++)
                        for (int j = 0; j < (tile.FreeSpace.Y * 2) + 1; j++)
                        {
                            if (worldTiles[x - (int)tile.FreeSpace.X + i, y - (int)tile.FreeSpace.Y + j, z + 1] != null
                                || worldTiles[x - (int)tile.FreeSpace.X + i, y - (int)tile.FreeSpace.Y + j, z] == null)
                                canGenerate = false;
                        }
                    if (canGenerate)
                    {
                        tile.Position = Position * 32f;
                        tile.Position += new Vector3(16 - (tile.Texture.Width / tile.MaxFrames.X) / 2, -12, 0) + new Vector3(tile.Offset.X, tile.Offset.Y, 0);
                        Main.ActiveEntities.Insert(tile.WhoAmI, tile);
                        tile.Direction = Main.Rand.Next(2) == 0 ? 1 : -1;
                        worldTiles[x, y, z].CanPlaceObject = false;
                        tile.WorldPosition = new Vector3(x, y, z);
                        return tile;
                    }
                }
            }
            return null;
        }

        public static TileObject PlaceTile(Vector3 Position, Type t)
        {
            Main Main = Main.Instance;
            int x = (int)Position.X;
            int y = (int)Position.Y;
            int z = (int)Position.Z;
            var worldTiles = Main.World.WorldTiles;
            bool canGenerate = true;
            if (worldTiles[x, y, z] != null && worldTiles[x, y, z].CanPlaceObject && worldTiles[x, y, z + 1] == null)
            {
                TileObject tile = (TileObject)Activator.CreateInstance(t);
                if (x > tile.FreeSpace.X && x < (Main.World.WorldWidth / 32) - tile.FreeSpace.X
                    && y > tile.FreeSpace.Y && y < (Main.World.WorldLength / 32) - tile.FreeSpace.Y)
                {
                    for (int i = 0; i < (tile.FreeSpace.X * 2) + 1; i++)
                        for (int j = 0; j < (tile.FreeSpace.Y * 2) + 1; j++)
                        {
                            if (worldTiles[x - (int)tile.FreeSpace.X + i, y - (int)tile.FreeSpace.Y + j, z + 1] != null
                                || worldTiles[x - (int)tile.FreeSpace.X + i, y - (int)tile.FreeSpace.Y + j, z] == null)
                                canGenerate = false;
                        }
                    if (canGenerate)
                    {
                        tile.Position = Position * 32f;
                        tile.Position += new Vector3(16 - (tile.Texture.Width / tile.MaxFrames.X) / 2, -12, 0) + new Vector3(tile.Offset.X, tile.Offset.Y, 0);
                        Main.ActiveEntities.Insert(tile.WhoAmI, tile);
                        tile.Direction = Main.Rand.Next(2) == 0 ? 1 : -1;
                        worldTiles[x, y, z].CanPlaceObject = false;
                        tile.WorldPosition = new Vector3(x, y, z);
                        return tile;
                    }
                }
            }
            return null;
        }

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)Position.X + (int)TextureOffset.X - (int)Main.Screen.ScreenPosition.X, (int)Position.Y + (int)TextureOffset.Y - Texture.Height - (int)Main.Screen.ScreenPosition.Y - (int)Position.Z, (int)(Texture.Width / MaxFrames.X), (int)(Texture.Height / MaxFrames.Y));

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (TextureBox().IntersectsWith(Main.Screen.ScreenR))
            {
                UpdateDepth();
                spriteBatch.Draw(RecoloredTexture == null ? Texture : RecoloredTexture, new Vector2(Position.X, Position.Y - Position.Z), new Rectangle((int)(Texture.Width / MaxFrames.X) * RandomFrame, 0, (int)(Texture.Width / MaxFrames.X), (int)(Texture.Height / MaxFrames.Y)), Color * Alpha, 0f, new Vector2(0, Texture.Height / MaxFrames.Y), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth);
            }
        }

        public override void DrawShadow(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ShadowTexture != null)
                spriteBatch.Draw(ShadowTexture, new Vector2((int)Position.X, (int)Position.Y - (int)Position.Z - 1) + new Vector2((Texture.Width / MaxFrames.X) / 2, 0) + TextureOffset - ShadowOffset + (Direction == -1 ? FlippedShadowOffset : Vector2.Zero), null, (Color.White) * (1f + (TextureOffset.Y / 50f)), 0f, new Vector2(ShadowTexture.Width, ShadowTexture.Height) / 2, Scale + (TextureOffset.Y / 80f), SpriteEffects.None, LayerDepth);
        }

        public override void Update(GameTime gameTime)
        {
            if (TextureBox().IntersectsWith(Main.Screen.ScreenR))
            {
                BaseHitbox = new Rectangle((int)Position.X, (int)Position.Y - 8, Texture.Width / (int)MaxFrames.X, 8);
                CheckPlayerCollision();
                Center = new Vector2(Position.X, Position.Y - Position.Z) + new Vector2((Texture.Width / MaxFrames.X) / 2, (-Texture.Height / MaxFrames.Y) / 2) + new Vector2(TextureOffset.X, -TextureOffset.Y);
                CanLoot = CanBeLooted && MouseOver() && Vector2.Distance(Main.Player.Center, Center) <= 20f + (Texture.Width / MaxFrames.X / 2);
                if (CanLoot && KeyButtons.Pressed(KeyButtons.Use))
                {
                    Drop();
                    Despawn();
                }
                base.Update(gameTime);
            }
        }

        public virtual void Despawn()
        {
            Main.ActiveEntities.RemoveAt(WhoAmI);
            for (int i = 0; i < Main.ActiveEntities.Count; i++)
                Main.ActiveEntities[i].WhoAmI = i;
            Main.Player.CanLoot = false;
        }

        public virtual void CheckPlayerCollision()
        {
            if (BaseHitbox.Intersects(Main.Player.BaseHitbox))
                OnPlayerCollision();
        }

        public virtual void OnPlayerCollision() { }

        public virtual void Drop() { }
    }
}
