﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities
{
    [AutoloadTextures(UITexture.SlotBackground)]
    public class Slot : UIElement
    {
        private Item _backupItem;
        public bool Selected { get; set; }
        public Item ContainedItem { get; set; }
        public Texture2D SlotBackground { get; set; }

        public Slot()
        {
            Selected = false;
            SetDefaults();
            Autoload();
            Interface.Inventory.InventorySlots.Add(this);
        }

        public override void Autoload()
        {
            base.Autoload();
            List<object> attr = Utils.GetAllAttributes(GetType());
            if (attr!= null)
                if (attr.Contains(UITexture.SlotBackground) && SlotBackground == null) SlotBackground = Utils.LoadTexture(TexturePath + "_Background");
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            base.DrawSelf(gameTime, spriteBatch);
            DrawContainedItemAmount(gameTime, spriteBatch);
            DrawContainedItem(gameTime, spriteBatch);
        }

        public virtual void DrawContainedItem(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ContainedItem != null && ContainedItem.InventoryTexture != null)
            {
                if (SlotBackground != null) DrawBackground(gameTime, spriteBatch);
                spriteBatch.Draw(ContainedItem.InventoryTexture, new Vector2(TextureBox().X, TextureBox().Y) + new Vector2(TextureBox().Width / 2, TextureBox().Height / 2) + new Vector2(0, 1), new Rectangle(0, 0, ContainedItem.InventoryTexture.Width, ContainedItem.InventoryTexture.Height), Color.White, 0f, new Vector2(ContainedItem.InventoryTexture.Width / 2, ContainedItem.InventoryTexture.Height / 2), Scale, SpriteEffects.None, 1f);
            }
        }

        public virtual void DrawBackground(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(SlotBackground, new Vector2(Position.X, Position.Y), new Rectangle(0, 0, SlotBackground.Width, SlotBackground.Height), Color.White, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
        }

        public virtual Vector2 DrawContainedItemAmount(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ContainedItem != null)
                return Utils.DrawBorderString(spriteBatch, ContainedItem.CurrentStack == 0 ? "" : ContainedItem.CurrentStack.ToString(), Position + new Vector2(0, -4), Color.Black, Color.White, 0.75f);
            return Vector2.Zero;
        }

        public virtual bool CanPlaceItem(Item item) => true;

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (ContainedItem != null) ContainedItem.UpdateInventory(gameTime, this);
            MouseTextString = ContainedItem != null ? ContainedItem.Name : "";
            if (Main.Interface.Inventory.InventoryOpened)
            {
                if (PressedLeftButton())
                {
                    if (ContainedItem != null)
                    {
                        if (Interface.Cursor.MouseItem == null)
                        {
                            Interface.Cursor.MouseItem = ContainedItem;
                            ContainedItem.RemovedFromSlot(this);
                            ContainedItem = null;
                        }
                        else if (Interface.Cursor.MouseItem != null && CanPlaceItem(Interface.Cursor.MouseItem))
                        {
                            if (Interface.Cursor.MouseItem.Type != ContainedItem.Type)
                            {
                                _backupItem = ContainedItem;
                                ContainedItem.RemovedFromSlot(this);
                                ContainedItem = Interface.Cursor.MouseItem;
                                ContainedItem.PlacedInSlot(this);
                                Interface.Cursor.MouseItem = _backupItem;
                            }
                            else if (ContainedItem.CurrentStack == 1 && ContainedItem.MaxStack > 1 && Interface.Cursor.MouseItem.CurrentStack == 1)
                            {
                                ContainedItem.CurrentStack++;
                                Interface.Cursor.MouseItem = null;
                            }
                            else
                            {
                                while (ContainedItem.CurrentStack < ContainedItem.MaxStack && Interface.Cursor.MouseItem.CurrentStack != 0)
                                {
                                    ContainedItem.CurrentStack++;
                                    if (Interface.Cursor.MouseItem.CurrentStack > 1) Interface.Cursor.MouseItem.CurrentStack--;
                                    else Interface.Cursor.MouseItem = null;
                                }
                            }
                        }
                    }
                    else
                    {
                        if (Interface.Cursor.MouseItem != null && CanPlaceItem(Interface.Cursor.MouseItem))
                        {
                            ContainedItem = Interface.Cursor.MouseItem;
                            ContainedItem.PlacedInSlot(this);
                            Interface.Cursor.MouseItem = null;
                        }
                    }
                }
                else if (PressedRightButton())
                {
                    if (ContainedItem != null)
                    {
                        if (Interface.Cursor.MouseItem == null)
                        {
                            Item i = Item.CreateItem(ContainedItem.GetType());
                            i.CurrentStack = 1;
                            Interface.Cursor.MouseItem = i;
                            if (ContainedItem.CurrentStack > 1) ContainedItem.CurrentStack--;
                            else
                            {
                                ContainedItem.RemovedFromSlot(this);
                                ContainedItem = null;
                            }
                        }
                        else
                        {
                            if (Interface.Cursor.MouseItem.Type == ContainedItem.Type && Interface.Cursor.MouseItem.CurrentStack < Interface.Cursor.MouseItem.MaxStack)
                            {
                                Interface.Cursor.MouseItem.CurrentStack++;
                                if (ContainedItem.CurrentStack > 1) ContainedItem.CurrentStack--;
                                else
                                {
                                    ContainedItem.RemovedFromSlot(this);
                                    ContainedItem = null;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
