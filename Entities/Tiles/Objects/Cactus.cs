﻿using Microsoft.Xna.Framework;
using TheLight.Entities.Items;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Objects
{
    public class Cactus : TileObject
    {
        public override void SetDefaults()
        {
            Name = "Кактус";
            ShadowOffset = new Vector2(0, 2);
            MaxFrames = new Vector2(4, 1);
            MaxRandomFrames = 4;
            Type = TileID.Cactus;
            CanBeLooted = true;
        }

        public override void Drop()
        {
            Vector3 DropPosition = new Vector3(Center.X, Center.Y, Position.Z);
            Item.SpawnItem<Cacti>(DropPosition);
        }

        public override void OnPlayerCollision() { Main.Player.DamagePlayer(5, this, 2f); }
    }
}
