﻿using Microsoft.Xna.Framework;
using TheLight.Entities.Items;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Objects
{
    public class Bones : TileObject
    {
        public override void SetDefaults()
        {
            Name = "Кости";
            ShadowOffset = new Vector2(0, 2);
            Type = TileID.Skull;
            CanBeLooted = true;
        }

        public override void Drop()
        {
            Vector3 DropPosition = new Vector3(Center.X, Center.Y, Position.Z);
            for (int i = 0; i < Main.Rand.Next(2, 4); i++) Item.SpawnItem<Bone>(DropPosition);
        }
    }
}
