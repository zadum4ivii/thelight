﻿using Microsoft.Xna.Framework;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Objects
{
    public class Skull : TileObject
    {
        public override void SetDefaults()
        {
            Name = "Череп";
            ShadowOffset = new Vector2(0, 2);
            Type = TileID.Skull;
            CanBeLooted = true;
        }

        public override void Drop()
        {
            Vector3 DropPosition = new Vector3(Center.X, Center.Y, Position.Z);
            Item.SpawnItem<Items.Skull>(DropPosition);
        }
    }
}
