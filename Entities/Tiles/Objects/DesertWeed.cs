﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheLight.Entities.Items;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Objects
{
    public class DesertWeed : TileObject
    {
        public override void SetDefaults()
        {
            Name = "Засохшее растение";
            ColorsTexture = Main.Content.Load<Texture2D>("Images/Tiles/Objects/DesertPlant_Colors");
            ShadowOffset = new Vector2(0, 2);
            Type = TileID.DesertWeed;
            CanBeLooted = true;
        }

        public override void Drop()
        {
            Vector3 DropPosition = new Vector3(Center.X, Center.Y, Position.Z);
            for (int i = 0; i < 2; i++)
            {
                Item stick = Item.SpawnItem<Stick>(DropPosition);
                stick.ColorIndex = ColorIndex;
                stick.Texture = Utils.SwapColors(stick.ColorsTexture, stick.Texture, stick.ColorIndex);
            }
        }
    }
}
