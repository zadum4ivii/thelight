﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Objects
{
    public class BigDeadTree : TileObject
    {
        public override void SetDefaults()
        {
            Name = "Мёртвое дерево";
            ColorsTexture = Main.Content.Load<Texture2D>("Images/Tiles/Objects/DesertPlant_Colors");
            ShadowOffset = new Vector2(0, 2);
            Type = TileID.BigDeadTree;
            Offset = new Vector2(2, 0);
            FlippedShadowOffset = new Vector2(4, 0);
            FreeSpace = new Vector2(1, 0);
        }
    }
}
