﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Objects
{
    public class MediumDeadTree : TileObject
    {
        public override void SetDefaults()
        {
            Name = "Мёртвое дерево";
            ColorsTexture = Main.Content.Load<Texture2D>("Images/Tiles/Objects/DesertPlant_Colors");
            ShadowOffset = new Vector2(0, 2);
            Type = TileID.MediumDeadTree;
            FreeSpace = new Vector2(1, 0);
        }
    }
}
