﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Objects
{
    public class DeadTreeStump : TileObject
    {
        public override void SetDefaults()
        {
            Name = "Пенёк";
            ColorsTexture = Main.Content.Load<Texture2D>("Images/Tiles/Objects/DesertPlant_Colors");
            ShadowOffset = new Vector2(0, 2);
            Type = TileID.DeadTreeStump;
        }
    }
}
