﻿using Microsoft.Xna.Framework;
using TheLight.Entities.Particles;
using TheLight.ID;

namespace TheLight.Entities.Tiles.Solid
{
    public class Sand : TileSolid
    {
        public override void SetDefaults()
        {
            Name = "Песок";
            Type = TileID.Sand;
            Solid = true;
            MaxFrames = new Vector2(9, 5);
            MaxRandomFrames = 2;
            WalkingParticle = typeof(SandWalking);
        }
    }
}
