﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System.Collections.Generic;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities
{
    [AutoloadTextures(UITexture.Texture, UITexture.Highlight)]
    public class UIElement : Entity
    {
        public Texture2D HightlightTexture { get; set; }
        public new Vector2 Position { get; set; }
        public SoundEffect PressSound { get; set; }
        public SoundEffect HoverSound { get; set; }
        private bool _justHovered { get; set; }
        public UIElement()
        {
            Scale = Interface.InterfaceScale;
            SetDefaults();
            Autoload();
            Main.Interface.InterfaceElements.Add(this);
        }

        public override void Autoload()
        {
            List<object> attr = Utils.GetAllAttributes(GetType());
            if (attr != null)
            {
                if (attr.Contains(UITexture.Texture) && Texture == null) Texture = Utils.LoadTexture(TexturePath);
                if (attr.Contains(UITexture.Highlight) && HightlightTexture == null) HightlightTexture = Utils.LoadTexture(TexturePath + "_Highlight");
            }
        }

        public override void Update(GameTime gameTime) 
        { 
            if (Scale != Interface.InterfaceScale) Scale = Interface.InterfaceScale;
            if (PressedLeftButton() && PressSound != null) PressSound.Play(); 
            if (MouseOver())
            {
                if (!_justHovered)
                {
                    _justHovered = true;
                    if (HoverSound != null) HoverSound.Play();
                }
            }
            else _justHovered = false;
        }

        public override bool MouseOver() => base.MouseOver();

        public override System.Drawing.Rectangle TextureBox() => new System.Drawing.Rectangle((int)Position.X, (int)Position.Y, (int)(Texture.Width * Scale), (int)(Texture.Height * Scale));

        public virtual bool PressedLeftButton() => MouseOver() && KeyButtons.PressedLeftMouse();

        public virtual bool PressedRightButton() => MouseOver() && KeyButtons.PressedRightMouse();

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, Position, new Rectangle(0, 0, Texture.Width, Texture.Height), Color * Alpha, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
            DrawHighlight(gameTime, spriteBatch);
        }

        public virtual void DrawHighlight(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (HightlightTexture != null && MouseOver())
                spriteBatch.Draw(HightlightTexture, Position, new Rectangle(0, 0, HightlightTexture.Width, HightlightTexture.Height), Color * Alpha, 0f, Vector2.Zero, Scale, SpriteEffects.None, 1f);
        }

        public virtual void UpdatePosition() { }
    }
}
