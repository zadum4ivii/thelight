﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using TheLight.Entities.UI;
using TheLight.ID;
using TheLight.Attributes;
using Microsoft.Xna.Framework.Audio;

namespace TheLight.Entities
{
    [AutoloadTextures(ItemTexture.Texture, ItemTexture.Shadow, ItemTexture.Inventory)]
    public class Item : Entity
    {
        private float _angle = 0f;
        public bool Consumable { get; set; }
        public bool DrawEquipTexture { get; set; }
        public bool DrawInHand { get; set; }
        public bool DrawPlayerEyes { get; set; }
        public bool DrawPlayerTexture { get; set; }
        public bool Bag { get; set; }
        public Dictionary<string, object> EffectsList { get; set; }
        public Dictionary<string, object> SetEffectsList { get; set; }
        public int CurrentStack { get; set; }
        public int EquipType { get; set; }
        public int MaxStack { get; set; }
        public int UseAnimation { get; set; }
        public int UseTime { get; set; }
        public int UseTimer { get; set; }
        public Texture2D ArmsEquipTexture { get; set; }
        public Texture2D BodyEquipTexture { get; set; }
        public Texture2D HandTexture { get; set; }
        public Texture2D HeadEquipTexture { get; set; }
        public Texture2D InventoryTexture { get; set; }
        public Texture2D LegsEquipTexture { get; set; }
        public Vector2 DrawOffset { get; set; }
        public Vector2 HandDrawOffset { get; set; }
        public SoundEffect UseSound { get; set; }

        public Item()
        {
            MaxFrames = 1;
            EquipType = -1;
            MaxStack = 1;
            CurrentStack = 1;
            DrawOffset = Vector2.Zero;
            HandDrawOffset = Vector2.Zero;
            DrawPlayerTexture = true;
            DrawPlayerEyes = true;
            DrawInHand = false;
            SetDefaults();
            Autoload();
            WhoAmI = Main.ActiveEntities.Count;
            EffectsList = new Dictionary<string, object>();
            SetEffectsList = new Dictionary<string, object>();
            ItemEffects();
            if (Bag) InteractionText = "F, чтобы открыть";
        }

        public override void Autoload()
        {
            List<object> attr = Utils.GetAllAttributes(GetType());
            if (attr != null)
            {
                if (attr.Contains(ItemTexture.Texture) && Texture == null)
                {
                    Texture = Utils.LoadTexture(TexturePath);
                    if (Texture == null) Texture = Utils.LoadTexture("Images/Items/Placeholder");
                }
                if (attr.Contains(ItemTexture.Shadow) && ShadowTexture == null)
                {
                    ShadowTexture = Utils.LoadTexture(TexturePath + "_Shadow");
                    if (ShadowTexture == null) ShadowTexture = Utils.GenerateItemShadow(Texture, MaxFrames);
                }
                if (attr.Contains(ItemTexture.Inventory) && InventoryTexture == null)
                {
                    InventoryTexture = Utils.LoadTexture(TexturePath + "_Inventory");
                    if (InventoryTexture == null) InventoryTexture = Utils.LoadTexture("Images/Items/Placeholder_Inventory");
                }
                if (attr.Contains(ItemTexture.Hand) && HandTexture == null) HandTexture = Utils.LoadTexture(TexturePath + "_Hand");
                if (attr.Contains(ItemTexture.Colors) && ColorsTexture == null) ColorsTexture = Utils.LoadTexture(TexturePath + "_Colors");
                if (attr.Contains(ItemTexture.Head) && HeadEquipTexture == null) HeadEquipTexture = Utils.LoadTexture(TexturePath + "_Head");
                if (attr.Contains(ItemTexture.Body) && BodyEquipTexture == null) BodyEquipTexture = Utils.LoadTexture(TexturePath + "_Body");
                if (attr.Contains(ItemTexture.Arms) && ArmsEquipTexture == null) ArmsEquipTexture = Utils.LoadTexture(TexturePath + "_Arms");
                if (attr.Contains(ItemTexture.Legs) && LegsEquipTexture == null) LegsEquipTexture = Utils.LoadTexture(TexturePath + "_Legs");
            }
        }

        public override void Update(GameTime gameTime)
        {
            Center = new Vector2(Position.X, Position.Y - Position.Z) + new Vector2((Texture.Width / MaxFrames) / 2, Texture.Height / 2) + new Vector2(TextureOffset.X, -TextureOffset.Y);
            BaseHitbox = new Rectangle(0, (int)Position.Y + Texture.Height - 6, Texture.Width / MaxFrames, Texture.Height / 6);
            CanLoot = MouseOver() && Vector2.Distance(Main.Player.Center, Center) <= 50f + (Texture.Width / 2);
            if (CanLoot && KeyButtons.Pressed(KeyButtons.Use)) Pickup();
            base.Update(gameTime);
        }

        public virtual void UpdateInventory(GameTime gameTime, Slot slot)
        {
            if (Main.Interface.SelectedLeftHandHotbar.ContainedItem == this)
            {
                Hitbox();
                if (Main.Player.UsingItem)
                {
                    if (UseTimer > 0)
                    {
                        switch (UseAnimation)
                        {
                            case UseAnimationID.Swing:
                                _angle += (float)(Math.PI / 2) / UseTime;
                                Main.Player.ArmRotation = Main.Player.Direction == 1 ? 0f + _angle : 0f - _angle;
                                break;
                            case UseAnimationID.Consuming:
                                _angle += (float)(Math.PI / 2) / UseTime;
                                Main.Player.ArmRotation = Main.Player.Direction == 1 ? -_angle : _angle;
                                break;
                            default:
                                break;
                        }
                        UseTimer--;
                    }
                    else if (UseTimer == 0)
                    {
                        switch (UseAnimation)
                        {
                            case UseAnimationID.Swing:
                                Main.Player.ArmRotation = Main.Player.Direction == 1 ? -(float)Math.PI / 4 : (float)Math.PI / 4; ;
                                break;
                            case UseAnimationID.Consuming:
                                Main.Player.ArmRotation = 0f;
                                break;
                            default:
                                break;
                        }
                        OnUseItem();
                        Main.Player.UsingItem = false;
                        _angle = 0f;
                    }
                }
            }
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (TextureBox().IntersectsWith(Main.Screen.ScreenR))
            {
                DrawShadow(gameTime, spriteBatch);
                if (MaxFrames > 1)
                    spriteBatch.Draw(Texture, new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(TextureOffset.X, -TextureOffset.Y), new Rectangle((Texture.Width / MaxFrames) * Frame, 0, (Texture.Width / MaxFrames), Texture.Height), Color * Alpha, 0f, new Vector2(0, Texture.Height), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth);
                else
                    spriteBatch.Draw(Texture, new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(TextureOffset.X, -TextureOffset.Y), new Rectangle(0, 0, Texture.Width, Texture.Height), Color * Alpha, 0f, new Vector2(0, Texture.Height), Scale, Direction == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth);
            }
        }

        public override void DrawShadow(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ShadowTexture != null)
                spriteBatch.Draw(ShadowTexture, new Vector2((int)Position.X, (int)Position.Y - 1) + new Vector2((Texture.Width / MaxFrames) / 2, 0) - ShadowOffset, null, (Color.White) * (1f + (TextureOffset.Y / 50f)), 0f, new Vector2(ShadowTexture.Width, ShadowTexture.Height) / 2, Scale + (TextureOffset.Y / 80f), SpriteEffects.None, LayerDepth);
        }

        public override void Animation()
        {
            if (AnimationTimer != AnimationSpeed) AnimationTimer++;
            if (AnimationTimer == AnimationSpeed)
            {
                Frame++;
                AnimationTimer = 0;
            }
            if (Frame >= MaxFrames) Frame = 1;
        }

        public virtual void Despawn()
        {
            Main.ActiveEntities.RemoveAt(WhoAmI);
            for (int i = 0; i < Main.ActiveEntities.Count; i++)
                Main.ActiveEntities[i].WhoAmI = i;
        }

        public static Item SpawnItem<T>(Vector3 Position) where T : Item, new()
        {
            Main Main = Main.Instance;
            Item item = new T();
            item.Position = Position + new Vector3(Main.Rand.Next(-20, 20), Main.Rand.Next(-20, 20), 0);
            item.Frame = Main.Rand.Next(0, item.MaxFrames);
            Main.ActiveEntities.Insert(item.WhoAmI, item);
            return item;
        }

        public static Item CreateItem(Type t)
        {
            Item item = (Item)Activator.CreateInstance(t);
            return item;
        }

        public static Item CreateItem<T>() where T : Item, new()
        {
            Item item = new T();
            return item;
        }

        public override System.Drawing.Rectangle TextureBox() { return base.TextureBox(); }

        public virtual void Pickup()
        {
            if (!Bag)
            {
                Slot s = Interface.Inventory.InventorySlots.Find(slot => slot.ContainedItem != null && slot.ContainedItem.Type == Type && slot.ContainedItem.CurrentStack < slot.ContainedItem.MaxStack);
                if (s == null)
                {
                    Slot s1 = Interface.Inventory.InventorySlots.Find(slot => slot.ContainedItem == null);
                    if (s1 != null && s1 is InventoryItemSlot)
                    {
                        s1.ContainedItem = this;
                        DrawItemPickupText();
                        Despawn();
                    }
                }
                else
                {
                    s.ContainedItem.CurrentStack++;
                    DrawItemPickupText();
                    Despawn();
                }
            }
            else
            {
                DropBagItems();
                Despawn();
            }
        }

        public virtual void ItemEffects() { }

        public virtual void EquipEffects(Player player)
        {
            if (EffectsList.Count != -1)
                foreach (var effect in EffectsList)
                {
                    var value = typeof(Player).GetProperty(effect.Key);

                    if (value.PropertyType == typeof(int))
                        value.SetValue(player, (int)(value.GetValue(player)) + (int)(effect.Value));
                    else if (value.PropertyType == typeof(float))
                        value.SetValue(player, (float)(value.GetValue(player)) + (float)(effect.Value));
                    else if (value.PropertyType == typeof(bool))
                        value.SetValue(player, (bool)(effect.Value));
                }
        }

        public virtual void UnequipEffects(Player player)
        {
            if (EffectsList.Count != -1)
                foreach (var effect in EffectsList)
                {
                    var value = typeof(Player).GetProperty(effect.Key);

                    if (value.PropertyType == typeof(int))
                        value.SetValue(player, (int)(value.GetValue(player)) - (int)(effect.Value));
                    else if (value.PropertyType == typeof(float))
                        value.SetValue(player, (float)(value.GetValue(player)) - (float)(effect.Value));
                    else if (value.PropertyType == typeof(bool))
                        value.SetValue(player, !(bool)(effect.Value));
                }
        }

        public virtual void PlacedInSlot(Slot slot)
        {
            if (slot is InventoryEquipSlot)
            {
                EquipEffects(Main.Player);
                SetEquipEffects(Main.Player);
                switch (EquipType)
                {
                    case EquipTypeID.Head:
                        Main.Player.HeadArmorTexture = DrawPlayerTexture ? Utils.MergeTextures(Main.Player.HeadTexture, HeadEquipTexture) : HeadEquipTexture;
                        break;
                    case EquipTypeID.Body:
                        Main.Player.BodyArmorTexture = DrawPlayerTexture ? Utils.MergeTextures(Main.Player.BodyTexture, BodyEquipTexture) : BodyEquipTexture;
                        Main.Player.ArmsArmorTexture = DrawPlayerTexture ? Utils.MergeTextures(Main.Player.ArmsTexture, ArmsEquipTexture) : ArmsEquipTexture;
                        break;
                    case EquipTypeID.Legs:
                        Main.Player.LegsArmorTexture = DrawPlayerTexture ? Utils.MergeTextures(Main.Player.LegsTexture, LegsEquipTexture) : LegsEquipTexture;
                        break;
                    default:
                        break;
                }
            }
        }

        public virtual void RemovedFromSlot(Slot slot)
        {
            if (slot is InventoryEquipSlot)
            {
                UnequipEffects(Main.Player);
                SetUnequipEffects(Main.Player);
                switch (EquipType)
                {
                    case EquipTypeID.Head:
                        Main.Player.HeadArmorTexture = null;
                        break;
                    case EquipTypeID.Body:
                        Main.Player.BodyArmorTexture = null;
                        Main.Player.ArmsArmorTexture = null;
                        break;
                    case EquipTypeID.Legs:
                        Main.Player.LegsArmorTexture = null;
                        break;
                    default:
                        break;
                }
            }
        }

        public virtual bool IsSet(Item headItem, Item bodyItem, Item legsItem) => false;

        public virtual void SetEquipEffects(Player player)
        {
            if (IsSet(Interface.Inventory.HeadArmorSlot.ContainedItem, Interface.Inventory.BodyArmorSlot.ContainedItem, Interface.Inventory.LegsArmorSlot.ContainedItem))
            {
                if (SetEffectsList.Count != -1)
                    foreach (var effect in SetEffectsList)
                    {
                        var value = typeof(Player).GetProperty(effect.Key);

                        if (value.PropertyType == typeof(int))
                            value.SetValue(player, (int)(value.GetValue(player)) + (int)(effect.Value));
                        else if (value.PropertyType == typeof(float))
                            value.SetValue(player, (float)(value.GetValue(player)) + (float)(effect.Value));
                        else if (value.PropertyType == typeof(bool))
                            value.SetValue(player, (bool)(effect.Value));
                    }
            }
        }

        public virtual void SetUnequipEffects(Player player)
        {
            if (IsSet(Interface.Inventory.HeadArmorSlot.ContainedItem, Interface.Inventory.BodyArmorSlot.ContainedItem, Interface.Inventory.LegsArmorSlot.ContainedItem))
            {
                if (SetEffectsList.Count != -1)
                    foreach (var effect in SetEffectsList)
                    {
                        var value = typeof(Player).GetProperty(effect.Key);

                        if (value.PropertyType == typeof(int))
                            value.SetValue(player, (int)(value.GetValue(player)) - (int)(effect.Value));
                        else if (value.PropertyType == typeof(float))
                            value.SetValue(player, (float)(value.GetValue(player)) - (float)(effect.Value));
                        else if (value.PropertyType == typeof(bool))
                            value.SetValue(player, !(bool)(effect.Value));
                    }
            }
        }

        public virtual void DrawEquipped(SpriteBatch spriteBatch, GameTime gameTime)
        {
            if (UseTimer == 0)
                switch (UseAnimation)
                {
                    case UseAnimationID.Swing:
                        Main.Player.ArmRotation = 0f;
                        break;
                    case UseAnimationID.Consuming:
                        Main.Player.ArmRotation = 0f;
                        break;
                    default:
                        break;
                }
            if (HandTexture != null)
            {
                spriteBatch.Draw(HandTexture, Main.Player.ArmPosition, new Rectangle(0, 0, HandTexture.Width, HandTexture.Height), Color * Alpha, Main.Player.ArmRotation, new Vector2(Main.Player.Direction == 1 ? 4 + HandDrawOffset.X : HandTexture.Width - 4 - HandDrawOffset.X, HandTexture.Height - 4 - HandDrawOffset.Y), Scale, Main.Player.Direction == 1 ? SpriteEffects.None : SpriteEffects.FlipHorizontally, (Main.Player.LayerDepth + (Main.Player.Position.Z + 3) * 0.00001f) - 0.0000001f);
                Utils.FillRectangle(spriteBatch, Hitbox(), Color.Red);
            }
        }

        public virtual void UseItem()
        {
            UseTimer = UseTime;
            Main.Player.UsingItem = true;
            if (UseSound != null) UseSound.Play();
        }

        public virtual void OnUseItem()
        {
            ItemUseEffects();
            
            if (Consumable)
            {
                CurrentStack--;
                if (CurrentStack == 0) Main.Interface.Inventory.FindItemSlot(this).ContainedItem = null;
            }
        }

        public virtual void ItemUseEffects() { }
        public virtual void DropBagItems() { }
        public virtual void DrawItemPickupText()
        {
            if (Main.Player.PickedItems.Any(t => t.Item1.Type == Type))
            {
                for (int i = 0; i < Main.Player.PickedItems.Count; i++)
                    if (Main.Player.PickedItems[i].Item1.Type == Type)
                    {
                        var t = Tuple.Create(Main.Player.PickedItems[i].Item1, Main.Player.PickedItems[i].Item2 + 1);
                        Main.Player.PickedItems.RemoveAt(i);
                        Main.Player.PickedItems.Add(t);
                        Main.Player.PickedItemsTimer = 0;
                    }
            }
            else
                Main.Player.PickedItems.Add(Tuple.Create(this, 1));
        }

        public Rectangle Hitbox()
        {
            return new Rectangle();
            //need to fucking rework this bullshit
            /*double angle = 45f + MathHelper.ToDegrees(Main.Player.ArmRotation);
            double textureLength = Sqrt(Pow(HandTexture.Width, 2) + Pow(HandTexture.Height, 2));
            if (Main.Player.Direction == -1) angle -= 90;
            angle = Abs(angle);
            Main.Chat.NewMessage("" + angle);
            Main.Chat.NewMessage("" + textureLength);
            double width = Abs(Sin(MathHelper.ToRadians((float)angle)) * textureLength);
            double height = Abs(Cos(MathHelper.ToRadians((float)angle)) * textureLength);
            Vector2 position = Main.Player.ArmPosition - new Vector2(Main.Player.Direction == 1 ? 4 + HandDrawOffset.X : - 4 - HandDrawOffset.X, - 4 - HandDrawOffset.Y);
            if (Main.Player.Direction == 1)
            {
                if (angle < 90) position.Y -= (float)height;
            }
            else
            {
                position.X -= (float)width;
                if (angle < 90) position.Y -= (float)height;
            }
            return new Rectangle((int)position.X, (int)(position.Y), (int)width, (int)height);*/
        }
    }
}
