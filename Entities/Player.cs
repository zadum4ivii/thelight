﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using TheLight.Entities.Particles;
using TheLight.Entities.UI;
using TheLight.IO;

namespace TheLight.Entities
{
    public class Player : Entity
    {
        public const string PlayerSaveFile = ".player";
        internal List<Tuple<Item, int>> PickedItems { get; set; }
        private bool _wasMoving = false;
        private bool _wasStanding = false;
        private int _blinkSpeed { get; set; }
        private int _blinkTimer { get; set; }
        public bool IsRunning { get; set; }
        public bool Jumped { get; set; }
        public bool MovingDown { get; set; }
        public bool MovingLeft { get; set; }
        public bool MovingRight { get; set; }
        public bool MovingUp { get; set; }
        public bool UsingItem { get; set; }
        public bool WasRunningBeforeJump { get; set; }
        public float ArmRotation { get; set; }
        public float RunSpeedMultiplier { get; set; }
        public float Speed { get; set; }
        public int CurrentHealth { get; set; }
        public int CurrentMana { get; set; }
        public int InvincibilityTime { get; set; }
        public int JumpCooldown { get; set; }
        public int JumpTime { get; set; }
        public int JumpTimeMax { get; set; }
        public int MaximumHealth { get; set; }
        public int MaximumMana { get; set; }
        public int MaxInvincibilityTime { get; set; }
        public int PickedItemsTimer { get; set; }
        public Texture2D ArmArmorTexture { get; set; }
        public Texture2D ArmsAltTexture { get; set; }
        public Texture2D ArmsArmorTexture { get; set; }
        public Texture2D ArmsTexture { get; set; }
        public Texture2D ArmTexture { get; set; }
        public Texture2D BodyArmorTexture { get; set; }
        public Texture2D BodyTexture { get; set; }
        public Texture2D EyesTexture { get; set; }
        public Texture2D HeadArmorTexture { get; set; }
        public Texture2D HeadTexture { get; set; }
        public Texture2D LegsArmorTexture { get; set; }
        public Texture2D LegsTexture { get; set; }
        public Vector2 ArmPosition { get; set; }
        public Vector2 ArmsPosition { get; set; }
        public Vector2 BodyPosition { get; set; }
        public Vector2 EyesPosition { get; set; }
        public Vector2 HeadPosition { get; set; }
        public Vector2 LegsPosition { get; set; }
        public bool CanMoveUp { get; set; }
        public bool CanMoveDown { get; set; }
        public bool CanMoveLeft { get; set; }
        public bool CanMoveRight { get; set; }
        public bool IsCollidingLeft { get; set; }
        public bool IsCollidingRight { get; set; }
        public bool IsCollidingUp { get; set; }
        public bool IsCollidingDown { get; set; }
        public SoundEffect HitSound { get; set; }
        public SoundEffect DeathSound { get; set; }
        public SoundEffect JumpSound { get; set; }
        public bool CanClimbUp { get; set; }
        public bool IsClimbingUp { get; set; }
        public float ClimbTime { get; set; }
        public Vector3 ClimbStartPosition { get; set; }
        public Vector3 ClimbEndPosition { get; set; }
        public int ClimbingDirection { get; set; }
        public float CurrentZLvl { get; set; }
        public Player()
        {
            MaxFrames = 13;
            RunSpeedMultiplier = 1.315f;
            Speed = 1.7f;
            Acceleration = 0.1f;
            AnimationTimer = 0;
            _blinkSpeed = 90;
            _blinkTimer = 0;
            JumpTimeMax = 30;
            Name = "Игрок";
            ColorsTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Skin");
            ColorIndex = Main.Rand.Next(1, ColorsTexture.Height);
            Autoload();
            WhoAmI = 0;
            PickedItems = new List<Tuple<Item, int>>() { };
            MaximumHealth = 80;
            CurrentHealth = MaximumHealth;
            MaximumMana = 80;
            CurrentMana = 80;
            MaxInvincibilityTime = 30;
            CanMoveUp = true;
            CanMoveDown = true;
            CanMoveLeft = true;
            CanMoveRight = true;
        }

        public override void Autoload()
        {
            Texture = Main.Content.Load<Texture2D>("Images/Player/Player_Hitbox");
            HeadTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Head");
            BodyTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Body");
            LegsTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Legs");
            ArmsTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Arms");
            ArmTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Arm");
            ArmsAltTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Arms_1");
            EyesTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Eyes");
            ShadowTexture = Main.Content.Load<Texture2D>("Images/Player/Player_Shadow");
            HeadTexture = Utils.SwapColors(ColorsTexture, HeadTexture, ColorIndex);
            BodyTexture = Utils.SwapColors(ColorsTexture, BodyTexture, ColorIndex);
            ArmsTexture = Utils.SwapColors(ColorsTexture, ArmsTexture, ColorIndex);
            ArmTexture = Utils.SwapColors(ColorsTexture, ArmTexture, ColorIndex);
            ArmsAltTexture = Utils.SwapColors(ColorsTexture, ArmsAltTexture, ColorIndex);
            LegsTexture = Utils.SwapColors(ColorsTexture, LegsTexture, ColorIndex);
            EyesTexture = Utils.SwapColor(EyesTexture, new Color(81, 81, 81), new Color(50 + Main.Rand.Next(70), 50 + Main.Rand.Next(70), 50 + Main.Rand.Next(70)));
            HitSound = Main.Content.Load<SoundEffect>("Audio/Sounds/PlayerHurt");
            DeathSound = Main.Content.Load<SoundEffect>("Audio/Sounds/PlayerDeath");
            JumpSound = Main.Content.Load<SoundEffect>("Audio/Sounds/PlayerJump");
        }

        public void PlayerLoad()
        {
            if (File.Exists(PlayerSaveFile))
            {
                if (MessageBox.Show("Обнаружено сохранение игрока. Загрузить?", "Уведомление", System.Windows.Forms.MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
                {
                    Utils.Log("Player file found, loading player");
                    using (Stream stream = File.Open(PlayerSaveFile, FileMode.Open))
                    {
                        BinaryFormatter binaryFormatter = new BinaryFormatter();
                        while (stream.Position != stream.Length)
                        {
                            object obj = binaryFormatter.Deserialize(stream);
                            //player
                            if (obj.GetType().ToString().Contains("PlayerIO"))
                            {
                                PlayerIO player = (PlayerIO)obj;
                                player.Deserialize();
                            }
                            //Inventory
                            if (obj.GetType().ToString().Contains("InventoryIO"))
                            {
                                InventoryIO inventory = (InventoryIO)obj;
                                inventory.Deserialize();
                            }
                        }
                    }
                }
                else
                {
                    Utils.Log("Player save file found, but creating a new player");
                    Position = new Vector3(Main.Rand.Next(100, Main.Screen.Width - 100), Main.Rand.Next(100, Main.Screen.Height - 100), 0);
                }
            }
            else
            {
                Utils.Log("Player save file not found, creating a new player");
                Position = new Vector3(Main.Rand.Next(100, Main.Screen.Width - 100), Main.Rand.Next(100, Main.Screen.Height - 100), 0);
            }
        }

        public void PlayerSave()
        {
            Utils.Log("Saving the player");
            try
            {
                using (Stream stream = File.Open(PlayerSaveFile, FileMode.Create))
                {
                    PlayerIO p = new PlayerIO(this);
                    p.Serialize(stream);
                    InventoryIO i = new InventoryIO(Main.Interface.Inventory);
                    i.Serialize(stream);
                }
            }
            catch (Exception e)
            {
                Utils.Log("Failed to save the player: " + e);
            }
        }
        public override void UpdateDepth() { LayerDepth = Position.Y / 1000000f; }

        public override void Update(GameTime gameTime)
        {
            TileCollision();
            Control();
            AnimationSpeed = (int)(8f - Math.Abs(Velocity.Length()));
            if (_blinkTimer == _blinkSpeed) _blinkTimer = 0;
            if (JumpCooldown != 0) JumpCooldown--;
            if (JumpTime != 0) JumpTime--;
            bool InJump = JumpTime >= (JumpTimeMax / 3) * 2;
            //Position.Z = (float)Math.Round((double)Position.Z, 2);
            if (JumpTime != 0)
            {
                if (InJump) Position.Z += 0.0725f;
                ShadowOffset = new Vector2(Direction == 1 ? 2 : -2, 0);
            }
            else ShadowOffset = Vector2.Zero;
            TextureOffset = new Vector2(0, Position.Z * 28.5f);
            Center = new Vector2(Position.X + Texture.Width / 2, Position.Y - Texture.Height / 2 + Position.Z);
            BaseHitbox = new Rectangle((int)Position.X, (int)Position.Y - 4, Texture.Width, 6);
            base.Update(gameTime);
            if (PickedItems.Count > 0 && PickedItemsTimer < 180) PickedItemsTimer++;
            if (PickedItems.Count == 0) PickedItemsTimer = 0;
            if (PickedItemsTimer == 180)
            {
                PickedItems.RemoveAt(0);
                PickedItemsTimer = 0;
            }
            if (InvincibilityTime > 0)
            {
                InvincibilityTime--;
                Alpha = 0.8f;
            }
            else Alpha = 1f;
            if (CurrentHealth >= MaximumHealth) CurrentHealth = MaximumHealth;
            if (GetGroundTile() != null) TileStandingEffects();
            else if (!IsClimbingUp && !InJump)
                Position.Z -= 0.065f;
            Climb();
            if (Position.Z < 0f)
                Position.Z = 0f;
        }

        public override void Animation()
        {
            if (Velocity == Vector2.Zero)
            {
                _wasStanding = true;
                if (_wasMoving)
                {
                    AnimationTimer = 0;
                    _wasMoving = false;
                }
                AnimationTimer++;
                if (AnimationTimer == AnimationSpeed * 3)
                {
                    Frame++;
                    AnimationTimer = 0;
                }
                if (Frame < MaxFrames - 7 || Frame >= MaxFrames - 3) Frame = 6;
            }
            else if (JumpTime != 0)
            {
                if (JumpTime < JumpTimeMax / 3) Frame = MaxFrames - 1;
                else if (JumpTime >= (JumpTimeMax / 3) * 2) Frame = MaxFrames - 3;
                else Frame = MaxFrames - 2;
            }
            else
            {
                _wasMoving = true;
                if (_wasStanding)
                {
                    AnimationTimer = 0;
                    _wasStanding = false;
                }
                if (AnimationTimer != AnimationSpeed) AnimationTimer++;
                if (AnimationTimer == AnimationSpeed)
                {
                    Frame++;
                    AnimationTimer = 0;
                }
                if (Frame >= MaxFrames - 7) Frame = 0;
            }
        }

        private void TileCollision()
        {
            CanClimbUp = false;
            float Acceleration2 = Acceleration * (IsRunning ? RunSpeedMultiplier : 1.15f);
            foreach (TileSolid tile in Main.World.WorldTiles)
            {
                if (tile != null && tile.Position.Z / 32f > 0 && Main.Player.BaseHitbox.Intersects(tile.BaseHitbox))
                {
                    if (Position.Z <= tile.Position.Z / 32f && !tile.IsAnyHighestTileTop() && tile.Position.Z / 32f - Position.Z <= 1 && KeyButtons.Pressed(KeyButtons.Jump))
                    {
                        ClimbingDirection = Position.X > tile.Position.X - 100f && Position.X < tile.Position.X + 16f ? 1 : -1;
                        IsClimbingUp = true;
                        ClimbTime = 0f;
                        ClimbStartPosition = Position;
                        Vector3 climbPosition = new Vector3(tile.Position.X, tile.Position.Y - 8f, Position.Z + 1);
                        ClimbEndPosition = new Vector3(climbPosition.X, climbPosition.Y, climbPosition.Z);
                    }
                    else if (tile.Position.Z / 32f > Position.Z)
                    {
                        IsCollidingLeft = tile.RightHitbox != Rectangle.Empty && tile.RightHitbox.Intersects(BaseHitbox);
                        IsCollidingRight = tile.LeftHitbox != Rectangle.Empty && tile.LeftHitbox.Intersects(BaseHitbox);
                        IsCollidingUp = tile.BottomHitbox != Rectangle.Empty && tile.BottomHitbox.Intersects(BaseHitbox);
                        IsCollidingDown = tile.TopHitbox != Rectangle.Empty && tile.TopHitbox.Intersects(BaseHitbox);
                        if (IsCollidingLeft || IsCollidingRight)
                        {
                            if (IsCollidingLeft)
                            {
                                if (Velocity.X < 0f)
                                    Position.X = tile.RightHitbox.X + Acceleration2;
                            }
                            else if (IsCollidingRight)
                                if (Velocity.X > 0f)
                                    Position.X = tile.LeftHitbox.X - 32 - Acceleration2;
                            if ((double)Math.Abs(Velocity.X) <= 0.1) Velocity.X = 0f;
                        }
                        if (IsCollidingUp || IsCollidingDown)
                        {
                            if (IsCollidingUp)
                            {
                                if (Velocity.Y < 0f)
                                    Position.Y = tile.BottomHitbox.Y + 4 + Acceleration2;
                            }
                            else if (IsCollidingDown)
                                if (Velocity.Y > 0f)
                                    Position.Y = tile.TopHitbox.Y - Acceleration2;
                            if ((double)Math.Abs(Velocity.Y) <= 0.1) Velocity.Y = 0f;
                        }
                    }
                }
            }
        }

        public virtual void Control()
        {
            float MaxSpeed = Speed * (IsRunning ? RunSpeedMultiplier : 1f);
            float Acceleration2 = Acceleration * (IsRunning ? RunSpeedMultiplier : 1.15f);
            float Acceleration3 = Acceleration * 2f * (IsRunning ? RunSpeedMultiplier : 1f);
            if (JumpTime == 0) WasRunningBeforeJump = false;
            bool info = !Main.Chat.ChatMode || Main.Settings.ShowInfo;
            MovingLeft = info && KeyButtons.Pressing(KeyButtons.Left) && JumpTime == 0 && !IsCollidingLeft;
            MovingRight = info && KeyButtons.Pressing(KeyButtons.Right) && JumpTime == 0 && !IsCollidingRight;
            MovingUp = info & KeyButtons.Pressing(KeyButtons.Up) && JumpTime == 0 && !IsCollidingUp;
            MovingDown = info && KeyButtons.Pressing(KeyButtons.Down) && JumpTime == 0 && !IsCollidingDown;
            if (KeyButtons.Pressed(KeyButtons.Jump) && JumpCooldown == 0)
            {
                JumpSound.Play();
                JumpCooldown = 60;
                JumpTime = JumpTimeMax;
                Jumped = !Jumped;
                WasRunningBeforeJump = IsRunning;
            }
            if (KeyButtons.Pressing(KeyButtons.Run) && !MovingDown && !MovingDown && !MovingLeft && !MovingRight) IsRunning = false;
            IsRunning = KeyButtons.Pressing(KeyButtons.Run) && JumpTime == 0 && (float)Math.Abs(Velocity.Length()) >= 1.25f;
            if (Jumped)
            {
                Jumped = false;
                Velocity *= 1.3f;
            }
            else if (JumpTime == 0)
            {
                if (MovingLeft && !MovingRight)
                {
                    if (Velocity.X < -MaxSpeed) Velocity.X = -MaxSpeed;
                    Velocity.X -= Acceleration2;
                }
                else if (MovingRight && !MovingLeft)
                {
                    if (Velocity.X > MaxSpeed) Velocity.X = MaxSpeed;
                    Velocity.X += Acceleration2;
                }
                else if (Velocity.X > 0f) Velocity.X -= Acceleration3;
                else if (Velocity.X < 0f) Velocity.X += Acceleration3;
                else Velocity.X = 0f;
                if (Velocity.X <= 0.1f && Velocity.X >= -0.1f)
                    if ((!MovingLeft && !MovingRight) || (MovingLeft && MovingRight))
                        Velocity.X = 0f;
                if (MovingUp && !MovingDown)
                {
                    if (Velocity.Y < -MaxSpeed) Velocity.Y = -MaxSpeed;
                    Velocity.Y -= Acceleration2;
                }
                else if (MovingDown && !MovingUp)
                {
                    if (Velocity.Y > MaxSpeed) Velocity.Y = MaxSpeed;
                    Velocity.Y += Acceleration2;
                }
                else if (Velocity.Y > 0f) Velocity.Y -= Acceleration3;
                else if (Velocity.Y < 0f) Velocity.Y += Acceleration3;
                else Velocity.Y = 0f;
                if (Velocity.Y <= 0.1f && Velocity.Y >= -0.1f)
                    if ((!MovingUp && !MovingDown) || (MovingUp && MovingDown))
                        Velocity.Y = 0f;
                if (NewVelocity.X > 0f) NewVelocity.X -= Acceleration3;
                else if (NewVelocity.X < 0f) NewVelocity.X += Acceleration3;
                else NewVelocity.X = 0f;
                if (NewVelocity.X <= 0.1f && NewVelocity.X >= -0.1f) NewVelocity.X = 0f;
                if (NewVelocity.Y > 0f) NewVelocity.Y -= Acceleration3;
                else if (NewVelocity.Y < 0f) NewVelocity.Y += Acceleration3;
                else NewVelocity.Y = 0f;
                if (NewVelocity.Y <= 0.1f && NewVelocity.Y >= -0.1f) NewVelocity.Y = 0f;
            }
            if (!UsingItem && KeyButtons.PressedLeftMouse() && Main.Interface.SelectedLeftHandHotbar.ContainedItem != null
                && Main.Interface.SelectedLeftHandHotbar.ContainedItem.UseTime != 0 && !Main.Interface.OverInterface)
                Main.Interface.SelectedLeftHandHotbar.ContainedItem.UseItem();
            IsCollidingLeft = IsCollidingRight = IsCollidingUp = IsCollidingDown = false;
        }

        private void Climb()
        {
            if (IsClimbingUp)
            {
                Velocity *= 0.6f;
                ClimbTime += 2f / 60f;
                ClimbTime = MathHelper.Clamp(ClimbTime, 0f, 1f);
                if (ClimbTime < 0.75f) Position.Z = MathHelper.Lerp(ClimbStartPosition.Z, ClimbEndPosition.Z, ClimbTime / 0.75f);
                else Position.Z = ClimbEndPosition.Z;
                Position.X = MathHelper.Lerp(ClimbStartPosition.X, ClimbEndPosition.X, ClimbTime);
                Position.Y = MathHelper.Lerp(ClimbStartPosition.Y, ClimbEndPosition.Y, ClimbTime);
                if (ClimbTime >= 1f)
                {
                    ClimbingDirection = 0;
                    IsClimbingUp = false;
                }
                //else
                //Rotation = ClimbTime * 0.7f * (float)Direction;
            }
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            LegsPosition = new Vector2(Position.X, Position.Y - Position.Z) + new Vector2(-6, -TextureOffset.Y + 6);
            BodyPosition = LegsPosition - new Vector2(0, LegsTexture.Height - 16);
            ArmsPosition = BodyPosition - new Vector2(0, -2);
            bool MovedUp = (Frame == 0 || Frame == 1 || Frame == 3 || Frame == 4 || Frame == 6 || Frame > 8);
            Vector2 jumpArmOffset = new Vector2();
            if (Frame == MaxFrames - 1 || Frame == MaxFrames - 2) jumpArmOffset = new Vector2(Direction == 1 ? 3 : -3, -2);
            else if (Frame == MaxFrames - 3) jumpArmOffset = new Vector2(Direction == 1 ? 1 : -1, -2);
            else jumpArmOffset = Vector2.Zero;
            ArmPosition = BodyPosition + new Vector2(Direction == 1 ? Texture.Width + 2 : 10, -12) - new Vector2(0, MovedUp ? 2 : 0) + jumpArmOffset;
            HeadPosition = BodyPosition - new Vector2(0, BodyTexture.Height - 16);
            EyesPosition = HeadPosition - new Vector2(0, 6);
            Item HeadItem = Main.Interface.Inventory.HeadArmorSlot.ContainedItem;
            Item BodyItem = Main.Interface.Inventory.BodyArmorSlot.ContainedItem;
            Item LegsItem = Main.Interface.Inventory.LegsArmorSlot.ContainedItem;
            int NewDirection = ClimbingDirection != 0 ? ClimbingDirection : Direction;
            spriteBatch.Draw((LegsItem != null && LegsItem.LegsEquipTexture != null) ? LegsArmorTexture : LegsTexture, new Vector2(LegsPosition.X, LegsPosition.Y), new Rectangle((LegsTexture.Width / MaxFrames) * Frame, 0, (LegsTexture.Width / MaxFrames), LegsTexture.Height), Color * Alpha, 0f, new Vector2(0, LegsTexture.Height), Scale, NewDirection == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth + (Position.Z + 3) * 0.00001f);
            spriteBatch.Draw((BodyItem != null && BodyItem.BodyEquipTexture != null) ? BodyArmorTexture : BodyTexture, new Vector2(BodyPosition.X, BodyPosition.Y), new Rectangle((BodyTexture.Width / MaxFrames) * Frame, 0, (BodyTexture.Width / MaxFrames), BodyTexture.Height), Color * Alpha, 0f, new Vector2(0, BodyTexture.Height), Scale, NewDirection == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth + (Position.Z + 3) * 0.00001f);
            bool DrawItem = Interface.SelectedLeftHandHotbar.ContainedItem != null && Interface.SelectedLeftHandHotbar.ContainedItem.DrawInHand == true && Interface.SelectedLeftHandHotbar.ContainedItem.HandTexture != null;
            spriteBatch.Draw((BodyItem != null && BodyItem.ArmsEquipTexture != null) ? ArmsArmorTexture : (DrawItem ? ArmsAltTexture : ArmsTexture), new Vector2(ArmsPosition.X, ArmsPosition.Y), new Rectangle((ArmsTexture.Width / MaxFrames) * Frame, 0, (ArmsTexture.Width / MaxFrames), ArmsTexture.Height), Color * Alpha, 0f, new Vector2(0, ArmsTexture.Height), Scale, NewDirection == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth + (Position.Z + 3) * 0.00001f);
            if (DrawItem)
            {
                Main.Interface.SelectedLeftHandHotbar.ContainedItem.DrawEquipped(spriteBatch, gameTime);
                spriteBatch.Draw((BodyItem != null && BodyItem.ArmsEquipTexture != null) ? ArmArmorTexture : ArmTexture, new Vector2(ArmPosition.X, ArmPosition.Y), new Rectangle(0, 0, ArmTexture.Width, ArmTexture.Height), Color * Alpha, ArmRotation, new Vector2(ArmTexture.Width, ArmTexture.Height) / 2, Scale, SpriteEffects.None, LayerDepth + (Position.Z + 3) * 0.00001f);
            }
            spriteBatch.Draw((HeadItem != null && HeadItem.HeadEquipTexture != null && HeadArmorTexture != null) ? HeadArmorTexture : HeadTexture, new Vector2(HeadPosition.X, HeadPosition.Y), new Rectangle((HeadTexture.Width / MaxFrames) * Frame, 0, (HeadTexture.Width / MaxFrames), HeadTexture.Height), Color * Alpha, 0f, new Vector2(0, HeadTexture.Height), Scale, NewDirection == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth + (Position.Z + 3) * 0.00001f);
            if (HeadItem == null || (HeadItem != null && HeadItem.DrawPlayerEyes))
                spriteBatch.Draw(EyesTexture, new Vector2(EyesPosition.X, EyesPosition.Y), new Rectangle((EyesTexture.Width / MaxFrames) * Frame, 0, (EyesTexture.Width / MaxFrames), EyesTexture.Height), _blinkTimer > _blinkSpeed - 5 ? new Color(112, 51, 57) * Alpha : Color.White * Alpha, 0f, new Vector2(0, EyesTexture.Height), Scale, NewDirection == -1 ? SpriteEffects.FlipHorizontally : SpriteEffects.None, LayerDepth + (Position.Z + 3) * 0.00001f);
        }

        public override System.Drawing.Rectangle TextureBox()
        {
            return new System.Drawing.Rectangle((int)(Position.X + TextureOffset.X - Main.Screen.ScreenPosition.X), (int)(Position.Y - Position.Z + TextureOffset.Y - Main.Screen.ScreenPosition.Y - Texture.Height), Texture.Width, Texture.Height);
        }

        public override bool MouseOver()
        {
            System.Drawing.Rectangle Cursor = new System.Drawing.Rectangle((int)Interface.Cursor.Position.X, (int)Interface.Cursor.Position.Y, 4, 4);
            System.Drawing.Rectangle Player = new System.Drawing.Rectangle((int)Position.X + (int)TextureOffset.X, (int)Position.Y + (int)TextureOffset.Y - (int)Position.Z, Texture.Width, Texture.Height);
            return Cursor.IntersectsWith(Player);
        }

        public override void DrawShadow(GameTime gameTime, SpriteBatch spriteBatch)
        {
            if (ShadowTexture != null)
                spriteBatch.Draw(ShadowTexture, new Vector2(Position.X + (Direction == 1 ? 1 : -1), Position.Y - 1) + new Vector2(Texture.Width / 2, 0) + ShadowOffset, null, (Color.White) * (1f + (TextureOffset.Y / 50f)), 0f, new Vector2(ShadowTexture.Width, ShadowTexture.Height) / 2, Scale + (TextureOffset.Y / 80f), SpriteEffects.None, LayerDepth);
        }

        public virtual void DamagePlayer(int damage, Entity damageDealer, float knockback)
        {
            if (InvincibilityTime == 0)
            {
                for (int i = 0; i < Main.Rand.Next(8, 14); i++)
                {
                    Particle p = Particle.SpawnParticle<Blood>(this);
                    p.TimeLeft += i;
                    p.StickToTarget = true;
                }
                damage += Main.Rand.Next(-2, 3);
                FloatingText.NewText(damage.ToString(), this, FloatingText.TextTypes.Damage);
                CurrentHealth -= damage;
                InvincibilityTime = MaxInvincibilityTime;
                Vector2 screenPos = Main.Screen.ScreenPosition;
                float hitDirectionX = Position.X - screenPos.X > damageDealer.Position.X - screenPos.X ? 1f : -1f;
                float hitDirectionY = Position.Y - screenPos.Y > damageDealer.Position.Y - screenPos.Y ? 1f : -1f;
                if (knockback > 5f) knockback = 5f;
                NewVelocity.X += knockback * hitDirectionX;
                NewVelocity.Y += knockback * hitDirectionY;
                if (CurrentHealth > 0)
                    HitSound.Play();
                else
                {
                    DeathSound.Play();
                    CurrentHealth = 0;
                    if (MessageBox.Show("Потрачено!", "Уведомление", MessageBoxButtons.OK) == DialogResult.OK)
                        Main.Instance.Exit();
                }
                //gradual knockback

                /*
                float knockBackSpeed = knockback * 0.5f;
                if (hitDirectionX == -1f && Velocity.X > 0f - knockback)
                {
                    if (Velocity.X > 0f)
                        Velocity.X -= knockBackSpeed;
                    Velocity.X -= knockBackSpeed;
                    if (Velocity.X < 0f - knockback)
                        Velocity.X = 0f - knockBackSpeed;
                }
                else if (hitDirectionX == 1f && Velocity.X < knockback)
                {
                    if (Velocity.X < 0f)
                        Velocity.X += knockBackSpeed;
                    Velocity.X += knockBackSpeed;
                    if (Velocity.X > knockback)
                        Velocity.X = knockBackSpeed;
                }
                if (hitDirectionY == -1f && Velocity.Y > 0f - knockback)
                {
                    if (Velocity.Y > 0f)
                        Velocity.Y -= knockBackSpeed;
                    Velocity.Y -= knockBackSpeed;
                    if (Velocity.Y < 0f - knockback)
                        Velocity.Y = 0f - knockBackSpeed;
                }
                else if (hitDirectionY == 1f && Velocity.Y < knockback)
                {
                    if (Velocity.Y < 0f)
                        Velocity.Y += knockBackSpeed;
                    Velocity.Y += knockBackSpeed;
                    if (Velocity.Y > knockback)
                        Velocity.Y = knockBackSpeed;
                } */
            }
        }

        public virtual void HealPlayer(int heal)
        {
            FloatingText.NewText(heal.ToString(), this, FloatingText.TextTypes.Heal);
            CurrentHealth += heal;
            if (CurrentHealth > MaximumHealth) CurrentHealth = MaximumHealth;
        }

        public virtual TileSolid GetGroundTile()
        {
            float k = Position.Z;
            float i = (Position.X + 16f) / 32f;
            float j = (Position.Y + 32f) / 32f;
            TileSolid tile = Main.World.WorldTiles[(int)i, (int)j, (int)k];
            if (tile != null && tile.Position.Z / 32f == Position.Z)
                return tile;
            return null;
        }

        public virtual void TileStandingEffects()
        {
            GetGroundTile().OnPlayerStanding();
            if (Velocity != Vector2.Zero && GetGroundTile().WalkingParticle != null && Main.Rand.Next(5) == 0)
            {
                Particle p = Particle.SpawnParticle(GetGroundTile().WalkingParticle, Main.Player);
                p.TimeLeft = 10;
                p.Alpha = 1f;
                p.Position = Main.Player.LegsPosition + new Vector2(Main.Player.Direction == -1 ? Main.Player.Texture.Width - 6 : 10, -10);
                p.Velocity = new Vector2(0f, -Main.Rand.Next(1, 4) / 10f);
            }
        }
    }
}
