﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using TheLight.Attributes;
using TheLight.ID;

namespace TheLight.Entities
{
    [AutoloadTextures(ParticleTexture.Texture)]
    public class Particle : Entity
    {
        public float TimeLeft { get; set; }
        public new Vector2 Position { get; set; }
        public Entity Target { get; set; }
        public int RandomFrame { get; set; }
        public float Rotation { get; set; }
        public bool StickToTarget { get; set; }
        public Vector2 Movement { get; set; }

        public Particle() 
        {
            WhoAmI = Main.ActiveParticles.Count;
            RandomFrame = Main.Rand.Next(0, MaxFrames + 1);
            TimeLeft = 30;
            Alpha = 0.8f;
            Rotation = Main.Rand.Next(-1, 1);
            Color = Color.White;
            Velocity = new Vector2(Main.Rand.Next(-10, 10), Main.Rand.Next(-10, 10)) / 10f; 
            SetDefaults();
            Autoload();
        }

        public override void Autoload()
        {
            List<object> attr = Utils.GetAllAttributes(GetType());
            if (attr != null)
            {
                if (attr.Contains(ParticleTexture.Texture) && Texture == null) Texture = Utils.LoadTexture(TexturePath);
            }
        }

        public override void Update(GameTime gameTime)
        {
            if (!StickToTarget) Position += Velocity;
            else
            {
                Movement += Velocity;
                Position = Movement + Target.Center;
            }

            if (TimeLeft > 0) TimeLeft--;
            else
            {
                Main.ActiveParticles.RemoveAt(WhoAmI);
                for (int i = 0; i < Main.ActiveParticles.Count; i++)
                    Main.ActiveParticles[i].WhoAmI = i;
            }
        }

        public override void DrawSelf(GameTime gameTime, SpriteBatch spriteBatch)
        {
            UpdateDepth();
            spriteBatch.Draw(Texture, Position, new Rectangle((Texture.Width / MaxFrames) * RandomFrame, 0, (Texture.Width / MaxFrames), Texture.Height), Color * Alpha, Rotation, Vector2.Zero, 1f, SpriteEffects.None, LayerDepth);
        }

        public static Particle SpawnParticle<T>(Entity target) where T: Particle, new()
        {
            Main Main = Main.Instance;
            Particle particle = new T();
            particle.Target = target;
            particle.Position = target.Center;
            Main.ActiveParticles.Insert(particle.WhoAmI, particle);
            return particle;
        }

        public static Particle SpawnParticle(Type t, Entity target)
        {
            Main Main = Main.Instance;
            Particle particle = (Particle)Activator.CreateInstance(t);
            particle.Target = target;
            particle.Position = target.Center;
            Main.ActiveParticles.Insert(particle.WhoAmI, particle);
            return particle;
        }
    }
}
