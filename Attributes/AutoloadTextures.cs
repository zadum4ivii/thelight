﻿using System;

namespace TheLight.Attributes
{
    [AttributeUsage(AttributeTargets.Class, Inherited = true)]
    public class AutoloadTextures : Attribute
    {
        public readonly Object[] loadTextures;

        public AutoloadTextures(params Object[] loadTextures) { this.loadTextures = loadTextures; }
    }
}
