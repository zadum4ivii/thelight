﻿using Microsoft.Xna.Framework.Graphics;
using System.Linq;

namespace TheLight.Options
{
    public class Settings
    {
        public bool ShowInfo { get; set; }
        public bool Fullscreen { get; set; }
        public bool EnableEnemies { get; set; }
        public bool EnableTilePlacement { get; set; }
        public bool EnableEnemyPlacement { get; set; }
        public int PreferredResolutionX { get; }
        public int PreferredResolutionY { get; }

        public Settings()
        {
            PreferredResolutionX = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes.Last<DisplayMode>().Width;
            PreferredResolutionY = GraphicsAdapter.DefaultAdapter.SupportedDisplayModes.Last<DisplayMode>().Height;
        }
    }
}
