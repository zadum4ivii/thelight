﻿using Microsoft.Xna.Framework.Input;
using TheLight;

public class KeyButtons
{
    public static KeyboardState CurrentKeyState;
    public static KeyboardState PreviousKeyState;
    public static MouseState CurrentMouseState;
    public static MouseState PreviousMouseState;
    public static float CurrentWheelState;
    public static float PreviousWheelState;
    public static Keys Escape = Keys.Escape;
    public static Keys Left = Keys.A;
    public static Keys Right = Keys.D;
    public static Keys Up = Keys.W;
    public static Keys Down = Keys.S;
    public static Keys Info = Keys.F1;
    public static Keys Run = Keys.LeftShift;
    public static Keys Jump = Keys.Space;
    public static Keys Fullscreen = Keys.F11;
    public static Keys Attack = Keys.LeftControl;
    public static Keys EnableEnemies = Keys.F2;
    public static Keys EnableTilePlacement = Keys.F3;
    public static Keys EnableEnemyPlacement = Keys.F4;
    public static Keys Use = Keys.F;
    public static Keys SwitchLeftHand = Keys.Q;
    public static Keys SwitchRightHand = Keys.E;
    public static Keys Inventory = Keys.Tab;
    public static Keys Enter = Keys.Enter;
    public static Keys Back = Keys.Back;

    public static KeyboardState GetKeyboardState()
    {
        PreviousKeyState = CurrentKeyState;
        CurrentKeyState = Keyboard.GetState();
        return CurrentKeyState;
    }

    public static MouseState GetMouseState()
    {
        PreviousMouseState = CurrentMouseState;
        CurrentMouseState = Mouse.GetState();
        return CurrentMouseState;
    }

    public static bool Pressing(Keys key)
    {
        if (Main.Instance.Chat.ChatMode && key != Info)
            return false;
        return CurrentKeyState.IsKeyDown(key);
    }

    public static bool Pressed(Keys key)
    {
        if (Main.Instance.Chat.ChatMode && key != Info)
            return false;
        return CurrentKeyState.IsKeyDown(key) && !PreviousKeyState.IsKeyDown(key);
    }

    public static bool PressedForChat(Keys key) => CurrentKeyState.IsKeyDown(key) && !PreviousKeyState.IsKeyDown(key);
    public static bool Pressed2ForChat(Keys key) => PreviousKeyState.IsKeyDown(key) && !CurrentKeyState.IsKeyDown(key);

    public static bool Pressing2(Keys key)
    {
        if (Main.Instance.Chat.ChatMode && key != Info)
            return false;
        return PreviousKeyState.IsKeyDown(key);
    }

    public static bool Pressed2(Keys key)
    {
        if (Main.Instance.Chat.ChatMode && key != Info)
            return false;
        return PreviousKeyState.IsKeyDown(key) && !CurrentKeyState.IsKeyDown(key);
    }

    public static bool PressingLeftMouse() => CurrentMouseState.LeftButton == ButtonState.Pressed;
    public static bool PressedLeftMouse() => CurrentMouseState.LeftButton == ButtonState.Pressed && PreviousMouseState.LeftButton != ButtonState.Pressed;
    public static bool PressingRightMouse() => CurrentMouseState.RightButton == ButtonState.Pressed;
    public static bool PressedRightMouse() => CurrentMouseState.RightButton == ButtonState.Pressed && PreviousMouseState.RightButton != ButtonState.Pressed;
    public static bool ScrolledWheelUp() => CurrentMouseState.ScrollWheelValue > PreviousWheelState;
    public static bool ScrolledWheelDown() => CurrentMouseState.ScrollWheelValue < PreviousWheelState;
}