﻿namespace TheLight.ID
{
    public enum ItemTexture
    {
        Texture,
        Shadow,
        Inventory,
        Colors,
        Hand,
        Head,
        Body,
        Arms,
        Legs
    }

    public enum NPCTexture
    {
        Texture,
        Shadow,
        Colors,
        Death,
        Outline
    }

    public enum TileTexture
    {
        Texture,
        Shadow,
        Colors
    }

    public enum UITexture
    {
        Texture,
        Highlight,
        SlotBackground
    }

    public enum ParticleTexture
    {
        Texture
    }
}
