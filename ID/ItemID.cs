﻿namespace TheLight.ID
{
    public class ItemID
    {
        public const short Coin = 0;
        public const short ExampleItem = 1;
        public const short Stick = 2;
        public const short Cactus = 3;
        public const short Hat = 4;
        public const short BizarreHat = 5;
        public const short Knife = 6;
        public const short ForgottenSack = 7;
        public const short WeakPotionofHealing = 8;
        public const short Skull = 9;
        public const short Bone = 10;
        public const short TestAxe = 11;
    }
}
