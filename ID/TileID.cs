﻿namespace TheLight.ID
{
    public class TileID
    {
        public const short Sand = 0;
        public const short Cactus = 1;
        public const short BigDeadTree = 2;
        public const short MediumDeadTree = 3;
        public const short DeadTreeStump = 4;
        public const short DesertWeed = 5;
        public const short Skull = 6;
        public const short Bones = 7;
    }
}
