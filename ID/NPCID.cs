﻿namespace TheLight.ID
{
    public class NPCID
    {
        public const short Mummy = 0;
        public const short SmallTumbleweed = 1;
        public const short BigTumbleweed = 2;
    }
}
