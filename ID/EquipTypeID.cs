﻿namespace TheLight.ID
{
    public class EquipTypeID
    {
        public const short Head = 0;
        public const short Body = 1;
        public const short Legs = 2;
        public const short Necklace = 3;
        public const short Ring = 4;
        public const short Accessory = 5;
    }
}
