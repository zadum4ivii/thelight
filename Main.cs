﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using TheLight.Entities;
using TheLight.Entities.UI;
using TheLight.Gameplay;
using TheLight.Options;
using TheLight.UI;

namespace TheLight
{
    public class Main : Game
    {
        internal List<Entity> ActiveEntities { get; set; }
        internal List<Entity> EntitiesDepth { get; set; }
        internal List<FloatingText> ActiveFloatingText { get; set; }
        internal List<Particle> ActiveParticles { get; set; }
        public double TimeOfDrawing { get; private set; }
        public GraphicsDeviceManager Graphics;
        public Interface Interface { get; set; }
        public Player Player { get; set; }
        public Random Rand { get; set; }
        public Screen Screen { get; set; }
        public Settings Settings { get; set; }
        public Lighting Lighting { get; set; }
        public SpriteBatch SpriteBatch { get; set; }
        public SpriteFont SpriteFont { get; set; }
        public static Main Instance { get; private set; }
        public World World { get; set; }
        public Chat Chat { get; set; }
        public static Texture2D Pixel { get; private set; }

        public Main()
        {
            Instance = this;
            Graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = true;
            Rand = new Random();
        }

        protected override void Initialize()
        {
            Settings = new Settings();
            Window.Title = "The Light";
            Screen = new Screen();
            Interface = new Interface();
            Interface.AddInterfaceElements();
            Player = new Player();
            IsMouseVisible = false;
            ActiveEntities = new List<Entity>() { Player };
            EntitiesDepth = new List<Entity>() { };
            ActiveFloatingText = new List<FloatingText>() { };
            ActiveParticles = new List<Particle>() { };
            World = new World();
            Lighting = new Lighting();
            Chat = new Chat();
            Graphics.HardwareModeSwitch = false;
            Graphics.PreferredBackBufferWidth = Screen.Width;
            Graphics.PreferredBackBufferHeight = Screen.Height;
            Graphics.IsFullScreen = false;
            Graphics.ApplyChanges();
            LoadGame();
            base.Initialize();
        }

        protected void LoadGame()
        {
            World.WorldLoad();
            Player.PlayerLoad();
            System.Windows.Forms.MessageBox.Show("Игра успешно загружена!", "Уведомление", System.Windows.Forms.MessageBoxButtons.OK);
        }

        public void SaveGame()
        {
            World.WorldSave();
            Player.PlayerSave();
            System.Windows.Forms.MessageBox.Show("Игра успешно сохранена!", "Уведомление", System.Windows.Forms.MessageBoxButtons.OK);
        }

        protected override void LoadContent()
        {
            SpriteBatch = new SpriteBatch(GraphicsDevice);
            SpriteFont = Content.Load<SpriteFont>("Fonts/Font");

            Pixel = new Texture2D(GraphicsDevice, 1, 1);
            Pixel.SetData(new Color[] { Color.White });
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            KeyButtons.GetKeyboardState();
            KeyButtons.GetMouseState();
            if (ActiveEntities.Count != -1)
                for (int i = 0; i < ActiveEntities.Count; i++)
                    ActiveEntities[i].Update(gameTime);
            Interface.Update(gameTime);
            EntitiesDepth = new List<Entity>() { };
            for (int i = 0; i < ActiveEntities.Count; i++)
                EntitiesDepth.Add(ActiveEntities[i]);
            EntitiesDepth.Sort((e1, e2) => e1.BaseHitbox.Y.CompareTo(e2.BaseHitbox.Y));
            Screen.Update(gameTime);
            Lighting.Update(gameTime);
            if (ActiveFloatingText.Count != -1)
                for (int i = 0; i < ActiveFloatingText.Count; i++)
                    ActiveFloatingText[i].Update(gameTime);
            if (ActiveParticles.Count != -1)
                for (int i = 0; i < ActiveParticles.Count; i++)
                    ActiveParticles[i].Update(gameTime);
            World.Update(gameTime);
            Buttons();
            if (!Settings.ShowInfo) Chat.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.White);
            Screen.UpdateCam(Player);
            ++TimeOfDrawing;
            //drawing solid tiles at Y = 0
            SpriteBatch.Begin(SpriteSortMode.FrontToBack, blendState: BlendState.AlphaBlend, samplerState: SamplerState.PointClamp, transformMatrix: new Matrix?(Matrix.CreateTranslation(new Vector3(-Screen.ScreenPosition.X, -Screen.ScreenPosition.Y, 0.0f))));
            for (float i = 0; i < World.WorldLength / 32f; i++)
                for (float j = 0; j < World.WorldWidth / 32f; j++)
                        if (World.WorldTiles[(int)i, (int)j, 0] != null)
                            World.WorldTiles[(int)i, (int)j, 0].DrawSelf(gameTime, SpriteBatch);
            SpriteBatch.End();
            //drawing shadows 
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, transformMatrix: new Matrix?(Matrix.CreateTranslation(new Vector3(-Screen.ScreenPosition.X, -Screen.ScreenPosition.Y, 0.0f))));
            if (ActiveEntities.Count != -1)
                for (int i = 0; i < ActiveEntities.Count; i++)
                    ActiveEntities[i].DrawShadow(gameTime, SpriteBatch);
            SpriteBatch.End();
            //drawing all entities & tiles at Y > 0
            SpriteBatch.Begin(SpriteSortMode.FrontToBack, BlendState.AlphaBlend, SamplerState.PointClamp, transformMatrix: new Matrix?(Matrix.CreateTranslation(new Vector3(-Screen.ScreenPosition.X, -Screen.ScreenPosition.Y, 0.0f))));
            if (ActiveEntities.Count != -1)
                for (int i = 0; i < ActiveEntities.Count; i++)
                    ActiveEntities[i].DrawSelf(gameTime, SpriteBatch);
            for (float i = 0; i < World.WorldLength / 32f; i++)
                for (float j = 0; j < World.WorldWidth / 32f; j++)
                    for (float k = 1; k < World.WorldHeight / 32f; k++)
                        if (World.WorldTiles[(int)i, (int)j, (int)k] != null)
                            World.WorldTiles[(int)i, (int)j, (int)k].DrawSelf(gameTime, SpriteBatch);
            if (ActiveParticles.Count != -1)
                for (int i = 0; i < ActiveParticles.Count; i++)
                    ActiveParticles[i].DrawSelf(gameTime, SpriteBatch);

            SpriteBatch.End();
            //drawing lighting
            SpriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp);
            Lighting.DrawSelf(gameTime, SpriteBatch);
            SpriteBatch.End();
            //drawing screen text
            SpriteBatch.Begin(blendState: BlendState.AlphaBlend, samplerState: SamplerState.PointClamp);
            for (int i = 0; i < ActiveEntities.Count; i++)
                if (ActiveEntities[i].CanLoot)
                {
                    Utils.DrawBorderString(SpriteBatch, ActiveEntities[i].InteractionText, new Vector2(Player.Position.X, Player.HeadPosition.Y - Player.HeadTexture.Height) - new Vector2(SpriteFont.MeasureString(ActiveEntities[i].InteractionText).Length() / 2, 0) + new Vector2((Player.Texture.Width / 2) + 6, -22) - Screen.ScreenPosition, Color.Black, Color.White);
                    break;
                }
            if (ActiveFloatingText.Count != -1)
                for (int i = 0; i < ActiveFloatingText.Count; i++)
                    ActiveFloatingText[i].DrawSelf(gameTime, SpriteBatch);
            if (Settings.ShowInfo) DrawInfo();
            if (Player.PickedItems.Count > 0) DrawPickedItems();
            if (!Settings.ShowInfo) Chat.DrawSelf(gameTime, SpriteBatch);
            Interface.DrawSelf(gameTime, SpriteBatch);
            DrawingUIElements();
            if (!Interface.OverInterface)
                DrawingObjectsNames();
            if (World.DrawingWorldName) World.DrawWorldName(gameTime, SpriteBatch);
            Utils.DrawBorderString(SpriteBatch, "Made by zadum4ivii and Zerokk", new Vector2(5, Screen.Height - 20), Color.Black, Color.White);
            SpriteBatch.End();
            base.Draw(gameTime);
        }

        protected void DrawInfo()
        {
            string[] info = new string[12] {
                "Позиция X: " + Player.Position.X,
                "Позиция Y: " + Player.Position.Y,
                "Позиция Z: " + Player.Position.Z,
                "Скорость X: " + Player.Velocity.X,
                "Скорость Y: " + Player.Velocity.Y,
                "Направление: " + Player.Direction,
                "Количество объектов: " + ActiveEntities.Count,
                "Спавн мобов: " + (Settings.EnableEnemies ? "Да" : "Нет"),
                "FPS: " + World.FPS,
                "Экран: " + Screen.Width + "x" + Screen.Height,
                "Спавн блоков на ПКМ: " + Settings.EnableTilePlacement,
                "Спавн врагов на ПКМ: " + Settings.EnableEnemyPlacement
            };
            for (int i = 0; i < info.Length; i++)
                Utils.DrawBorderString(SpriteBatch, info[i], new Vector2(22, 75 + (20 * i)), Color.Black, Color.White);
        }

        protected void DrawPickedItems()
        {
            for (int i = 0; i < Player.PickedItems.Count; i++)
            {
                string s = Player.PickedItems[i].Item1.Name + " x" + Player.PickedItems[i].Item2;
                Utils.DrawBorderString(SpriteBatch, s, new Vector2(Player.Position.X, Player.HeadPosition.Y - Player.HeadTexture.Height - (i * 15) - 20) - new Vector2(SpriteFont.MeasureString(s).Length() / 2, 0) + new Vector2((Player.Texture.Width / 2) + 6, -22) - Screen.ScreenPosition, Color.Black, Color.White);
            }
        }

        protected void Buttons()
        {
            KeyButtons.PreviousWheelState = KeyButtons.CurrentWheelState;
            KeyButtons.CurrentWheelState = KeyButtons.GetMouseState().ScrollWheelValue;
            if (KeyButtons.Pressed(KeyButtons.Info)) Settings.ShowInfo = !Settings.ShowInfo;
            if (KeyButtons.Pressed(KeyButtons.Inventory)) Interface.Inventory.InventoryOpened = !Interface.Inventory.InventoryOpened;
            if (KeyButtons.Pressed(KeyButtons.EnableEnemies)) Settings.EnableEnemies = !Settings.EnableEnemies;
            if (KeyButtons.Pressed(KeyButtons.EnableTilePlacement))
            {
                Settings.EnableTilePlacement = !Settings.EnableTilePlacement;
                Settings.EnableEnemyPlacement = false;
            }
            if (KeyButtons.Pressed(KeyButtons.EnableEnemyPlacement))
            {
                Settings.EnableEnemyPlacement = !Settings.EnableEnemyPlacement;
                Settings.EnableTilePlacement = false;
            }
            if (KeyButtons.Pressed(KeyButtons.Fullscreen))
            {
                Settings.Fullscreen = !Settings.Fullscreen;
                ChangeFullscreen(Settings.Fullscreen);
            }
            if (KeyButtons.Pressed(KeyButtons.SwitchLeftHand) && !Player.UsingItem)
            {
                Interface.LeftHandHotbar1.Selected = !Interface.LeftHandHotbar1.Selected;
                Interface.LeftHandHotbar2.Selected = !Interface.LeftHandHotbar2.Selected;
                Interface.SelectedLeftHandHotbar = Interface.LeftHandHotbar1.Selected ? Interface.LeftHandHotbar1 : Interface.LeftHandHotbar2;
            }
            if (KeyButtons.Pressed(KeyButtons.SwitchRightHand) && !Player.UsingItem)
            {
                Interface.RightHandHotbar1.Selected = !Interface.RightHandHotbar1.Selected;
                Interface.RightHandHotbar2.Selected = !Interface.RightHandHotbar2.Selected;
                Interface.SelectedRightHandHotbar = Interface.RightHandHotbar1.Selected ? Interface.RightHandHotbar1 : Interface.RightHandHotbar2;
            }
            if (KeyButtons.Pressed(KeyButtons.Escape) && !Chat.ChatMode)
            {
                if (!Interface.MenuOpenButton.DrawCloseAnimation && !Interface.MenuOpenButton.DrawOpenAnimation)
                {
                    Interface.MenuOpenButton.DrawOpenAnimation = !Interface.MenuOpenButton.MenuOpened;
                    Interface.MenuOpenButton.DrawCloseAnimation = Interface.MenuOpenButton.MenuOpened;
                }
            }
        }

        public void MouseText(string Text, Color color)
        {
            Interface.Cursor.MouseText = true;
            MouseState mouse = Mouse.GetState();
            SpriteBatch.DrawString(SpriteFont, Text, new Vector2(mouse.X, mouse.Y) + new Vector2(20), color, 0f, new Vector2(), 1f, SpriteEffects.None, 1f);
        }

        public void MouseBlackTextWithOutline(string Text, Color OutlineColor)
        {
            Interface.Cursor.MouseText = true;
            MouseState mouse = Mouse.GetState();
            Utils.DrawBorderString(SpriteBatch, Text, new Vector2(mouse.X, mouse.Y) + new Vector2(20), Color.Black, OutlineColor);
        }

        public void DrawingUIElements()
        {
            if (Interface.Cursor.MouseText) return;
            List<UIElement> Buttons = new List<UIElement> {
                Interface.MenuOpenButton, Interface.MenuOpenButton.MenuCloseButton,
                Interface.MenuOpenButton.MenuOptionsButton, Interface.MenuOpenButton.MenuSaveButton,
                Interface.LeftHandHotbar1, Interface.LeftHandHotbar2,
                Interface.RightHandHotbar1, Interface.RightHandHotbar2,
                Interface.HealthBar, Interface.ManaBar, Interface.TimeBar
            };
            foreach (UIElement b in Buttons)
                if (b != null)
                    if (b.MouseOver())
                        MouseBlackTextWithOutline(b.MouseTextString ?? b.Name, Color.White);
            if (Interface.Inventory.InventoryOpened)
                foreach (Slot s in Interface.Inventory.InventorySlots)
                    if (s != null)
                        if (s.MouseOver())
                            MouseBlackTextWithOutline(s.MouseTextString ?? s.Name, Color.White);
        }

        public void DrawingObjectsNames()
        {
            System.Drawing.Rectangle mouseRect = new System.Drawing.Rectangle((int)Interface.Cursor.Position.X, (int)Interface.Cursor.Position.Y, 4, 4);
            if (EntitiesDepth.Count != -1)
                for (int i = EntitiesDepth.Count - 1; i >= 0; i--)
                {
                    Entity entity = EntitiesDepth[i];

                    System.Drawing.Rectangle rect2 = entity.TextureBox();

                    if (mouseRect.IntersectsWith(rect2) && !Interface.Cursor.MouseText)
                    {
                        if (Interface.Inventory.InventoryOpened)
                        {
                            if (!mouseRect.IntersectsWith(Interface.Inventory.TextureBox()) || mouseRect.IntersectsWith(Interface.Inventory.WindowTextureBox()))
                                MouseBlackTextWithOutline(entity.MouseTextString ?? entity.Name, Color.White);
                        }
                        else
                            MouseBlackTextWithOutline(entity.MouseTextString ?? entity.Name, Color.White);
                    }
                }
        }

        public void ChangeFullscreen(bool Fullscreen)
        {
            Graphics.PreferredBackBufferWidth = Fullscreen ? Settings.PreferredResolutionX : 800;
            Graphics.PreferredBackBufferHeight = Fullscreen ? Settings.PreferredResolutionY : 600;
            Screen.Width = Graphics.PreferredBackBufferWidth;
            Screen.Height = Graphics.PreferredBackBufferHeight;
            Graphics.IsFullScreen = Fullscreen;
            Graphics.ApplyChanges();
            Interface.NeedPositionUpdate = true;
            Lighting.LightingTexture();
        }

        public void TimeOfDrawingNull() { TimeOfDrawing = 0.0; }
    }
}
