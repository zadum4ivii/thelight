﻿using System;

namespace TheLight
{
    public static class Program
    {
        [STAThread]
        static void Main()
        {
            try
            {
                using var game = new Main();
                game.Run();
            }
            catch (Exception e)
            {
                Utils.Exception(e);
            }
        }
    }
}
