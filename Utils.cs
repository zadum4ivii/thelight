﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;
using TheLight.Attributes;
using TheLight.Entities;

namespace TheLight
{
    public static class Utils
    {
        public static float AbsPow(float num, float power) => num >= 0.0 ? (float)Math.Pow(num, power) : -(float)Math.Pow(-num, power);

        public static void MoveTowards(Entity entity, Vector2 target, float speed, float turnResistance)
        {
            Vector2 movement = target - entity.Center;
            float length = movement.Length();
            if (length > speed) movement *= speed / length;
            movement = (entity.Velocity * turnResistance + movement) / (turnResistance + 1f);
            length = movement.Length();
            if (length > speed) movement *= speed / length;
            entity.Velocity = movement;
        }

        public static float NextFloat(this Random randoom) => (float)randoom.NextDouble();

        public static float NextFloat(this Random random, float min, float max) => random.NextFloat() * (max - min) + min;

        public static bool NextChance(this Random randoom, double chance) => randoom.NextDouble() <= chance;

        public static Texture2D SwapColors(Texture2D colorsTexture, Texture2D originalTexture, int colorIndex)
        {
            Main Main = Main.Instance;
            Texture2D newTexture = new Texture2D(Main.GraphicsDevice, originalTexture.Width, originalTexture.Height);
            Color[] originalTextureData = new Color[originalTexture.Width * originalTexture.Height];
            originalTexture.GetData(originalTextureData);
            Color[] colorsTextureData = new Color[colorsTexture.Width * colorsTexture.Height];
            colorsTexture.GetData(colorsTextureData);
            Color[] newTextureData = new Color[newTexture.Width * newTexture.Height];
            for (int i = 0; i < originalTextureData.Length; i++)
            {
                for (int j = 0; j < colorsTexture.Width; j++)
                {
                    if (originalTextureData[i].R == colorsTextureData[j].R
                    && originalTextureData[i].G == colorsTextureData[j].G
                    && originalTextureData[i].B == colorsTextureData[j].B)
                        newTextureData[i] = new Color
                        (
                            colorsTextureData[j + (colorsTexture.Width * colorIndex)].R,
                            colorsTextureData[j + (colorsTexture.Width * colorIndex)].G,
                            colorsTextureData[j + (colorsTexture.Width * colorIndex)].B
                        );
                }
            }
            for (int i = 0; i < originalTextureData.Length; i++)
                if (originalTextureData[i].A == 255 && newTextureData[i].A == 0)
                    newTextureData[i] = new Color
                    (
                        originalTextureData[i].R, originalTextureData[i].G, originalTextureData[i].B
                    );
            newTexture.SetData(newTextureData);
            return newTexture;
        }

        public static void MakeTextureTransparent(Texture2D texture)
        {
            Color[] data = new Color[texture.Width * texture.Height];
            texture.GetData(data);
            for (int y = 0; y < texture.Height; y++)
                for (int x = 0; x < texture.Width; x++)
                    data[x + y * texture.Width] = Color.FromNonPremultiplied(0, 0, 0, 0);
            Texture2D newTexture = new Texture2D(texture.GraphicsDevice, texture.Width, texture.Height);
            newTexture.SetData(data);
        }

        public static void DrawRectangle(SpriteBatch SpriteBatch, Rectangle Rectangle, Color Color)
        {
            SpriteBatch.Draw(Main.Pixel, new Rectangle(Rectangle.Left, Rectangle.Top, Rectangle.Width, 1), Color);
            SpriteBatch.Draw(Main.Pixel, new Rectangle(Rectangle.Left, Rectangle.Bottom, Rectangle.Width, 1), Color);
            SpriteBatch.Draw(Main.Pixel, new Rectangle(Rectangle.Left, Rectangle.Top, 1, Rectangle.Height), Color);
            SpriteBatch.Draw(Main.Pixel, new Rectangle(Rectangle.Right, Rectangle.Top, 1, Rectangle.Height + 1), Color);
        }

        public static void FillRectangle(SpriteBatch SpriteBatch, Rectangle Rectangle, Color Color)
            => SpriteBatch.Draw(Main.Pixel, Rectangle, Color);

        public static Vector2 DrawBorderString(SpriteBatch SpriteBatch, string Text, Vector2 Position, Color TextColor, Color OutLineColor, float Scale = 1f, float Anchorx = 0f, float AnchoryY = 0f, int StringLimit = -1)
        {
            if (StringLimit != -1 && Text.Length > StringLimit)
                Text.Substring(0, StringLimit);
            SpriteFont fontMouseText = Main.Instance.SpriteFont;
            for (int i = -1; i < 2; i++)
                for (int j = -1; j < 2; j++)
                    SpriteBatch.DrawString(fontMouseText, Text, Position + new Vector2(i, j), OutLineColor, 0f, new Vector2(Anchorx, AnchoryY) * fontMouseText.MeasureString(Text), Scale, SpriteEffects.None, 0f);
            SpriteBatch.DrawString(fontMouseText, Text, Position, TextColor, 0f, new Vector2(Anchorx, AnchoryY) * fontMouseText.MeasureString(Text), Scale, SpriteEffects.None, 0f);
            return fontMouseText.MeasureString(Text) * Scale;
        }

        public static Texture2D GenerateItemShadow(Texture2D texture, int maxFrames)
        {
            Main Main = Main.Instance;
            Texture2D shadowTexture = new Texture2D(Main.GraphicsDevice, texture.Width / maxFrames, 2);
            Color[] shadowTextureData = new Color[shadowTexture.Width * shadowTexture.Height];
            for (int i = 0; i < shadowTextureData.Length; i++)
                shadowTextureData[i] = new Color(69, 67, 68);
            shadowTexture.SetData(shadowTextureData);
            return shadowTexture;
        }

        public static Texture2D MergeTextures(Texture2D texture1, Texture2D texture2)
        {
            Main Main = Main.Instance;
            Texture2D mergedTexture = new Texture2D(Main.GraphicsDevice, texture1.Width, texture1.Height);
            Color[] texture1Data = new Color[texture1.Width * texture1.Height];
            texture1.GetData(texture1Data);
            Color[] texture2Data = new Color[texture2.Width * texture2.Height];
            texture2.GetData(texture2Data);
            Color[] mergedTextureData = new Color[texture1.Width * texture1.Height];
            for (int i = 0; i < mergedTextureData.Length; i++)
                mergedTextureData[i] = texture2Data[i].A == 255 ? texture2Data[i] : texture1Data[i];
            mergedTexture.SetData(mergedTextureData);
            return mergedTexture;
        }

        public static Texture2D SwapColor(Texture2D texture, Color originalColor, Color newColor)
        {
            Color[] textureData = new Color[texture.Width * texture.Height];
            texture.GetData(textureData);
            for (int i = 0; i < textureData.Length; i++)
                if (textureData[i] == originalColor) textureData[i] = newColor;
            texture.SetData(textureData);
            return texture;
        }

        public static Texture2D SwapAllColors(Texture2D originalTexture, Color newColor)
        {
            Main Main = Main.Instance;
            Texture2D newTexture = new Texture2D(Main.GraphicsDevice, originalTexture.Width, originalTexture.Height);
            Color[] newTextureData = new Color[newTexture.Width * newTexture.Height];
            Color[] originalTextureData = new Color[originalTexture.Width * originalTexture.Height];
            originalTexture.GetData(originalTextureData);
            for (int i = 0; i < originalTextureData.Length; i++)
                if (originalTextureData[i].A == 255)
                    newTextureData[i] = newColor;
            newTexture.SetData(newTextureData);
            return newTexture;
        }

        public static Texture2D LoadTexture(string texturePath)
        {
            Main Main = Main.Instance;
            if (File.Exists($@"Content/{texturePath}.xnb")) return Main.Content.Load<Texture2D>($@"{texturePath}");
            else return null;
        }

        public static void Exception(Exception e)
        {
            try
            {
                string message = e.ToString();
                StreamWriter crash = new StreamWriter(".log", append: true);
                crash.WriteLine(DateTime.Now + "\n" + message + "\n");
                crash.Close();
                MessageBox.Show(message, "The Light: Ошибка");
            }
            catch
            { }
        }

        public static void Log(string message)
        {
            try
            {
                StreamWriter crash = new StreamWriter(".log", append: true);
                crash.WriteLine(DateTime.Now + "\n" + message + "\n");
                crash.Close();
            }
            catch
            { }
        }

        public static List<Object> GetAllAttributes(Type type)
        {
            List<Object> attributesList = new List<Object>();
            AutoloadTextures attr = (AutoloadTextures)Attribute.GetCustomAttribute(type, typeof(AutoloadTextures));
            Type parent = type.BaseType;

            if (parent != null && !parent.Namespace.StartsWith("System"))
            {
                var newAttr = GetAllAttributes(parent);
                foreach (Object obj in newAttr)
                    attributesList.Add(obj);
            }
            if (attr != null)
            {
                foreach (Object obj in attr.loadTextures)
                    attributesList.Add(obj);
            }
            return attributesList;
        }
    }
}
